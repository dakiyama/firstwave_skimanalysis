cd ../firstwave_skimanalysis/Root
rootcint -f MyAnalysisDict.cxx -c ../MyAnalysis/PhysicsObjectProxyBase.h LinkDef.h
g++ -fPIC `root-config --cflags --libs` -shared MyAnalysisDict.cxx MyAnalysis.cxx alg_util.cxx track_manager.cxx event_manager.cxx physics_object_manager.cxx truth_manager.cxx ntuple_manager.cxx -o libmyanalysis.so
