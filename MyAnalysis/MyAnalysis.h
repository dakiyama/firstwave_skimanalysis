//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Oct  8 10:26:25 2021 by ROOT version 6.22/08
// from TChain tree/
//////////////////////////////////////////////////////////

#ifndef MyAnalysis_h
#define MyAnalysis_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <map>
#include <iostream>
#include <memory>
#include <utility>

// Header file for the classes stored in the TTree if any.
#include "TClonesArray.h"
#include "TObject.h"
#include "vector"
#include "TH1.h"
#include "TLorentzVector.h"

class PhysObjBase;
class myTrack;
class myJet;
class myElectron;
class myMuon;
class myTau;
class myZee;
class myZmumu;
class myMET;
class myTruth;
class myCaloCluster;

class MyAnalysis {
 public :
  TTree          *fChain;   //!pointer to the analyzed TTree or TChain
  Int_t           fCurrent; //!current Tree number in a TChain
  myMET                        *m_MET;
  myMET                        *m_MET_Electron;
  myMET                        *m_MET_Muon;
  myMET                        *m_MET_XeTrigger;
  std::vector<float>           *m_mcEventWeights;

  std::unique_ptr<myMET>                        m_MET_DRAW;
  std::unique_ptr<std::vector<myJet*>>         m_drawJets;
  std::vector<myJet*>          *m_goodJets;
  std::vector<myJet*>          *m_truthJets;
  std::vector<myMuon*>         *m_goodMuons;
  std::vector<myTau*>          *m_goodTaus;
  std::vector<myTrack*>        *m_msTracks;
  std::vector<myElectron*>     *m_goodElectrons;
  std::vector<myCaloCluster*>  *m_egammaClusters;
  std::vector<myTruth*>        *m_truthParticles;
  std::vector<myTrack*>        *m_threeLayerTracks;
  std::vector<myTrack*>        *m_fourLayerTracks;
  std::vector<myTrack*>        *m_Tracks;

  std::unique_ptr<TClonesArray> m_tca_drawJets;
  TClonesArray *m_tca_goodJets;
  TClonesArray *m_tca_truthJets;
  TClonesArray *m_tca_goodMuons;
  TClonesArray *m_tca_goodTaus;
  TClonesArray *m_tca_msTracks;
  TClonesArray *m_tca_goodElectrons;
  TClonesArray *m_tca_egammaClusters;
  TClonesArray *m_tca_truthParticles;
  TClonesArray *m_tca_threeLayerTracks;
  TClonesArray *m_tca_fourLayerTracks;
  TClonesArray *m_tca_Tracks;
  std::unique_ptr<TClonesArray> m_tca_mcEventWeights;

  // Fixed size dimensions of array or collections stored in the TTree if any.
  static constexpr Int_t kMaxTracks = 136;
  static constexpr Int_t kMaxThreeLayerTracks = 1;
  static constexpr Int_t kMaxFourLayerTracks = 1;
  static constexpr Int_t kMaxDRAWJets = 78;
  static constexpr Int_t kMaxRawJets = 63;
  static constexpr Int_t kMaxGoodJets = 12;
  static constexpr Int_t kMaxTruthJets = 43;
  static constexpr Int_t kMaxGoodElectrons = 1;
  static constexpr Int_t kMaxGoodMuons = 1;
  static constexpr Int_t kMaxGoodTaus = 2;
  static constexpr Int_t kMaxZee_ProbeCluster = 1;
  static constexpr Int_t kMaxZee_ProbeTrack = 14;
  static constexpr Int_t kMaxZmumu_ProbeCluster = 1;
  static constexpr Int_t kMaxZmumu_ProbeTrack = 1;
  static constexpr Int_t kMaxTruths = 43;
  static constexpr Int_t kMaxMSTracks = 12;
  static constexpr Int_t kMaxEgammaClusters = 178;
  static constexpr Int_t kMaxMET = 1;
  static constexpr Int_t kMaxMET_ForEleCR = 1;
  static constexpr Int_t kMaxMET_ForMuCR = 1;
  static constexpr Int_t kMaxMET_Truth = 1;
  static constexpr Int_t kMaxMET_ForXeTrigger = 1;
  static constexpr Int_t kMaxMET_ForDRAW = 1;

  const float zmass;

  // file
  TTree* m_tree;
  TFile* m_file;
  TH1D* m_h_xsec_eff;
  TH1D* m_h_sum_of_events;
  TH1D* m_h_zmass_os_greater10;
  std::string m_kinematics_region;

  // event
  int    m_is_data;
  int    m_do_prw;
  int    m_all_event_pass;
  int    m_all_track_pass;
  int    m_all_truth_pass;
  int    m_do_not_write_truth;

  int    m_sum_of_events;
  float  m_sum_of_weights;
  float  m_sum_of_weights_squared;
  float  m_mc_channel_number;
  float  m_xsec_eff;
  int    m_runNumber;
  int    m_lumiBlock;
  long long int    m_eventNumber;
  int    m_randomRunNumber;
  float  m_mc_eventweight;
  float  m_prw_weight;
  float  m_average_interaction_per_crossing;
  float  m_actual_interaction_per_crossing;
  float  m_corrected_average_interaction_per_crossing;
  float  m_SUSYCrossSectionDB;
  int    m_pass_electron;
  int    m_pass_muon;
  int    m_pass_missingET;
  int    m_has_pv;
  int    m_has_bad_muon;
  int    m_has_cosmic_muon;
  int    m_has_bad_jet;
  int    m_pass_data_quality;
  int    m_pass_event_cleaning;
  int    m_is_bad_muon_missingET;
  float  m_pv_z;
  int    m_nMuons;
  int    m_nElectrons;
  int    m_nTaus;
  int    m_nJets;
  int    m_nMSTracks;
  int    m_nCaloClusters;
  int    m_nTracks;
  int    m_nTruths;
  int    m_nTruthJets;

  // muon
  std::vector<float>* m_muon_phi;
  std::vector<float>* m_muon_eta;
  std::vector<float>* m_muon_pt;
  std::vector<float>* m_muon_e;
  std::vector<int>*   m_muon_charge;
  std::vector<float>* m_muon_ptVarCone20OverPt;
  std::vector<float>* m_muon_ptVarCone30OverPt;
  std::vector<float>* m_muon_ptVarCone40OverPt;
  std::vector<float>* m_muon_eTTopoCone20;
  std::vector<float>* m_muon_eTTopoCone30;
  std::vector<float>* m_muon_eTTopoCone40;
  std::vector<int>*   m_muon_quality;
  std::vector<int>*   m_muon_isTrigMatch;
  std::vector<int>*   m_muon_isSignal;
  std::vector<std::vector<long unsigned int>>*   m_muon_index;
  std::vector<int>*   m_muon_key;

  // electron
  std::vector<float>* m_electron_phi;
  std::vector<float>* m_electron_eta;
  std::vector<float>* m_electron_pt;
  std::vector<float>* m_electron_e;
  std::vector<int>*   m_electron_charge;
  std::vector<float>* m_electron_ptVarCone20OverPt;
  std::vector<float>* m_electron_eTTopoCone20;
  std::vector<int>*   m_electron_quality;
  std::vector<int>*   m_electron_isTrigMatch;
  std::vector<int>*   m_electron_isSignal;
  std::vector<std::vector<long unsigned int>>*   m_electron_index;
  std::vector<int>*   m_electron_key;

  // tau
  std::vector<float>* m_tau_phi;
  std::vector<float>* m_tau_eta;
  std::vector<float>* m_tau_pt;
  std::vector<float>* m_tau_e;
  std::vector<int>* m_tau_charge;
  std::vector<int>* m_tau_nprongs;
  std::vector<float>* m_tau_BDTJetScore;
  std::vector<int>* m_tau_isMedium;
  /* std::vector<std::vector<float>>* m_tau_track_phi; */
  /* std::vector<std::vector<float>>* m_tau_track_eta; */
  /* std::vector<std::vector<float>>* m_tau_track_pt; */
  /* std::vector<std::vector<float>>* m_tau_track_e; */
  /* std::vector<std::vector<int>>* m_tau_track_charge; */

  // jet
  std::vector<float>* m_jet_phi;
  std::vector<float>* m_jet_eta;
  std::vector<float>* m_jet_pt;
  std::vector<float>* m_jet_e;
  std::vector<int>*   m_jet_isBJet;

  // MSTrack
  std::vector<float>* m_msTrack_phi;
  std::vector<float>* m_msTrack_eta;
  std::vector<float>* m_msTrack_pt;
  std::vector<float>* m_msTrack_e;
  std::vector<int>*   m_msTrack_charge;

  // Calo Cluster
  std::vector<float>* m_caloCluster_phi;
  std::vector<float>* m_caloCluster_eta;
  std::vector<float>* m_caloCluster_pt;
  std::vector<float>* m_caloCluster_e;
  std::vector<float>* m_caloCluster_clusterSize;

  // met
  float m_missingET_TST_pt;
  float m_missingET_TST_phi;
  float m_TST_pt;
  float m_TST_phi;
  float m_missingET_CST_pt;
  float m_missingET_CST_phi;
  float m_CST_pt;
  float m_CST_phi;
  float m_missingET_TST_leleVeto_pt;
  float m_missingET_TST_leleVeto_phi;
  float m_missingET_TST_lmuVeto_pt;
  float m_missingET_TST_lmuVeto_phi;

  // truthjet
  std::vector<float>* m_truthjet_phi;
  std::vector<float>* m_truthjet_eta;
  std::vector<float>* m_truthjet_pt;
  std::vector<float>* m_truthjet_e;

  // truth
  std::vector<float>* m_truth_phi;
  std::vector<float>* m_truth_eta;
  std::vector<float>* m_truth_pt;
  std::vector<float>* m_truth_e;
  std::vector<float>* m_truth_vis_phi;
  std::vector<float>* m_truth_vis_eta;
  std::vector<float>* m_truth_vis_pt;
  std::vector<float>* m_truth_vis_e;
  std::vector<int>* m_truth_charge;
  std::vector<int>* m_truth_pdgid;
  std::vector<int>* m_truth_barcode;
  std::vector<int>* m_truth_nChildren;
  std::vector<int>* m_truth_nTauProngs;
  std::vector<int>* m_truth_nPi0;
  std::vector<int>* m_truth_isTauHad;
  std::vector<int>* m_truth_parent_pdgid;
  std::vector<int>* m_truth_parent_barcode;
  std::vector<float>* m_truth_properTime;
  std::vector<float>* m_truth_decayr;

  // track
  std::vector<int>* m_track_isTracklet;
  std::vector<float>* m_track_phi;
  std::vector<float>* m_track_eta;
  std::vector<float>* m_track_pt;
  std::vector<float>* m_track_e;
  std::vector<int>* m_track_charge;
  std::vector<float>* m_track_KVUPhi;
  std::vector<float>* m_track_KVUEta;
  std::vector<float>* m_track_KVUPt;
  std::vector<int>* m_track_KVUCharge;
  std::vector<float>* m_track_KVUChi2Prob;
  std::vector<int>* m_track_pixelBarrel_0;
  std::vector<int>* m_track_pixelBarrel_1;
  std::vector<int>* m_track_pixelBarrel_2;
  std::vector<int>* m_track_pixelBarrel_3;
  std::vector<int>* m_track_sctBarrel_0;
  std::vector<int>* m_track_sctBarrel_1;
  std::vector<int>* m_track_sctBarrel_2;
  std::vector<int>* m_track_sctBarrel_3;
  std::vector<int>* m_track_sctEndcap_0;
  std::vector<int>* m_track_sctEndcap_1;
  std::vector<int>* m_track_sctEndcap_2;
  std::vector<int>* m_track_sctEndcap_3;
  std::vector<int>* m_track_sctEndcap_4;
  std::vector<int>* m_track_sctEndcap_5;
  std::vector<int>* m_track_sctEndcap_6;
  std::vector<int>* m_track_sctEndcap_7;
  std::vector<int>* m_track_sctEndcap_8;
  std::vector<int>* m_track_nPixelEndcap;
  std::vector<int>* m_track_nPixHits;
  std::vector<int>* m_track_nPixHoles;
  std::vector<int>* m_track_nPixSharedHits;
  std::vector<int>* m_track_nPixOutliers;
  std::vector<int>* m_track_nPixDeadSensors;
  std::vector<int>* m_track_nContribPixelLayers;
  std::vector<int>* m_track_nGangedFlaggedFakes;
  std::vector<int>* m_track_nPixelSpoiltHits;
  std::vector<int>* m_track_nSCTHits;
  std::vector<int>* m_track_nSCTHoles;
  std::vector<int>* m_track_nSCTSharedHits;
  std::vector<int>* m_track_nSCTOutliers;
  std::vector<int>* m_track_nSCTDeadSensors;
  std::vector<int>* m_track_nTRTHits;
  std::vector<int>* m_track_nTRTHoles;
  std::vector<int>* m_track_nTRTSharedHits;
  std::vector<int>* m_track_nTRTDeadSensors;
  std::vector<int>* m_track_nTRTOutliers;
  std::vector<float>* m_track_dEdx;
  std::vector<float>* m_track_ptCone20OverPt;
  std::vector<float>* m_track_ptCone30OverPt;
  std::vector<float>* m_track_ptCone40OverPt;
  std::vector<float>* m_track_ptCone20OverKVUPt;
  std::vector<float>* m_track_ptCone30OverKVUPt;
  std::vector<float>* m_track_ptCone40OverKVUPt;
  std::vector<float>* m_track_eTTopoClus20;
  std::vector<float>* m_track_eTTopoClus30;
  std::vector<float>* m_track_eTTopoClus40;
  std::vector<float>* m_track_eTTopoCone20;
  std::vector<float>* m_track_eTTopoCone30;
  std::vector<float>* m_track_eTTopoCone40;
  std::vector<float>* m_track_dRJet20;
  std::vector<float>* m_track_dRJet50;
  std::vector<float>* m_track_dRMSTrack;
  std::vector<float>* m_track_dRMuon;
  std::vector<float>* m_track_dRElectron;
  std::vector<float>* m_track_d0;
  std::vector<float>* m_track_d0Sig;
  std::vector<float>* m_track_z0;
  std::vector<float>* m_track_z0PV;
  std::vector<float>* m_track_z0PVSinTheta;
  std::vector<float>* m_track_chi2Prob;
  // std::vector<std::vector<float>>* m_track_parameterCovMatrix;
  std::vector<float>* m_track_truth_phi;
  std::vector<int>* m_track_electron_key;
  std::vector<int>* m_track_muon_key;
  std::vector<float>* m_track_truth_eta;
  std::vector<float>* m_track_truth_pt;
  std::vector<float>* m_track_truth_e;
  std::vector<int>* m_track_truth_charge;
  std::vector<int>* m_track_truth_pdgid;
  std::vector<int>* m_track_truth_barcode;
  std::vector<int>* m_track_truth_nChildren;
  std::vector<float>* m_track_truth_properTime;
  std::vector<float>* m_track_truth_decayr;
  std::vector<float>* m_track_truth_matchingProb;
  std::vector<float>* m_track_truth_parent_phi;
  std::vector<float>* m_track_truth_parent_eta;
  std::vector<float>* m_track_truth_parent_pt;
  std::vector<float>* m_track_truth_parent_e;
  std::vector<int>* m_track_truth_parent_charge;
  std::vector<int>* m_track_truth_parent_pdgid;
  std::vector<int>* m_track_truth_parent_barcode;
  std::vector<int>* m_track_truth_parent_nChildren;
  std::vector<float>* m_track_truth_parent_decayr;


  // Declaration of leaf types
  Bool_t          HLT_xe70;
  Bool_t          HLT_xe70_tc_lcw;
  Bool_t          HLT_xe70_mht;
  Bool_t          HLT_xe80_tc_lcw_L1XE50;
  Bool_t          HLT_xe90_tc_lcw_L1XE50;
  Bool_t          HLT_xe90_mht_L1XE50;
  Bool_t          HLT_xe100_mht_L1XE50;
  Bool_t          HLT_xe100;
  Bool_t          HLT_xe100_tc_lcw;
  Bool_t          HLT_xe110_mht_L1XE50;
  Bool_t          HLT_xe110_mht_xe70_L1XE50;
  Bool_t          HLT_xe120_mht;
  Bool_t          HLT_xe120_pueta;
  Bool_t          HLT_xe120_pufit;
  Bool_t          HLT_noalg_L1J400;
  Bool_t          HLT_xe90_pufit_L1XE50;
  Bool_t          HLT_xe100_pufit_L1XE50;
  Bool_t          HLT_xe100_pufit_L1XE55;
  Bool_t          HLT_xe110_pufit_L1XE50;
  Bool_t          HLT_xe110_pufit_L1XE55;
  Bool_t          HLT_xe110_pufit_xe65_L1XE50;
  Bool_t          HLT_xe110_pufit_xe70_L1XE50;
  Bool_t          HLT_xe120_pufit_L1XE50;
  Bool_t          HLT_j80_xe80;
  Bool_t          HLT_j100_xe80;
  Bool_t          HLT_j120_xe80;
  Bool_t          HLT_j40_cleanT_xe80_L1XE60;
  Bool_t          HLT_e24_lhmedium_L1EM20VH;
  Bool_t          HLT_e24_lhmedium_L1EM18VH;
  Bool_t          HLT_e24_lhmedium_nod0_ivarloose;
  Bool_t          HLT_e24_lhtight_nod0_ivarloose;
  Bool_t          HLT_e26_lhtight_nod0_ivarloose;
  Bool_t          HLT_e60_lhmedium;
  Bool_t          HLT_e60_lhmedium_nod0;
  Bool_t          HLT_e60_medium;
  Bool_t          HLT_e120_lhloose;
  Bool_t          HLT_e140_lhloose_nod0;
  Bool_t          HLT_e300_etcut;
  Bool_t          HLT_mu20_iloose_L1MU15;
  Bool_t          HLT_mu24_iloose;
  Bool_t          HLT_mu24_iloose_L1MU15;
  Bool_t          HLT_mu24_ivarloose;
  Bool_t          HLT_mu24_ivarloose_L1MU15;
  Bool_t          HLT_mu24_imedium;
  Bool_t          HLT_mu24_ivarmedium;
  Bool_t          HLT_mu26_imedium;
  Bool_t          HLT_mu40;
  Bool_t          HLT_mu26_ivarmedium;
  Bool_t          HLT_mu50;
  Bool_t          HLT_mu60_0eta105_msonly;
  Bool_t          LU_METtrigger_SUSYTools;
  Bool_t          LU_SingleElectrontrigger;
  Bool_t          LU_SingleMuontrigger;
  Int_t           Tracks_;
  UInt_t          Tracks_fUniqueID[kMaxTracks];   //[Tracks_]
  UInt_t          Tracks_fBits[kMaxTracks];   //[Tracks_]
  TLorentzVector  Tracks_p4[kMaxTracks];
  Bool_t          Tracks_ReTracking[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_PixelTracklet[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_d0[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_z0[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_theta[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_phi[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_qoverp[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_qoverpt[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_z0wrtPV[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_z0sinthetawrtPV[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_d0err[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_z0err[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_thetaerr[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_phierr[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_qoverperr[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_z0wrtPVerr[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_z0wrtPVsig[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_z0sinthetawrtPVsig[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_d0sigTool[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_d0sigToolBeamErr[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_BeamErr[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_charge[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_pixeldEdx[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_chiSquared[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_Quality[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_chi2OvernDoF[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_nDoF[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_truthMatchingProbability[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_ptcone20_1gev[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_ptcone30_1gev[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_ptcone40_1gev[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_ptcone20overPt_1gev[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_ptcone30overPt_1gev[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_ptcone40overPt_1gev[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_ptvarcone20[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_ptvarcone30[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_ptvarcone40[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_ptvarcone20overPt[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_ptvarcone30overPt[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_ptvarcone40overPt[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_etcone20_topo[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_etcone30_topo[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_etcone40_topo[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_etcone20overPt_topo[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_etcone30overPt_topo[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_etcone40overPt_topo[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_etclus20_topo[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_etclus30_topo[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_etclus40_topo[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_etclus20overPt_topo[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_etclus30overPt_topo[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_etclus40overPt_topo[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_nearestClusterdR_calo[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_nearestClusteret_calo[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_nearestClusterdR_topo[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_nearestClusteret_topo[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_dRJet20[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_dRJet50[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_dRElectron[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_dRMuon[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_dRMSTrack[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_dRTrack_5gev[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_dRTrack_10gev[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_KVUPt[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_KVUEta[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_KVUPhi[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_KVUd0[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_KVUz0[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_KVUtheta[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_KVUphi[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_KVUqoverp[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_KVUqoverpt[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_KVUz0wrtPV[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_KVUz0sinthetawrtPV[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_KVUd0err[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_KVUz0err[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_KVUthetaerr[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_KVUphierr[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_KVUqoverperr[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_KVUz0wrtPVerr[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_KVUz0wrtPVsig[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_KVUz0sinthetawrtPVsig[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_KVUd0sigTool[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_KVUd0sigToolBeamErr[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_KVUBeamErr[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_KVUchiSquared[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_KVUQuality[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_KVUchi2OvernDoF[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_numberOfContribPixelLayers[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_nBLayerHits[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_nPixHits[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_nSCTHits[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_nTRTHits[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_nSiHits[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_nBLayerSharedHits[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_nPixelSharedHits[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_nSCTSharedHits[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_nTRTSharedHits[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_nSiSharedHits[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_nPixelHoles[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_nSCTHoles[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_nSCTDoubleHoles[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_nTRTHoles[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_nSiHoles[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_nBLayerOutliers[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_nPixelOutliers[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_nSCTOutliers[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_nTRTOutliers[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_nSiOutliers[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_nBLayerHitsPlusOutlier[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_nPixHitsPlusOutlier[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_nSCTHitsPlusOutlier[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_nTRTHitsPlusOutlier[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_nSiHitsPlusOutlier[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_numberOfPixelSpoiltHits[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_numberOfSCTSpoiltHits[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_expectBLayerHit[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_expectInnermostPixelLayerHit[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_numberOfInnermostPixelLayerHits[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_numberOfGangedPixels[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_numberOfGangedFlaggedFakes[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_numberOfPixelDeadSensors[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_numberOfSCTDeadSensors[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_numberOfTRTDeadStraws[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_MuonSegment40_nPrecisionHits[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_MuonSegment40_nPhiLayers[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_MuonSegment40_nTrigEtaLayers[kMaxTracks];   //[Tracks_]
  Int_t           Tracks_MuonSegment40_nAllHits[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_IsPassedSR[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_IsPassedFakeCR[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_IsPassedHadCR[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_IsPassedEleCRTag[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_IsPassedEleCRProbe[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_IsPassedMuCRTag[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_IsPassedMuCRProbe[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_IsPassedIsolated[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_IsPassedIsolatedLeading[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_IsPassedORJet[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_IsPassedORLep[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_IsPassedQuality[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_IsPassedBadHitsVeto[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_IsPassedEta[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_IsPassedImpactParameter[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_IsPassedTRTVeto[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_IsPassedSCTVeto[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_pixelBarrel0[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_pixelBarrel1[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_pixelBarrel2[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_pixelBarrel3[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_pixelEndCap0[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_pixelEndCap1[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_pixelEndCap2[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_sctBarrel0[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_sctBarrel1[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_sctBarrel2[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_sctBarrel3[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_sctEndCap0[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_sctEndCap1[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_sctEndCap2[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_sctEndCap3[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_sctEndCap4[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_sctEndCap5[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_sctEndCap6[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_sctEndCap7[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_sctEndCap8[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_trtBarrel[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_trtEndCap[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_DBM0[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_DBM1[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_DBM2[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_DecayRadius[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_DecayProperTime[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_deltaPt[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_deltaQoverP[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_deltaQoverPt[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_deltaEta[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_deltaTheta[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_deltaPhi[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_deltaR[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_deltaQoverPSig[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_deltaThetaSig[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_deltaPhiSig[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_TruthPt[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_TruthEta[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_TruthPhi[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_TruthCharge[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_TruthMass[kMaxTracks];   //[Tracks_]
  Float_t         Tracks_TruthFromZ[kMaxTracks];   //[Tracks_]
  ULong_t         Tracks_index_Truth[kMaxTracks];   //[Tracks_]
  std::string          Tracks_TruthType[kMaxTracks];
  Bool_t          Tracks_TruthDAODtruth[kMaxTracks];   //[Tracks_]
  Bool_t          Tracks_TruthORtruthJet[kMaxTracks];   //[Tracks_]
  std::string          Tracks_TruthOrigin[kMaxTracks];
  std::vector<std::string>  Tracks_syst_name[kMaxTracks];
  std::vector<float>   Tracks_syst_dRJet20[kMaxTracks];
  std::vector<float>   Tracks_syst_dRJet50[kMaxTracks];
  std::vector<float>   Tracks_syst_dRElectron[kMaxTracks];
  std::vector<float>   Tracks_syst_dRMuon[kMaxTracks];
  std::vector<float>   Tracks_syst_dRMSTrack[kMaxTracks];
  Int_t           ThreeLayerTracks_;
  UInt_t          ThreeLayerTracks_fUniqueID[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  UInt_t          ThreeLayerTracks_fBits[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  TLorentzVector  ThreeLayerTracks_p4[kMaxThreeLayerTracks];
  Bool_t          ThreeLayerTracks_ReTracking[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_PixelTracklet[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_d0[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_z0[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_theta[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_phi[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_qoverp[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_qoverpt[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_z0wrtPV[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_z0sinthetawrtPV[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_d0err[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_z0err[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_thetaerr[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_phierr[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_qoverperr[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_z0wrtPVerr[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_z0wrtPVsig[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_z0sinthetawrtPVsig[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_d0sigTool[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_d0sigToolBeamErr[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_BeamErr[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_charge[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_pixeldEdx[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_chiSquared[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_Quality[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_chi2OvernDoF[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_nDoF[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_truthMatchingProbability[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_ptcone20_1gev[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_ptcone30_1gev[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_ptcone40_1gev[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_ptcone20overPt_1gev[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_ptcone30overPt_1gev[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_ptcone40overPt_1gev[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_ptvarcone20[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_ptvarcone30[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_ptvarcone40[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_ptvarcone20overPt[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_ptvarcone30overPt[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_ptvarcone40overPt[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_etcone20_topo[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_etcone30_topo[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_etcone40_topo[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_etcone20overPt_topo[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_etcone30overPt_topo[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_etcone40overPt_topo[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_etclus20_topo[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_etclus30_topo[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_etclus40_topo[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_etclus20overPt_topo[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_etclus30overPt_topo[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_etclus40overPt_topo[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_nearestClusterdR_calo[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_nearestClusteret_calo[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_nearestClusterdR_topo[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_nearestClusteret_topo[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_dRJet20[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_dRJet50[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_dRElectron[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_dRMuon[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_dRMSTrack[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_dRTrack_5gev[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_dRTrack_10gev[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_KVUPt[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_KVUEta[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_KVUPhi[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_KVUd0[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_KVUz0[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_KVUtheta[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_KVUphi[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_KVUqoverp[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_KVUqoverpt[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_KVUz0wrtPV[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_KVUz0sinthetawrtPV[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_KVUd0err[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_KVUz0err[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_KVUthetaerr[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_KVUphierr[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_KVUqoverperr[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_KVUz0wrtPVerr[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_KVUz0wrtPVsig[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_KVUz0sinthetawrtPVsig[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_KVUd0sigTool[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_KVUd0sigToolBeamErr[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_KVUBeamErr[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_KVUchiSquared[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_KVUQuality[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_KVUchi2OvernDoF[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_numberOfContribPixelLayers[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_nBLayerHits[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_nPixHits[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_nSCTHits[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_nTRTHits[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_nSiHits[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_nBLayerSharedHits[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_nPixelSharedHits[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_nSCTSharedHits[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_nTRTSharedHits[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_nSiSharedHits[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_nPixelHoles[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_nSCTHoles[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_nSCTDoubleHoles[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_nTRTHoles[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_nSiHoles[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_nBLayerOutliers[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_nPixelOutliers[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_nSCTOutliers[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_nTRTOutliers[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_nSiOutliers[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_nBLayerHitsPlusOutlier[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_nPixHitsPlusOutlier[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_nSCTHitsPlusOutlier[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_nTRTHitsPlusOutlier[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_nSiHitsPlusOutlier[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_numberOfPixelSpoiltHits[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_numberOfSCTSpoiltHits[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_expectBLayerHit[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_expectInnermostPixelLayerHit[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_numberOfInnermostPixelLayerHits[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_numberOfGangedPixels[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_numberOfGangedFlaggedFakes[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_numberOfPixelDeadSensors[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_numberOfSCTDeadSensors[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_numberOfTRTDeadStraws[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_MuonSegment40_nPrecisionHits[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_MuonSegment40_nPhiLayers[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_MuonSegment40_nTrigEtaLayers[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Int_t           ThreeLayerTracks_MuonSegment40_nAllHits[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_IsPassedSR[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_IsPassedFakeCR[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_IsPassedHadCR[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_IsPassedEleCRTag[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_IsPassedEleCRProbe[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_IsPassedMuCRTag[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_IsPassedMuCRProbe[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_IsPassedIsolated[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_IsPassedIsolatedLeading[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_IsPassedORJet[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_IsPassedORLep[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_IsPassedQuality[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_IsPassedBadHitsVeto[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_IsPassedEta[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_IsPassedImpactParameter[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_IsPassedTRTVeto[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_IsPassedSCTVeto[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_pixelBarrel0[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_pixelBarrel1[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_pixelBarrel2[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_pixelBarrel3[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_pixelEndCap0[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_pixelEndCap1[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_pixelEndCap2[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_sctBarrel0[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_sctBarrel1[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_sctBarrel2[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_sctBarrel3[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_sctEndCap0[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_sctEndCap1[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_sctEndCap2[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_sctEndCap3[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_sctEndCap4[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_sctEndCap5[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_sctEndCap6[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_sctEndCap7[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_sctEndCap8[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_trtBarrel[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_trtEndCap[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_DBM0[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_DBM1[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_DBM2[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_DecayRadius[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_DecayProperTime[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_deltaPt[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_deltaQoverP[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_deltaQoverPt[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_deltaEta[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_deltaTheta[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_deltaPhi[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_deltaR[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_deltaQoverPSig[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_deltaThetaSig[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_deltaPhiSig[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_TruthPt[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_TruthEta[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_TruthPhi[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_TruthCharge[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_TruthMass[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Float_t         ThreeLayerTracks_TruthFromZ[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  ULong_t         ThreeLayerTracks_index_Truth[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  std::string          ThreeLayerTracks_TruthType[kMaxThreeLayerTracks];
  Bool_t          ThreeLayerTracks_TruthDAODtruth[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  Bool_t          ThreeLayerTracks_TruthORtruthJet[kMaxThreeLayerTracks];   //[ThreeLayerTracks_]
  std::string          ThreeLayerTracks_TruthOrigin[kMaxThreeLayerTracks];
  std::vector<std::string>  ThreeLayerTracks_syst_name[kMaxThreeLayerTracks];
  std::vector<float>   ThreeLayerTracks_syst_dRJet20[kMaxThreeLayerTracks];
  std::vector<float>   ThreeLayerTracks_syst_dRJet50[kMaxThreeLayerTracks];
  std::vector<float>   ThreeLayerTracks_syst_dRElectron[kMaxThreeLayerTracks];
  std::vector<float>   ThreeLayerTracks_syst_dRMuon[kMaxThreeLayerTracks];
  std::vector<float>   ThreeLayerTracks_syst_dRMSTrack[kMaxThreeLayerTracks];
  Int_t           FourLayerTracks_;
  UInt_t          FourLayerTracks_fUniqueID[kMaxFourLayerTracks];   //[FourLayerTracks_]
  UInt_t          FourLayerTracks_fBits[kMaxFourLayerTracks];   //[FourLayerTracks_]
  TLorentzVector  FourLayerTracks_p4[kMaxFourLayerTracks];
  Bool_t          FourLayerTracks_ReTracking[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_PixelTracklet[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_d0[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_z0[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_theta[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_phi[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_qoverp[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_qoverpt[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_z0wrtPV[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_z0sinthetawrtPV[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_d0err[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_z0err[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_thetaerr[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_phierr[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_qoverperr[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_z0wrtPVerr[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_z0wrtPVsig[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_z0sinthetawrtPVsig[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_d0sigTool[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_d0sigToolBeamErr[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_BeamErr[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_charge[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_pixeldEdx[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_chiSquared[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_Quality[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_chi2OvernDoF[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_nDoF[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_truthMatchingProbability[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_ptcone20_1gev[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_ptcone30_1gev[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_ptcone40_1gev[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_ptcone20overPt_1gev[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_ptcone30overPt_1gev[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_ptcone40overPt_1gev[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_ptvarcone20[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_ptvarcone30[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_ptvarcone40[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_ptvarcone20overPt[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_ptvarcone30overPt[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_ptvarcone40overPt[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_etcone20_topo[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_etcone30_topo[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_etcone40_topo[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_etcone20overPt_topo[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_etcone30overPt_topo[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_etcone40overPt_topo[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_etclus20_topo[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_etclus30_topo[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_etclus40_topo[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_etclus20overPt_topo[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_etclus30overPt_topo[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_etclus40overPt_topo[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_nearestClusterdR_calo[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_nearestClusteret_calo[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_nearestClusterdR_topo[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_nearestClusteret_topo[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_dRJet20[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_dRJet50[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_dRElectron[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_dRMuon[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_dRMSTrack[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_dRTrack_5gev[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_dRTrack_10gev[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_KVUPt[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_KVUEta[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_KVUPhi[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_KVUd0[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_KVUz0[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_KVUtheta[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_KVUphi[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_KVUqoverp[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_KVUqoverpt[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_KVUz0wrtPV[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_KVUz0sinthetawrtPV[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_KVUd0err[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_KVUz0err[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_KVUthetaerr[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_KVUphierr[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_KVUqoverperr[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_KVUz0wrtPVerr[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_KVUz0wrtPVsig[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_KVUz0sinthetawrtPVsig[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_KVUd0sigTool[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_KVUd0sigToolBeamErr[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_KVUBeamErr[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_KVUchiSquared[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_KVUQuality[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_KVUchi2OvernDoF[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_numberOfContribPixelLayers[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_nBLayerHits[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_nPixHits[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_nSCTHits[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_nTRTHits[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_nSiHits[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_nBLayerSharedHits[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_nPixelSharedHits[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_nSCTSharedHits[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_nTRTSharedHits[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_nSiSharedHits[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_nPixelHoles[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_nSCTHoles[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_nSCTDoubleHoles[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_nTRTHoles[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_nSiHoles[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_nBLayerOutliers[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_nPixelOutliers[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_nSCTOutliers[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_nTRTOutliers[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_nSiOutliers[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_nBLayerHitsPlusOutlier[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_nPixHitsPlusOutlier[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_nSCTHitsPlusOutlier[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_nTRTHitsPlusOutlier[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_nSiHitsPlusOutlier[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_numberOfPixelSpoiltHits[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_numberOfSCTSpoiltHits[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_expectBLayerHit[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_expectInnermostPixelLayerHit[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_numberOfInnermostPixelLayerHits[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_numberOfGangedPixels[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_numberOfGangedFlaggedFakes[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_numberOfPixelDeadSensors[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_numberOfSCTDeadSensors[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_numberOfTRTDeadStraws[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_MuonSegment40_nPrecisionHits[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_MuonSegment40_nPhiLayers[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_MuonSegment40_nTrigEtaLayers[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Int_t           FourLayerTracks_MuonSegment40_nAllHits[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_IsPassedSR[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_IsPassedFakeCR[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_IsPassedHadCR[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_IsPassedEleCRTag[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_IsPassedEleCRProbe[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_IsPassedMuCRTag[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_IsPassedMuCRProbe[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_IsPassedIsolated[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_IsPassedIsolatedLeading[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_IsPassedORJet[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_IsPassedORLep[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_IsPassedQuality[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_IsPassedBadHitsVeto[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_IsPassedEta[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_IsPassedImpactParameter[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_IsPassedTRTVeto[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_IsPassedSCTVeto[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_pixelBarrel0[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_pixelBarrel1[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_pixelBarrel2[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_pixelBarrel3[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_pixelEndCap0[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_pixelEndCap1[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_pixelEndCap2[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_sctBarrel0[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_sctBarrel1[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_sctBarrel2[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_sctBarrel3[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_sctEndCap0[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_sctEndCap1[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_sctEndCap2[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_sctEndCap3[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_sctEndCap4[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_sctEndCap5[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_sctEndCap6[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_sctEndCap7[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_sctEndCap8[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_trtBarrel[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_trtEndCap[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_DBM0[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_DBM1[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_DBM2[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_DecayRadius[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_DecayProperTime[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_deltaPt[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_deltaQoverP[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_deltaQoverPt[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_deltaEta[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_deltaTheta[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_deltaPhi[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_deltaR[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_deltaQoverPSig[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_deltaThetaSig[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_deltaPhiSig[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_TruthPt[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_TruthEta[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_TruthPhi[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_TruthCharge[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_TruthMass[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Float_t         FourLayerTracks_TruthFromZ[kMaxFourLayerTracks];   //[FourLayerTracks_]
  ULong_t         FourLayerTracks_index_Truth[kMaxFourLayerTracks];   //[FourLayerTracks_]
  std::string          FourLayerTracks_TruthType[kMaxFourLayerTracks];
  Bool_t          FourLayerTracks_TruthDAODtruth[kMaxFourLayerTracks];   //[FourLayerTracks_]
  Bool_t          FourLayerTracks_TruthORtruthJet[kMaxFourLayerTracks];   //[FourLayerTracks_]
  std::string          FourLayerTracks_TruthOrigin[kMaxFourLayerTracks];
  std::vector<std::string>  FourLayerTracks_syst_name[kMaxFourLayerTracks];
  std::vector<float>   FourLayerTracks_syst_dRJet20[kMaxFourLayerTracks];
  std::vector<float>   FourLayerTracks_syst_dRJet50[kMaxFourLayerTracks];
  std::vector<float>   FourLayerTracks_syst_dRElectron[kMaxFourLayerTracks];
  std::vector<float>   FourLayerTracks_syst_dRMuon[kMaxFourLayerTracks];
  std::vector<float>   FourLayerTracks_syst_dRMSTrack[kMaxFourLayerTracks];
  Int_t           DRAWJets_;
  UInt_t          DRAWJets_fUniqueID[kMaxDRAWJets];   //[DRAWJets_]
  UInt_t          DRAWJets_fBits[kMaxDRAWJets];   //[DRAWJets_]
  TLorentzVector  DRAWJets_p4[kMaxDRAWJets];
  Bool_t          DRAWJets_IsSignal[kMaxDRAWJets];   //[DRAWJets_]
  Bool_t          DRAWJets_BTagged[kMaxDRAWJets];   //[DRAWJets_]
  Int_t           DRAWJets_Quality[kMaxDRAWJets];   //[DRAWJets_]
  Int_t           DRAWJets_TruthLabelID[kMaxDRAWJets];   //[DRAWJets_]
  Int_t           RawJets_;
  UInt_t          RawJets_fUniqueID[kMaxRawJets];   //[RawJets_]
  UInt_t          RawJets_fBits[kMaxRawJets];   //[RawJets_]
  TLorentzVector  RawJets_p4[kMaxRawJets];
  Bool_t          RawJets_IsSignal[kMaxRawJets];   //[RawJets_]
  Bool_t          RawJets_BTagged[kMaxRawJets];   //[RawJets_]
  Int_t           RawJets_Quality[kMaxRawJets];   //[RawJets_]
  Int_t           RawJets_TruthLabelID[kMaxRawJets];   //[RawJets_]
  Int_t           GoodJets_;
  UInt_t          GoodJets_fUniqueID[kMaxGoodJets];   //[GoodJets_]
  UInt_t          GoodJets_fBits[kMaxGoodJets];   //[GoodJets_]
  TLorentzVector  GoodJets_p4[kMaxGoodJets];
  Bool_t          GoodJets_IsSignal[kMaxGoodJets];   //[GoodJets_]
  Bool_t          GoodJets_BTagged[kMaxGoodJets];   //[GoodJets_]
  Int_t           GoodJets_Quality[kMaxGoodJets];   //[GoodJets_]
  Int_t           GoodJets_TruthLabelID[kMaxGoodJets];   //[GoodJets_]
  Int_t           TruthJets_;
  UInt_t          TruthJets_fUniqueID[kMaxTruthJets];   //[TruthJets_]
  UInt_t          TruthJets_fBits[kMaxTruthJets];   //[TruthJets_]
  TLorentzVector  TruthJets_p4[kMaxTruthJets];
  Bool_t          TruthJets_IsSignal[kMaxTruthJets];   //[TruthJets_]
  Bool_t          TruthJets_BTagged[kMaxTruthJets];   //[TruthJets_]
  Int_t           TruthJets_Quality[kMaxTruthJets];   //[TruthJets_]
  Int_t           TruthJets_TruthLabelID[kMaxTruthJets];   //[TruthJets_]
  Int_t           GoodElectrons_;
  UInt_t          GoodElectrons_fUniqueID[kMaxGoodElectrons];   //[GoodElectrons_]
  UInt_t          GoodElectrons_fBits[kMaxGoodElectrons];   //[GoodElectrons_]
  TLorentzVector  GoodElectrons_p4[kMaxGoodElectrons];
  Bool_t          GoodElectrons_IsSignal[kMaxGoodElectrons];   //[GoodElectrons_]
  Int_t           GoodElectrons_Quality[kMaxGoodElectrons];   //[GoodElectrons_]
  Bool_t          GoodElectrons_IsGoodIP[kMaxGoodElectrons];   //[GoodElectrons_]
  Float_t         GoodElectrons_Charge[kMaxGoodElectrons];   //[GoodElectrons_]
  Float_t         GoodElectrons_TriggerSF[kMaxGoodElectrons];   //[GoodElectrons_]
  std::map<std::string,bool> GoodElectrons_triggerMatching[kMaxGoodElectrons];
  std::vector<unsigned long> GoodElectrons_index_IDTrack[kMaxGoodElectrons];
  Int_t           GoodMuons_;
  UInt_t          GoodMuons_fUniqueID[kMaxGoodMuons];   //[GoodMuons_]
  UInt_t          GoodMuons_fBits[kMaxGoodMuons];   //[GoodMuons_]
  TLorentzVector  GoodMuons_p4[kMaxGoodMuons];
  Bool_t          GoodMuons_IsSignal[kMaxGoodMuons];   //[GoodMuons_]
  Int_t           GoodMuons_Quality[kMaxGoodMuons];   //[GoodMuons_]
  Bool_t          GoodMuons_IsGoodIP[kMaxGoodMuons];   //[GoodMuons_]
  Float_t         GoodMuons_Charge[kMaxGoodMuons];   //[GoodMuons_]
  Float_t         GoodMuons_TriggerSF[kMaxGoodMuons];   //[GoodMuons_]
  std::map<std::string,bool> GoodMuons_triggerMatching[kMaxGoodMuons];
  std::vector<unsigned long> GoodMuons_index_IDTrack[kMaxGoodMuons];
  Int_t           GoodTaus_;
  UInt_t          GoodTaus_fUniqueID[kMaxGoodTaus];   //[GoodTaus_]
  UInt_t          GoodTaus_fBits[kMaxGoodTaus];   //[GoodTaus_]
  TLorentzVector  GoodTaus_p4[kMaxGoodTaus];
  Bool_t          GoodTaus_IsSignal[kMaxGoodTaus];   //[GoodTaus_]
  Int_t           GoodTaus_Quality[kMaxGoodTaus];   //[GoodTaus_]
  Bool_t          GoodTaus_IsGoodIP[kMaxGoodTaus];   //[GoodTaus_]
  Float_t         GoodTaus_Charge[kMaxGoodTaus];   //[GoodTaus_]
  Float_t         GoodTaus_TriggerSF[kMaxGoodTaus];   //[GoodTaus_]
  std::map<std::string,bool> GoodTaus_triggerMatching[kMaxGoodTaus];
  std::vector<unsigned long> GoodTaus_index_IDTrack[kMaxGoodTaus];
  Int_t           Zee_ProbeCluster_;
  UInt_t          Zee_ProbeCluster_fUniqueID[kMaxZee_ProbeCluster];   //[Zee_ProbeCluster_]
  UInt_t          Zee_ProbeCluster_fBits[kMaxZee_ProbeCluster];   //[Zee_ProbeCluster_]
  TLorentzVector  Zee_ProbeCluster_p4[kMaxZee_ProbeCluster];
  ULong_t         Zee_ProbeCluster_index_TagLepton[kMaxZee_ProbeCluster];   //[Zee_ProbeCluster_]
  ULong_t         Zee_ProbeCluster_index_ProbeIdTrack[kMaxZee_ProbeCluster];   //[Zee_ProbeCluster_]
  ULong_t         Zee_ProbeCluster_index_ProbeCaloCluster[kMaxZee_ProbeCluster];   //[Zee_ProbeCluster_]
  Int_t           Zee_ProbeTrack_;
  UInt_t          Zee_ProbeTrack_fUniqueID[kMaxZee_ProbeTrack];   //[Zee_ProbeTrack_]
  UInt_t          Zee_ProbeTrack_fBits[kMaxZee_ProbeTrack];   //[Zee_ProbeTrack_]
  TLorentzVector  Zee_ProbeTrack_p4[kMaxZee_ProbeTrack];
  ULong_t         Zee_ProbeTrack_index_TagLepton[kMaxZee_ProbeTrack];   //[Zee_ProbeTrack_]
  ULong_t         Zee_ProbeTrack_index_ProbeIdTrack[kMaxZee_ProbeTrack];   //[Zee_ProbeTrack_]
  ULong_t         Zee_ProbeTrack_index_ProbeCaloCluster[kMaxZee_ProbeTrack];   //[Zee_ProbeTrack_]
  Int_t           Zmumu_ProbeCluster_;
  UInt_t          Zmumu_ProbeCluster_fUniqueID[kMaxZmumu_ProbeCluster];   //[Zmumu_ProbeCluster_]
  UInt_t          Zmumu_ProbeCluster_fBits[kMaxZmumu_ProbeCluster];   //[Zmumu_ProbeCluster_]
  TLorentzVector  Zmumu_ProbeCluster_p4[kMaxZmumu_ProbeCluster];
  ULong_t         Zmumu_ProbeCluster_index_TagLepton[kMaxZmumu_ProbeCluster];   //[Zmumu_ProbeCluster_]
  ULong_t         Zmumu_ProbeCluster_index_ProbeIdTrack[kMaxZmumu_ProbeCluster];   //[Zmumu_ProbeCluster_]
  ULong_t         Zmumu_ProbeCluster_index_ProbeMSTrack[kMaxZmumu_ProbeCluster];   //[Zmumu_ProbeCluster_]
  Int_t           Zmumu_ProbeTrack_;
  UInt_t          Zmumu_ProbeTrack_fUniqueID[kMaxZmumu_ProbeTrack];   //[Zmumu_ProbeTrack_]
  UInt_t          Zmumu_ProbeTrack_fBits[kMaxZmumu_ProbeTrack];   //[Zmumu_ProbeTrack_]
  TLorentzVector  Zmumu_ProbeTrack_p4[kMaxZmumu_ProbeTrack];
  ULong_t         Zmumu_ProbeTrack_index_TagLepton[kMaxZmumu_ProbeTrack];   //[Zmumu_ProbeTrack_]
  ULong_t         Zmumu_ProbeTrack_index_ProbeIdTrack[kMaxZmumu_ProbeTrack];   //[Zmumu_ProbeTrack_]
  ULong_t         Zmumu_ProbeTrack_index_ProbeMSTrack[kMaxZmumu_ProbeTrack];   //[Zmumu_ProbeTrack_]
  Int_t           Truths_;
  UInt_t          Truths_fUniqueID[kMaxTruths];   //[Truths_]
  UInt_t          Truths_fBits[kMaxTruths];   //[Truths_]
  TLorentzVector  Truths_p4[kMaxTruths];
  Float_t         Truths_qoverp[kMaxTruths];   //[Truths_]
  Float_t         Truths_qoverpt[kMaxTruths];   //[Truths_]
  Float_t         Truths_ProperTime[kMaxTruths];   //[Truths_]
  Float_t         Truths_Mass[kMaxTruths];   //[Truths_]
  Float_t         Truths_Charge[kMaxTruths];   //[Truths_]
  Float_t         Truths_ProdRadius[kMaxTruths];   //[Truths_]
  Float_t         Truths_DecayRadius[kMaxTruths];   //[Truths_]
  Int_t           Truths_nChildren[kMaxTruths];   //[Truths_]
  Bool_t          Truths_FromZ[kMaxTruths];   //[Truths_]
  Float_t         Truths_PdgId[kMaxTruths];   //[Truths_]
  std::string          Truths_Type[kMaxTruths];
  Bool_t          Truths_DAODtruth[kMaxTruths];   //[Truths_]
  std::string          Truths_Origin[kMaxTruths];
  Bool_t          Truths_ORtruthJet[kMaxTruths];   //[Truths_]
  Double_t        Truths_pt_vis[kMaxTruths];   //[Truths_]
  Double_t        Truths_eta_vis[kMaxTruths];   //[Truths_]
  Double_t        Truths_phi_vis[kMaxTruths];   //[Truths_]
  Double_t        Truths_m_vis[kMaxTruths];   //[Truths_]
  ULong_t         Truths_numCharged[kMaxTruths];   //[Truths_]
  ULong_t         Truths_numChargedPion[kMaxTruths];   //[Truths_]
  Bool_t          Truths_HasHadronInChildren[kMaxTruths];   //[Truths_]
  Bool_t          Truths_HasElectronInChildren[kMaxTruths];   //[Truths_]
  Bool_t          Truths_HasMuonInChildren[kMaxTruths];   //[Truths_]
  Bool_t          Truths_HasPhotonInChildren[kMaxTruths];   //[Truths_]
  //std::vector<myTruth> Truths_DecayParticle[kMaxTruths];
  std::vector<float>   Truths_DecayParticle_dR[kMaxTruths];
  std::vector<float>   Truths_DecayParticle_dPt[kMaxTruths];
  //std::vector<myTruth> Truths_ChildParticle[kMaxTruths];
  Int_t           MSTracks_;
  UInt_t          MSTracks_fUniqueID[kMaxMSTracks];   //[MSTracks_]
  UInt_t          MSTracks_fBits[kMaxMSTracks];   //[MSTracks_]
  TLorentzVector  MSTracks_p4[kMaxMSTracks];
  Bool_t          MSTracks_ReTracking[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_PixelTracklet[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_d0[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_z0[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_theta[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_phi[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_qoverp[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_qoverpt[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_z0wrtPV[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_z0sinthetawrtPV[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_d0err[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_z0err[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_thetaerr[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_phierr[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_qoverperr[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_z0wrtPVerr[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_z0wrtPVsig[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_z0sinthetawrtPVsig[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_d0sigTool[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_d0sigToolBeamErr[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_BeamErr[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_charge[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_pixeldEdx[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_chiSquared[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_Quality[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_chi2OvernDoF[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_nDoF[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_truthMatchingProbability[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_ptcone20_1gev[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_ptcone30_1gev[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_ptcone40_1gev[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_ptcone20overPt_1gev[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_ptcone30overPt_1gev[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_ptcone40overPt_1gev[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_ptvarcone20[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_ptvarcone30[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_ptvarcone40[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_ptvarcone20overPt[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_ptvarcone30overPt[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_ptvarcone40overPt[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_etcone20_topo[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_etcone30_topo[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_etcone40_topo[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_etcone20overPt_topo[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_etcone30overPt_topo[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_etcone40overPt_topo[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_etclus20_topo[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_etclus30_topo[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_etclus40_topo[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_etclus20overPt_topo[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_etclus30overPt_topo[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_etclus40overPt_topo[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_nearestClusterdR_calo[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_nearestClusteret_calo[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_nearestClusterdR_topo[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_nearestClusteret_topo[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_dRJet20[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_dRJet50[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_dRElectron[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_dRMuon[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_dRMSTrack[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_dRTrack_5gev[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_dRTrack_10gev[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_KVUPt[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_KVUEta[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_KVUPhi[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_KVUd0[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_KVUz0[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_KVUtheta[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_KVUphi[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_KVUqoverp[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_KVUqoverpt[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_KVUz0wrtPV[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_KVUz0sinthetawrtPV[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_KVUd0err[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_KVUz0err[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_KVUthetaerr[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_KVUphierr[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_KVUqoverperr[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_KVUz0wrtPVerr[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_KVUz0wrtPVsig[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_KVUz0sinthetawrtPVsig[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_KVUd0sigTool[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_KVUd0sigToolBeamErr[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_KVUBeamErr[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_KVUchiSquared[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_KVUQuality[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_KVUchi2OvernDoF[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_numberOfContribPixelLayers[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_nBLayerHits[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_nPixHits[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_nSCTHits[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_nTRTHits[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_nSiHits[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_nBLayerSharedHits[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_nPixelSharedHits[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_nSCTSharedHits[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_nTRTSharedHits[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_nSiSharedHits[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_nPixelHoles[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_nSCTHoles[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_nSCTDoubleHoles[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_nTRTHoles[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_nSiHoles[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_nBLayerOutliers[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_nPixelOutliers[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_nSCTOutliers[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_nTRTOutliers[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_nSiOutliers[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_nBLayerHitsPlusOutlier[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_nPixHitsPlusOutlier[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_nSCTHitsPlusOutlier[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_nTRTHitsPlusOutlier[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_nSiHitsPlusOutlier[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_numberOfPixelSpoiltHits[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_numberOfSCTSpoiltHits[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_expectBLayerHit[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_expectInnermostPixelLayerHit[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_numberOfInnermostPixelLayerHits[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_numberOfGangedPixels[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_numberOfGangedFlaggedFakes[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_numberOfPixelDeadSensors[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_numberOfSCTDeadSensors[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_numberOfTRTDeadStraws[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_MuonSegment40_nPrecisionHits[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_MuonSegment40_nPhiLayers[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_MuonSegment40_nTrigEtaLayers[kMaxMSTracks];   //[MSTracks_]
  Int_t           MSTracks_MuonSegment40_nAllHits[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_IsPassedSR[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_IsPassedFakeCR[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_IsPassedHadCR[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_IsPassedEleCRTag[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_IsPassedEleCRProbe[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_IsPassedMuCRTag[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_IsPassedMuCRProbe[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_IsPassedIsolated[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_IsPassedIsolatedLeading[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_IsPassedORJet[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_IsPassedORLep[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_IsPassedQuality[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_IsPassedBadHitsVeto[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_IsPassedEta[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_IsPassedImpactParameter[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_IsPassedTRTVeto[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_IsPassedSCTVeto[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_pixelBarrel0[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_pixelBarrel1[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_pixelBarrel2[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_pixelBarrel3[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_pixelEndCap0[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_pixelEndCap1[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_pixelEndCap2[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_sctBarrel0[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_sctBarrel1[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_sctBarrel2[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_sctBarrel3[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_sctEndCap0[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_sctEndCap1[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_sctEndCap2[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_sctEndCap3[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_sctEndCap4[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_sctEndCap5[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_sctEndCap6[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_sctEndCap7[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_sctEndCap8[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_trtBarrel[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_trtEndCap[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_DBM0[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_DBM1[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_DBM2[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_DecayRadius[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_DecayProperTime[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_deltaPt[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_deltaQoverP[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_deltaQoverPt[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_deltaEta[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_deltaTheta[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_deltaPhi[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_deltaR[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_deltaQoverPSig[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_deltaThetaSig[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_deltaPhiSig[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_TruthPt[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_TruthEta[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_TruthPhi[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_TruthCharge[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_TruthMass[kMaxMSTracks];   //[MSTracks_]
  Float_t         MSTracks_TruthFromZ[kMaxMSTracks];   //[MSTracks_]
  ULong_t         MSTracks_index_Truth[kMaxMSTracks];   //[MSTracks_]
  std::string          MSTracks_TruthType[kMaxMSTracks];
  Bool_t          MSTracks_TruthDAODtruth[kMaxMSTracks];   //[MSTracks_]
  Bool_t          MSTracks_TruthORtruthJet[kMaxMSTracks];   //[MSTracks_]
  std::string          MSTracks_TruthOrigin[kMaxMSTracks];
  std::vector<std::string>  MSTracks_syst_name[kMaxMSTracks];
  std::vector<float>   MSTracks_syst_dRJet20[kMaxMSTracks];
  std::vector<float>   MSTracks_syst_dRJet50[kMaxMSTracks];
  std::vector<float>   MSTracks_syst_dRElectron[kMaxMSTracks];
  std::vector<float>   MSTracks_syst_dRMuon[kMaxMSTracks];
  std::vector<float>   MSTracks_syst_dRMSTrack[kMaxMSTracks];
  Int_t           EgammaClusters_;
  UInt_t          EgammaClusters_fUniqueID[kMaxEgammaClusters];   //[EgammaClusters_]
  UInt_t          EgammaClusters_fBits[kMaxEgammaClusters];   //[EgammaClusters_]
  TLorentzVector  EgammaClusters_p4[kMaxEgammaClusters];
  Float_t         EgammaClusters_ClusterSize[kMaxEgammaClusters];   //[EgammaClusters_]
  //myMET           *MET_;
  UInt_t          MET_PhysObjBase_fUniqueID;
  UInt_t          MET_PhysObjBase_fBits;
  TLorentzVector  MET_PhysObjBase_p4;
  //std::map<std::string,TStd::Vector2> MET_term;
  //myMET           *MET_ForEleCR_;
  UInt_t          MET_ForEleCR_PhysObjBase_fUniqueID;
  UInt_t          MET_ForEleCR_PhysObjBase_fBits;
  TLorentzVector  MET_ForEleCR_PhysObjBase_p4;
  //std::map<std::string,TStd::Vector2> MET_ForEleCR_term;
  //myMET           *MET_ForMuCR_;
  UInt_t          MET_ForMuCR_PhysObjBase_fUniqueID;
  UInt_t          MET_ForMuCR_PhysObjBase_fBits;
  TLorentzVector  MET_ForMuCR_PhysObjBase_p4;
  //std::map<std::string,TStd::Vector2> MET_ForMuCR_term;
  //myMET           *MET_Truth_;
  UInt_t          MET_Truth_PhysObjBase_fUniqueID;
  UInt_t          MET_Truth_PhysObjBase_fBits;
  TLorentzVector  MET_Truth_PhysObjBase_p4;
  //std::map<std::string,TStd::Vector2> MET_Truth_term;
  //myMET           *MET_ForXeTrigger_;
  UInt_t          MET_ForXeTrigger_PhysObjBase_fUniqueID;
  UInt_t          MET_ForXeTrigger_PhysObjBase_fBits;
  TLorentzVector  MET_ForXeTrigger_PhysObjBase_p4;
  //std::map<std::string,TStd::Vector2> MET_ForXeTrigger_term;
  //myMET           *MET_ForDRAW_;
  UInt_t          MET_ForDRAW_PhysObjBase_fUniqueID;
  UInt_t          MET_ForDRAW_PhysObjBase_fBits;
  TLorentzVector  MET_ForDRAW_PhysObjBase_p4;
  //std::map<std::string,TStd::Vector2> MET_ForDRAW_term;
  Bool_t          GRL;
  Bool_t          IsData;
  Bool_t          BadJet_LooseBad;
  Bool_t          IsPassedDRAWMuonVeto;
  Bool_t          IsPassedDRAWElectronVeto;
  Bool_t          IsPassedMETTrigger;
  Bool_t          IsPassedElectronTrigger;
  Bool_t          IsPassedMuonTrigger;
  Bool_t          IsPassedJetMET;
  Bool_t          IsPassedBadEventVeto;
  Bool_t          IsPassedLeptonVeto;
  Bool_t          IsPassedBadJet;
  Bool_t          IsPassedNCBVeto;
  Bool_t          IsPassedBadMuonVeto;
  Bool_t          IsPassedBadMuonMETCleaning;
  Bool_t          IsPassedCosmicMuonVeto;
  Bool_t          IsPassedAnyMETTrigger;
  Bool_t          IsPassedKinematics_EWK;
  Bool_t          IsPassedKinematics_Strong;
  Float_t         METTriggerEfficiency_EW;
  Float_t         METTriggerEfficiency_EW_up;
  Float_t         METTriggerEfficiency_EW_down;
  Float_t         METTriggerEfficiency_strongPt0;
  Float_t         METTriggerEfficiency_strongPt0_up;
  Float_t         METTriggerEfficiency_strongPt0_down;
  Float_t         METTriggerEfficiency_strongPt2;
  Float_t         METTriggerEfficiency_strongPt2_up;
  Float_t         METTriggerEfficiency_strongPt2_down;
  Float_t         METPt;
  Float_t         JetPt;
  Float_t         actualInteractionsPerCrossing;
  Float_t         averageInteractionsPerCrossing;
  Float_t         CorrectedAverageInteractionsPerCrossing;
  Float_t         nTracksFromPrimaryVertex;
  Float_t         weightXsec;
  Float_t         weightMCweight;
  Float_t         weightSherpaNjets;
  Float_t         weightPileupReweighting;
  Float_t         weightElectronSF;
  Float_t         weightElectronTriggerSF;
  Float_t         weightTagElectronTriggerSF;
  Float_t         weightMuonSF;
  Float_t         weightMuonTriggerSF;
  Float_t         weightTagMuonTriggerSF;
  Float_t         weightTauSF;
  Int_t           treatAsYear;
  Int_t           prodType;
  Int_t           DetectorError;
  UInt_t          randomRunNumber;
  UInt_t          RunNumber;
  UInt_t          lumiBlock;
  UInt_t          nPrimaryVertex;
  ULong64_t       EventNumber;
  std::vector<float>   *mcEventWeights;

  // List of branches
  TBranch        *b_HLT_xe70;   //!
  TBranch        *b_HLT_xe70_tc_lcw;   //!
  TBranch        *b_HLT_xe70_mht;   //!
  TBranch        *b_HLT_xe80_tc_lcw_L1XE50;   //!
  TBranch        *b_HLT_xe90_tc_lcw_L1XE50;   //!
  TBranch        *b_HLT_xe90_mht_L1XE50;   //!
  TBranch        *b_HLT_xe100_mht_L1XE50;   //!
  TBranch        *b_HLT_xe100;   //!
  TBranch        *b_HLT_xe100_tc_lcw;   //!
  TBranch        *b_HLT_xe110_mht_L1XE50;   //!
  TBranch        *b_HLT_xe110_mht_xe70_L1XE50;   //!
  TBranch        *b_HLT_xe120_mht;   //!
  TBranch        *b_HLT_xe120_pueta;   //!
  TBranch        *b_HLT_xe120_pufit;   //!
  TBranch        *b_HLT_noalg_L1J400;   //!
  TBranch        *b_HLT_xe90_pufit_L1XE50;   //!
  TBranch        *b_HLT_xe100_pufit_L1XE50;   //!
  TBranch        *b_HLT_xe100_pufit_L1XE55;   //!
  TBranch        *b_HLT_xe110_pufit_L1XE50;   //!
  TBranch        *b_HLT_xe110_pufit_L1XE55;   //!
  TBranch        *b_HLT_xe110_pufit_xe65_L1XE50;   //!
  TBranch        *b_HLT_xe110_pufit_xe70_L1XE50;   //!
  TBranch        *b_HLT_xe120_pufit_L1XE50;   //!
  TBranch        *b_HLT_j80_xe80;   //!
  TBranch        *b_HLT_j100_xe80;   //!
  TBranch        *b_HLT_j120_xe80;   //!
  TBranch        *b_HLT_j40_cleanT_xe80_L1XE60;   //!
  TBranch        *b_HLT_e24_lhmedium_L1EM20VH;   //!
  TBranch        *b_HLT_e24_lhmedium_L1EM18VH;   //!
  TBranch        *b_HLT_e24_lhmedium_nod0_ivarloose;   //!
  TBranch        *b_HLT_e24_lhtight_nod0_ivarloose;   //!
  TBranch        *b_HLT_e26_lhtight_nod0_ivarloose;   //!
  TBranch        *b_HLT_e60_lhmedium;   //!
  TBranch        *b_HLT_e60_lhmedium_nod0;   //!
  TBranch        *b_HLT_e60_medium;   //!
  TBranch        *b_HLT_e120_lhloose;   //!
  TBranch        *b_HLT_e140_lhloose_nod0;   //!
  TBranch        *b_HLT_e300_etcut;   //!
  TBranch        *b_HLT_mu20_iloose_L1MU15;   //!
  TBranch        *b_HLT_mu24_iloose;   //!
  TBranch        *b_HLT_mu24_iloose_L1MU15;   //!
  TBranch        *b_HLT_mu24_ivarloose;   //!
  TBranch        *b_HLT_mu24_ivarloose_L1MU15;   //!
  TBranch        *b_HLT_mu24_imedium;   //!
  TBranch        *b_HLT_mu24_ivarmedium;   //!
  TBranch        *b_HLT_mu26_imedium;   //!
  TBranch        *b_HLT_mu40;   //!
  TBranch        *b_HLT_mu26_ivarmedium;   //!
  TBranch        *b_HLT_mu50;   //!
  TBranch        *b_HLT_mu60_0eta105_msonly;   //!
  TBranch        *b_LU_METtrigger_SUSYTools;   //!
  TBranch        *b_LU_SingleElectrontrigger;   //!
  TBranch        *b_LU_SingleMuontrigger;   //!
  TBranch        *b_Tracks_;   //!
  TBranch        *b_Tracks_fUniqueID;   //!
  TBranch        *b_Tracks_fBits;   //!
  TBranch        *b_Tracks_p4;   //!
  TBranch        *b_Tracks_ReTracking;   //!
  TBranch        *b_Tracks_PixelTracklet;   //!
  TBranch        *b_Tracks_d0;   //!
  TBranch        *b_Tracks_z0;   //!
  TBranch        *b_Tracks_theta;   //!
  TBranch        *b_Tracks_phi;   //!
  TBranch        *b_Tracks_qoverp;   //!
  TBranch        *b_Tracks_qoverpt;   //!
  TBranch        *b_Tracks_z0wrtPV;   //!
  TBranch        *b_Tracks_z0sinthetawrtPV;   //!
  TBranch        *b_Tracks_d0err;   //!
  TBranch        *b_Tracks_z0err;   //!
  TBranch        *b_Tracks_thetaerr;   //!
  TBranch        *b_Tracks_phierr;   //!
  TBranch        *b_Tracks_qoverperr;   //!
  TBranch        *b_Tracks_z0wrtPVerr;   //!
  TBranch        *b_Tracks_z0wrtPVsig;   //!
  TBranch        *b_Tracks_z0sinthetawrtPVsig;   //!
  TBranch        *b_Tracks_d0sigTool;   //!
  TBranch        *b_Tracks_d0sigToolBeamErr;   //!
  TBranch        *b_Tracks_BeamErr;   //!
  TBranch        *b_Tracks_charge;   //!
  TBranch        *b_Tracks_pixeldEdx;   //!
  TBranch        *b_Tracks_chiSquared;   //!
  TBranch        *b_Tracks_Quality;   //!
  TBranch        *b_Tracks_chi2OvernDoF;   //!
  TBranch        *b_Tracks_nDoF;   //!
  TBranch        *b_Tracks_truthMatchingProbability;   //!
  TBranch        *b_Tracks_ptcone20_1gev;   //!
  TBranch        *b_Tracks_ptcone30_1gev;   //!
  TBranch        *b_Tracks_ptcone40_1gev;   //!
  TBranch        *b_Tracks_ptcone20overPt_1gev;   //!
  TBranch        *b_Tracks_ptcone30overPt_1gev;   //!
  TBranch        *b_Tracks_ptcone40overPt_1gev;   //!
  TBranch        *b_Tracks_ptvarcone20;   //!
  TBranch        *b_Tracks_ptvarcone30;   //!
  TBranch        *b_Tracks_ptvarcone40;   //!
  TBranch        *b_Tracks_ptvarcone20overPt;   //!
  TBranch        *b_Tracks_ptvarcone30overPt;   //!
  TBranch        *b_Tracks_ptvarcone40overPt;   //!
  TBranch        *b_Tracks_etcone20_topo;   //!
  TBranch        *b_Tracks_etcone30_topo;   //!
  TBranch        *b_Tracks_etcone40_topo;   //!
  TBranch        *b_Tracks_etcone20overPt_topo;   //!
  TBranch        *b_Tracks_etcone30overPt_topo;   //!
  TBranch        *b_Tracks_etcone40overPt_topo;   //!
  TBranch        *b_Tracks_etclus20_topo;   //!
  TBranch        *b_Tracks_etclus30_topo;   //!
  TBranch        *b_Tracks_etclus40_topo;   //!
  TBranch        *b_Tracks_etclus20overPt_topo;   //!
  TBranch        *b_Tracks_etclus30overPt_topo;   //!
  TBranch        *b_Tracks_etclus40overPt_topo;   //!
  TBranch        *b_Tracks_nearestClusterdR_calo;   //!
  TBranch        *b_Tracks_nearestClusteret_calo;   //!
  TBranch        *b_Tracks_nearestClusterdR_topo;   //!
  TBranch        *b_Tracks_nearestClusteret_topo;   //!
  TBranch        *b_Tracks_dRJet20;   //!
  TBranch        *b_Tracks_dRJet50;   //!
  TBranch        *b_Tracks_dRElectron;   //!
  TBranch        *b_Tracks_dRMuon;   //!
  TBranch        *b_Tracks_dRMSTrack;   //!
  TBranch        *b_Tracks_dRTrack_5gev;   //!
  TBranch        *b_Tracks_dRTrack_10gev;   //!
  TBranch        *b_Tracks_KVUPt;   //!
  TBranch        *b_Tracks_KVUEta;   //!
  TBranch        *b_Tracks_KVUPhi;   //!
  TBranch        *b_Tracks_KVUd0;   //!
  TBranch        *b_Tracks_KVUz0;   //!
  TBranch        *b_Tracks_KVUtheta;   //!
  TBranch        *b_Tracks_KVUphi;   //!
  TBranch        *b_Tracks_KVUqoverp;   //!
  TBranch        *b_Tracks_KVUqoverpt;   //!
  TBranch        *b_Tracks_KVUz0wrtPV;   //!
  TBranch        *b_Tracks_KVUz0sinthetawrtPV;   //!
  TBranch        *b_Tracks_KVUd0err;   //!
  TBranch        *b_Tracks_KVUz0err;   //!
  TBranch        *b_Tracks_KVUthetaerr;   //!
  TBranch        *b_Tracks_KVUphierr;   //!
  TBranch        *b_Tracks_KVUqoverperr;   //!
  TBranch        *b_Tracks_KVUz0wrtPVerr;   //!
  TBranch        *b_Tracks_KVUz0wrtPVsig;   //!
  TBranch        *b_Tracks_KVUz0sinthetawrtPVsig;   //!
  TBranch        *b_Tracks_KVUd0sigTool;   //!
  TBranch        *b_Tracks_KVUd0sigToolBeamErr;   //!
  TBranch        *b_Tracks_KVUBeamErr;   //!
  TBranch        *b_Tracks_KVUchiSquared;   //!
  TBranch        *b_Tracks_KVUQuality;   //!
  TBranch        *b_Tracks_KVUchi2OvernDoF;   //!
  TBranch        *b_Tracks_numberOfContribPixelLayers;   //!
  TBranch        *b_Tracks_nBLayerHits;   //!
  TBranch        *b_Tracks_nPixHits;   //!
  TBranch        *b_Tracks_nSCTHits;   //!
  TBranch        *b_Tracks_nTRTHits;   //!
  TBranch        *b_Tracks_nSiHits;   //!
  TBranch        *b_Tracks_nBLayerSharedHits;   //!
  TBranch        *b_Tracks_nPixelSharedHits;   //!
  TBranch        *b_Tracks_nSCTSharedHits;   //!
  TBranch        *b_Tracks_nTRTSharedHits;   //!
  TBranch        *b_Tracks_nSiSharedHits;   //!
  TBranch        *b_Tracks_nPixelHoles;   //!
  TBranch        *b_Tracks_nSCTHoles;   //!
  TBranch        *b_Tracks_nSCTDoubleHoles;   //!
  TBranch        *b_Tracks_nTRTHoles;   //!
  TBranch        *b_Tracks_nSiHoles;   //!
  TBranch        *b_Tracks_nBLayerOutliers;   //!
  TBranch        *b_Tracks_nPixelOutliers;   //!
  TBranch        *b_Tracks_nSCTOutliers;   //!
  TBranch        *b_Tracks_nTRTOutliers;   //!
  TBranch        *b_Tracks_nSiOutliers;   //!
  TBranch        *b_Tracks_nBLayerHitsPlusOutlier;   //!
  TBranch        *b_Tracks_nPixHitsPlusOutlier;   //!
  TBranch        *b_Tracks_nSCTHitsPlusOutlier;   //!
  TBranch        *b_Tracks_nTRTHitsPlusOutlier;   //!
  TBranch        *b_Tracks_nSiHitsPlusOutlier;   //!
  TBranch        *b_Tracks_numberOfPixelSpoiltHits;   //!
  TBranch        *b_Tracks_numberOfSCTSpoiltHits;   //!
  TBranch        *b_Tracks_expectBLayerHit;   //!
  TBranch        *b_Tracks_expectInnermostPixelLayerHit;   //!
  TBranch        *b_Tracks_numberOfInnermostPixelLayerHits;   //!
  TBranch        *b_Tracks_numberOfGangedPixels;   //!
  TBranch        *b_Tracks_numberOfGangedFlaggedFakes;   //!
  TBranch        *b_Tracks_numberOfPixelDeadSensors;   //!
  TBranch        *b_Tracks_numberOfSCTDeadSensors;   //!
  TBranch        *b_Tracks_numberOfTRTDeadStraws;   //!
  TBranch        *b_Tracks_MuonSegment40_nPrecisionHits;   //!
  TBranch        *b_Tracks_MuonSegment40_nPhiLayers;   //!
  TBranch        *b_Tracks_MuonSegment40_nTrigEtaLayers;   //!
  TBranch        *b_Tracks_MuonSegment40_nAllHits;   //!
  TBranch        *b_Tracks_IsPassedSR;   //!
  TBranch        *b_Tracks_IsPassedFakeCR;   //!
  TBranch        *b_Tracks_IsPassedHadCR;   //!
  TBranch        *b_Tracks_IsPassedEleCRTag;   //!
  TBranch        *b_Tracks_IsPassedEleCRProbe;   //!
  TBranch        *b_Tracks_IsPassedMuCRTag;   //!
  TBranch        *b_Tracks_IsPassedMuCRProbe;   //!
  TBranch        *b_Tracks_IsPassedIsolated;   //!
  TBranch        *b_Tracks_IsPassedIsolatedLeading;   //!
  TBranch        *b_Tracks_IsPassedORJet;   //!
  TBranch        *b_Tracks_IsPassedORLep;   //!
  TBranch        *b_Tracks_IsPassedQuality;   //!
  TBranch        *b_Tracks_IsPassedBadHitsVeto;   //!
  TBranch        *b_Tracks_IsPassedEta;   //!
  TBranch        *b_Tracks_IsPassedImpactParameter;   //!
  TBranch        *b_Tracks_IsPassedTRTVeto;   //!
  TBranch        *b_Tracks_IsPassedSCTVeto;   //!
  TBranch        *b_Tracks_pixelBarrel0;   //!
  TBranch        *b_Tracks_pixelBarrel1;   //!
  TBranch        *b_Tracks_pixelBarrel2;   //!
  TBranch        *b_Tracks_pixelBarrel3;   //!
  TBranch        *b_Tracks_pixelEndCap0;   //!
  TBranch        *b_Tracks_pixelEndCap1;   //!
  TBranch        *b_Tracks_pixelEndCap2;   //!
  TBranch        *b_Tracks_sctBarrel0;   //!
  TBranch        *b_Tracks_sctBarrel1;   //!
  TBranch        *b_Tracks_sctBarrel2;   //!
  TBranch        *b_Tracks_sctBarrel3;   //!
  TBranch        *b_Tracks_sctEndCap0;   //!
  TBranch        *b_Tracks_sctEndCap1;   //!
  TBranch        *b_Tracks_sctEndCap2;   //!
  TBranch        *b_Tracks_sctEndCap3;   //!
  TBranch        *b_Tracks_sctEndCap4;   //!
  TBranch        *b_Tracks_sctEndCap5;   //!
  TBranch        *b_Tracks_sctEndCap6;   //!
  TBranch        *b_Tracks_sctEndCap7;   //!
  TBranch        *b_Tracks_sctEndCap8;   //!
  TBranch        *b_Tracks_trtBarrel;   //!
  TBranch        *b_Tracks_trtEndCap;   //!
  TBranch        *b_Tracks_DBM0;   //!
  TBranch        *b_Tracks_DBM1;   //!
  TBranch        *b_Tracks_DBM2;   //!
  TBranch        *b_Tracks_DecayRadius;   //!
  TBranch        *b_Tracks_DecayProperTime;   //!
  TBranch        *b_Tracks_deltaPt;   //!
  TBranch        *b_Tracks_deltaQoverP;   //!
  TBranch        *b_Tracks_deltaQoverPt;   //!
  TBranch        *b_Tracks_deltaEta;   //!
  TBranch        *b_Tracks_deltaTheta;   //!
  TBranch        *b_Tracks_deltaPhi;   //!
  TBranch        *b_Tracks_deltaR;   //!
  TBranch        *b_Tracks_deltaQoverPSig;   //!
  TBranch        *b_Tracks_deltaThetaSig;   //!
  TBranch        *b_Tracks_deltaPhiSig;   //!
  TBranch        *b_Tracks_TruthPt;   //!
  TBranch        *b_Tracks_TruthEta;   //!
  TBranch        *b_Tracks_TruthPhi;   //!
  TBranch        *b_Tracks_TruthCharge;   //!
  TBranch        *b_Tracks_TruthMass;   //!
  TBranch        *b_Tracks_TruthFromZ;   //!
  TBranch        *b_Tracks_index_Truth;   //!
  TBranch        *b_Tracks_TruthType;   //!
  TBranch        *b_Tracks_TruthDAODtruth;   //!
  TBranch        *b_Tracks_TruthORtruthJet;   //!
  TBranch        *b_Tracks_TruthOrigin;   //!
  TBranch        *b_Tracks_syst_name;   //!
  TBranch        *b_Tracks_syst_dRJet20;   //!
  TBranch        *b_Tracks_syst_dRJet50;   //!
  TBranch        *b_Tracks_syst_dRElectron;   //!
  TBranch        *b_Tracks_syst_dRMuon;   //!
  TBranch        *b_Tracks_syst_dRMSTrack;   //!
  TBranch        *b_ThreeLayerTracks_;   //!
  TBranch        *b_ThreeLayerTracks_fUniqueID;   //!
  TBranch        *b_ThreeLayerTracks_fBits;   //!
  TBranch        *b_ThreeLayerTracks_p4;   //!
  TBranch        *b_ThreeLayerTracks_ReTracking;   //!
  TBranch        *b_ThreeLayerTracks_PixelTracklet;   //!
  TBranch        *b_ThreeLayerTracks_d0;   //!
  TBranch        *b_ThreeLayerTracks_z0;   //!
  TBranch        *b_ThreeLayerTracks_theta;   //!
  TBranch        *b_ThreeLayerTracks_phi;   //!
  TBranch        *b_ThreeLayerTracks_qoverp;   //!
  TBranch        *b_ThreeLayerTracks_qoverpt;   //!
  TBranch        *b_ThreeLayerTracks_z0wrtPV;   //!
  TBranch        *b_ThreeLayerTracks_z0sinthetawrtPV;   //!
  TBranch        *b_ThreeLayerTracks_d0err;   //!
  TBranch        *b_ThreeLayerTracks_z0err;   //!
  TBranch        *b_ThreeLayerTracks_thetaerr;   //!
  TBranch        *b_ThreeLayerTracks_phierr;   //!
  TBranch        *b_ThreeLayerTracks_qoverperr;   //!
  TBranch        *b_ThreeLayerTracks_z0wrtPVerr;   //!
  TBranch        *b_ThreeLayerTracks_z0wrtPVsig;   //!
  TBranch        *b_ThreeLayerTracks_z0sinthetawrtPVsig;   //!
  TBranch        *b_ThreeLayerTracks_d0sigTool;   //!
  TBranch        *b_ThreeLayerTracks_d0sigToolBeamErr;   //!
  TBranch        *b_ThreeLayerTracks_BeamErr;   //!
  TBranch        *b_ThreeLayerTracks_charge;   //!
  TBranch        *b_ThreeLayerTracks_pixeldEdx;   //!
  TBranch        *b_ThreeLayerTracks_chiSquared;   //!
  TBranch        *b_ThreeLayerTracks_Quality;   //!
  TBranch        *b_ThreeLayerTracks_chi2OvernDoF;   //!
  TBranch        *b_ThreeLayerTracks_nDoF;   //!
  TBranch        *b_ThreeLayerTracks_truthMatchingProbability;   //!
  TBranch        *b_ThreeLayerTracks_ptcone20_1gev;   //!
  TBranch        *b_ThreeLayerTracks_ptcone30_1gev;   //!
  TBranch        *b_ThreeLayerTracks_ptcone40_1gev;   //!
  TBranch        *b_ThreeLayerTracks_ptcone20overPt_1gev;   //!
  TBranch        *b_ThreeLayerTracks_ptcone30overPt_1gev;   //!
  TBranch        *b_ThreeLayerTracks_ptcone40overPt_1gev;   //!
  TBranch        *b_ThreeLayerTracks_ptvarcone20;   //!
  TBranch        *b_ThreeLayerTracks_ptvarcone30;   //!
  TBranch        *b_ThreeLayerTracks_ptvarcone40;   //!
  TBranch        *b_ThreeLayerTracks_ptvarcone20overPt;   //!
  TBranch        *b_ThreeLayerTracks_ptvarcone30overPt;   //!
  TBranch        *b_ThreeLayerTracks_ptvarcone40overPt;   //!
  TBranch        *b_ThreeLayerTracks_etcone20_topo;   //!
  TBranch        *b_ThreeLayerTracks_etcone30_topo;   //!
  TBranch        *b_ThreeLayerTracks_etcone40_topo;   //!
  TBranch        *b_ThreeLayerTracks_etcone20overPt_topo;   //!
  TBranch        *b_ThreeLayerTracks_etcone30overPt_topo;   //!
  TBranch        *b_ThreeLayerTracks_etcone40overPt_topo;   //!
  TBranch        *b_ThreeLayerTracks_etclus20_topo;   //!
  TBranch        *b_ThreeLayerTracks_etclus30_topo;   //!
  TBranch        *b_ThreeLayerTracks_etclus40_topo;   //!
  TBranch        *b_ThreeLayerTracks_etclus20overPt_topo;   //!
  TBranch        *b_ThreeLayerTracks_etclus30overPt_topo;   //!
  TBranch        *b_ThreeLayerTracks_etclus40overPt_topo;   //!
  TBranch        *b_ThreeLayerTracks_nearestClusterdR_calo;   //!
  TBranch        *b_ThreeLayerTracks_nearestClusteret_calo;   //!
  TBranch        *b_ThreeLayerTracks_nearestClusterdR_topo;   //!
  TBranch        *b_ThreeLayerTracks_nearestClusteret_topo;   //!
  TBranch        *b_ThreeLayerTracks_dRJet20;   //!
  TBranch        *b_ThreeLayerTracks_dRJet50;   //!
  TBranch        *b_ThreeLayerTracks_dRElectron;   //!
  TBranch        *b_ThreeLayerTracks_dRMuon;   //!
  TBranch        *b_ThreeLayerTracks_dRMSTrack;   //!
  TBranch        *b_ThreeLayerTracks_dRTrack_5gev;   //!
  TBranch        *b_ThreeLayerTracks_dRTrack_10gev;   //!
  TBranch        *b_ThreeLayerTracks_KVUPt;   //!
  TBranch        *b_ThreeLayerTracks_KVUEta;   //!
  TBranch        *b_ThreeLayerTracks_KVUPhi;   //!
  TBranch        *b_ThreeLayerTracks_KVUd0;   //!
  TBranch        *b_ThreeLayerTracks_KVUz0;   //!
  TBranch        *b_ThreeLayerTracks_KVUtheta;   //!
  TBranch        *b_ThreeLayerTracks_KVUphi;   //!
  TBranch        *b_ThreeLayerTracks_KVUqoverp;   //!
  TBranch        *b_ThreeLayerTracks_KVUqoverpt;   //!
  TBranch        *b_ThreeLayerTracks_KVUz0wrtPV;   //!
  TBranch        *b_ThreeLayerTracks_KVUz0sinthetawrtPV;   //!
  TBranch        *b_ThreeLayerTracks_KVUd0err;   //!
  TBranch        *b_ThreeLayerTracks_KVUz0err;   //!
  TBranch        *b_ThreeLayerTracks_KVUthetaerr;   //!
  TBranch        *b_ThreeLayerTracks_KVUphierr;   //!
  TBranch        *b_ThreeLayerTracks_KVUqoverperr;   //!
  TBranch        *b_ThreeLayerTracks_KVUz0wrtPVerr;   //!
  TBranch        *b_ThreeLayerTracks_KVUz0wrtPVsig;   //!
  TBranch        *b_ThreeLayerTracks_KVUz0sinthetawrtPVsig;   //!
  TBranch        *b_ThreeLayerTracks_KVUd0sigTool;   //!
  TBranch        *b_ThreeLayerTracks_KVUd0sigToolBeamErr;   //!
  TBranch        *b_ThreeLayerTracks_KVUBeamErr;   //!
  TBranch        *b_ThreeLayerTracks_KVUchiSquared;   //!
  TBranch        *b_ThreeLayerTracks_KVUQuality;   //!
  TBranch        *b_ThreeLayerTracks_KVUchi2OvernDoF;   //!
  TBranch        *b_ThreeLayerTracks_numberOfContribPixelLayers;   //!
  TBranch        *b_ThreeLayerTracks_nBLayerHits;   //!
  TBranch        *b_ThreeLayerTracks_nPixHits;   //!
  TBranch        *b_ThreeLayerTracks_nSCTHits;   //!
  TBranch        *b_ThreeLayerTracks_nTRTHits;   //!
  TBranch        *b_ThreeLayerTracks_nSiHits;   //!
  TBranch        *b_ThreeLayerTracks_nBLayerSharedHits;   //!
  TBranch        *b_ThreeLayerTracks_nPixelSharedHits;   //!
  TBranch        *b_ThreeLayerTracks_nSCTSharedHits;   //!
  TBranch        *b_ThreeLayerTracks_nTRTSharedHits;   //!
  TBranch        *b_ThreeLayerTracks_nSiSharedHits;   //!
  TBranch        *b_ThreeLayerTracks_nPixelHoles;   //!
  TBranch        *b_ThreeLayerTracks_nSCTHoles;   //!
  TBranch        *b_ThreeLayerTracks_nSCTDoubleHoles;   //!
  TBranch        *b_ThreeLayerTracks_nTRTHoles;   //!
  TBranch        *b_ThreeLayerTracks_nSiHoles;   //!
  TBranch        *b_ThreeLayerTracks_nBLayerOutliers;   //!
  TBranch        *b_ThreeLayerTracks_nPixelOutliers;   //!
  TBranch        *b_ThreeLayerTracks_nSCTOutliers;   //!
  TBranch        *b_ThreeLayerTracks_nTRTOutliers;   //!
  TBranch        *b_ThreeLayerTracks_nSiOutliers;   //!
  TBranch        *b_ThreeLayerTracks_nBLayerHitsPlusOutlier;   //!
  TBranch        *b_ThreeLayerTracks_nPixHitsPlusOutlier;   //!
  TBranch        *b_ThreeLayerTracks_nSCTHitsPlusOutlier;   //!
  TBranch        *b_ThreeLayerTracks_nTRTHitsPlusOutlier;   //!
  TBranch        *b_ThreeLayerTracks_nSiHitsPlusOutlier;   //!
  TBranch        *b_ThreeLayerTracks_numberOfPixelSpoiltHits;   //!
  TBranch        *b_ThreeLayerTracks_numberOfSCTSpoiltHits;   //!
  TBranch        *b_ThreeLayerTracks_expectBLayerHit;   //!
  TBranch        *b_ThreeLayerTracks_expectInnermostPixelLayerHit;   //!
  TBranch        *b_ThreeLayerTracks_numberOfInnermostPixelLayerHits;   //!
  TBranch        *b_ThreeLayerTracks_numberOfGangedPixels;   //!
  TBranch        *b_ThreeLayerTracks_numberOfGangedFlaggedFakes;   //!
  TBranch        *b_ThreeLayerTracks_numberOfPixelDeadSensors;   //!
  TBranch        *b_ThreeLayerTracks_numberOfSCTDeadSensors;   //!
  TBranch        *b_ThreeLayerTracks_numberOfTRTDeadStraws;   //!
  TBranch        *b_ThreeLayerTracks_MuonSegment40_nPrecisionHits;   //!
  TBranch        *b_ThreeLayerTracks_MuonSegment40_nPhiLayers;   //!
  TBranch        *b_ThreeLayerTracks_MuonSegment40_nTrigEtaLayers;   //!
  TBranch        *b_ThreeLayerTracks_MuonSegment40_nAllHits;   //!
  TBranch        *b_ThreeLayerTracks_IsPassedSR;   //!
  TBranch        *b_ThreeLayerTracks_IsPassedFakeCR;   //!
  TBranch        *b_ThreeLayerTracks_IsPassedHadCR;   //!
  TBranch        *b_ThreeLayerTracks_IsPassedEleCRTag;   //!
  TBranch        *b_ThreeLayerTracks_IsPassedEleCRProbe;   //!
  TBranch        *b_ThreeLayerTracks_IsPassedMuCRTag;   //!
  TBranch        *b_ThreeLayerTracks_IsPassedMuCRProbe;   //!
  TBranch        *b_ThreeLayerTracks_IsPassedIsolated;   //!
  TBranch        *b_ThreeLayerTracks_IsPassedIsolatedLeading;   //!
  TBranch        *b_ThreeLayerTracks_IsPassedORJet;   //!
  TBranch        *b_ThreeLayerTracks_IsPassedORLep;   //!
  TBranch        *b_ThreeLayerTracks_IsPassedQuality;   //!
  TBranch        *b_ThreeLayerTracks_IsPassedBadHitsVeto;   //!
  TBranch        *b_ThreeLayerTracks_IsPassedEta;   //!
  TBranch        *b_ThreeLayerTracks_IsPassedImpactParameter;   //!
  TBranch        *b_ThreeLayerTracks_IsPassedTRTVeto;   //!
  TBranch        *b_ThreeLayerTracks_IsPassedSCTVeto;   //!
  TBranch        *b_ThreeLayerTracks_pixelBarrel0;   //!
  TBranch        *b_ThreeLayerTracks_pixelBarrel1;   //!
  TBranch        *b_ThreeLayerTracks_pixelBarrel2;   //!
  TBranch        *b_ThreeLayerTracks_pixelBarrel3;   //!
  TBranch        *b_ThreeLayerTracks_pixelEndCap0;   //!
  TBranch        *b_ThreeLayerTracks_pixelEndCap1;   //!
  TBranch        *b_ThreeLayerTracks_pixelEndCap2;   //!
  TBranch        *b_ThreeLayerTracks_sctBarrel0;   //!
  TBranch        *b_ThreeLayerTracks_sctBarrel1;   //!
  TBranch        *b_ThreeLayerTracks_sctBarrel2;   //!
  TBranch        *b_ThreeLayerTracks_sctBarrel3;   //!
  TBranch        *b_ThreeLayerTracks_sctEndCap0;   //!
  TBranch        *b_ThreeLayerTracks_sctEndCap1;   //!
  TBranch        *b_ThreeLayerTracks_sctEndCap2;   //!
  TBranch        *b_ThreeLayerTracks_sctEndCap3;   //!
  TBranch        *b_ThreeLayerTracks_sctEndCap4;   //!
  TBranch        *b_ThreeLayerTracks_sctEndCap5;   //!
  TBranch        *b_ThreeLayerTracks_sctEndCap6;   //!
  TBranch        *b_ThreeLayerTracks_sctEndCap7;   //!
  TBranch        *b_ThreeLayerTracks_sctEndCap8;   //!
  TBranch        *b_ThreeLayerTracks_trtBarrel;   //!
  TBranch        *b_ThreeLayerTracks_trtEndCap;   //!
  TBranch        *b_ThreeLayerTracks_DBM0;   //!
  TBranch        *b_ThreeLayerTracks_DBM1;   //!
  TBranch        *b_ThreeLayerTracks_DBM2;   //!
  TBranch        *b_ThreeLayerTracks_DecayRadius;   //!
  TBranch        *b_ThreeLayerTracks_DecayProperTime;   //!
  TBranch        *b_ThreeLayerTracks_deltaPt;   //!
  TBranch        *b_ThreeLayerTracks_deltaQoverP;   //!
  TBranch        *b_ThreeLayerTracks_deltaQoverPt;   //!
  TBranch        *b_ThreeLayerTracks_deltaEta;   //!
  TBranch        *b_ThreeLayerTracks_deltaTheta;   //!
  TBranch        *b_ThreeLayerTracks_deltaPhi;   //!
  TBranch        *b_ThreeLayerTracks_deltaR;   //!
  TBranch        *b_ThreeLayerTracks_deltaQoverPSig;   //!
  TBranch        *b_ThreeLayerTracks_deltaThetaSig;   //!
  TBranch        *b_ThreeLayerTracks_deltaPhiSig;   //!
  TBranch        *b_ThreeLayerTracks_TruthPt;   //!
  TBranch        *b_ThreeLayerTracks_TruthEta;   //!
  TBranch        *b_ThreeLayerTracks_TruthPhi;   //!
  TBranch        *b_ThreeLayerTracks_TruthCharge;   //!
  TBranch        *b_ThreeLayerTracks_TruthMass;   //!
  TBranch        *b_ThreeLayerTracks_TruthFromZ;   //!
  TBranch        *b_ThreeLayerTracks_index_Truth;   //!
  TBranch        *b_ThreeLayerTracks_TruthType;   //!
  TBranch        *b_ThreeLayerTracks_TruthDAODtruth;   //!
  TBranch        *b_ThreeLayerTracks_TruthORtruthJet;   //!
  TBranch        *b_ThreeLayerTracks_TruthOrigin;   //!
  TBranch        *b_ThreeLayerTracks_syst_name;   //!
  TBranch        *b_ThreeLayerTracks_syst_dRJet20;   //!
  TBranch        *b_ThreeLayerTracks_syst_dRJet50;   //!
  TBranch        *b_ThreeLayerTracks_syst_dRElectron;   //!
  TBranch        *b_ThreeLayerTracks_syst_dRMuon;   //!
  TBranch        *b_ThreeLayerTracks_syst_dRMSTrack;   //!
  TBranch        *b_FourLayerTracks_;   //!
  TBranch        *b_FourLayerTracks_fUniqueID;   //!
  TBranch        *b_FourLayerTracks_fBits;   //!
  TBranch        *b_FourLayerTracks_p4;   //!
  TBranch        *b_FourLayerTracks_ReTracking;   //!
  TBranch        *b_FourLayerTracks_PixelTracklet;   //!
  TBranch        *b_FourLayerTracks_d0;   //!
  TBranch        *b_FourLayerTracks_z0;   //!
  TBranch        *b_FourLayerTracks_theta;   //!
  TBranch        *b_FourLayerTracks_phi;   //!
  TBranch        *b_FourLayerTracks_qoverp;   //!
  TBranch        *b_FourLayerTracks_qoverpt;   //!
  TBranch        *b_FourLayerTracks_z0wrtPV;   //!
  TBranch        *b_FourLayerTracks_z0sinthetawrtPV;   //!
  TBranch        *b_FourLayerTracks_d0err;   //!
  TBranch        *b_FourLayerTracks_z0err;   //!
  TBranch        *b_FourLayerTracks_thetaerr;   //!
  TBranch        *b_FourLayerTracks_phierr;   //!
  TBranch        *b_FourLayerTracks_qoverperr;   //!
  TBranch        *b_FourLayerTracks_z0wrtPVerr;   //!
  TBranch        *b_FourLayerTracks_z0wrtPVsig;   //!
  TBranch        *b_FourLayerTracks_z0sinthetawrtPVsig;   //!
  TBranch        *b_FourLayerTracks_d0sigTool;   //!
  TBranch        *b_FourLayerTracks_d0sigToolBeamErr;   //!
  TBranch        *b_FourLayerTracks_BeamErr;   //!
  TBranch        *b_FourLayerTracks_charge;   //!
  TBranch        *b_FourLayerTracks_pixeldEdx;   //!
  TBranch        *b_FourLayerTracks_chiSquared;   //!
  TBranch        *b_FourLayerTracks_Quality;   //!
  TBranch        *b_FourLayerTracks_chi2OvernDoF;   //!
  TBranch        *b_FourLayerTracks_nDoF;   //!
  TBranch        *b_FourLayerTracks_truthMatchingProbability;   //!
  TBranch        *b_FourLayerTracks_ptcone20_1gev;   //!
  TBranch        *b_FourLayerTracks_ptcone30_1gev;   //!
  TBranch        *b_FourLayerTracks_ptcone40_1gev;   //!
  TBranch        *b_FourLayerTracks_ptcone20overPt_1gev;   //!
  TBranch        *b_FourLayerTracks_ptcone30overPt_1gev;   //!
  TBranch        *b_FourLayerTracks_ptcone40overPt_1gev;   //!
  TBranch        *b_FourLayerTracks_ptvarcone20;   //!
  TBranch        *b_FourLayerTracks_ptvarcone30;   //!
  TBranch        *b_FourLayerTracks_ptvarcone40;   //!
  TBranch        *b_FourLayerTracks_ptvarcone20overPt;   //!
  TBranch        *b_FourLayerTracks_ptvarcone30overPt;   //!
  TBranch        *b_FourLayerTracks_ptvarcone40overPt;   //!
  TBranch        *b_FourLayerTracks_etcone20_topo;   //!
  TBranch        *b_FourLayerTracks_etcone30_topo;   //!
  TBranch        *b_FourLayerTracks_etcone40_topo;   //!
  TBranch        *b_FourLayerTracks_etcone20overPt_topo;   //!
  TBranch        *b_FourLayerTracks_etcone30overPt_topo;   //!
  TBranch        *b_FourLayerTracks_etcone40overPt_topo;   //!
  TBranch        *b_FourLayerTracks_etclus20_topo;   //!
  TBranch        *b_FourLayerTracks_etclus30_topo;   //!
  TBranch        *b_FourLayerTracks_etclus40_topo;   //!
  TBranch        *b_FourLayerTracks_etclus20overPt_topo;   //!
  TBranch        *b_FourLayerTracks_etclus30overPt_topo;   //!
  TBranch        *b_FourLayerTracks_etclus40overPt_topo;   //!
  TBranch        *b_FourLayerTracks_nearestClusterdR_calo;   //!
  TBranch        *b_FourLayerTracks_nearestClusteret_calo;   //!
  TBranch        *b_FourLayerTracks_nearestClusterdR_topo;   //!
  TBranch        *b_FourLayerTracks_nearestClusteret_topo;   //!
  TBranch        *b_FourLayerTracks_dRJet20;   //!
  TBranch        *b_FourLayerTracks_dRJet50;   //!
  TBranch        *b_FourLayerTracks_dRElectron;   //!
  TBranch        *b_FourLayerTracks_dRMuon;   //!
  TBranch        *b_FourLayerTracks_dRMSTrack;   //!
  TBranch        *b_FourLayerTracks_dRTrack_5gev;   //!
  TBranch        *b_FourLayerTracks_dRTrack_10gev;   //!
  TBranch        *b_FourLayerTracks_KVUPt;   //!
  TBranch        *b_FourLayerTracks_KVUEta;   //!
  TBranch        *b_FourLayerTracks_KVUPhi;   //!
  TBranch        *b_FourLayerTracks_KVUd0;   //!
  TBranch        *b_FourLayerTracks_KVUz0;   //!
  TBranch        *b_FourLayerTracks_KVUtheta;   //!
  TBranch        *b_FourLayerTracks_KVUphi;   //!
  TBranch        *b_FourLayerTracks_KVUqoverp;   //!
  TBranch        *b_FourLayerTracks_KVUqoverpt;   //!
  TBranch        *b_FourLayerTracks_KVUz0wrtPV;   //!
  TBranch        *b_FourLayerTracks_KVUz0sinthetawrtPV;   //!
  TBranch        *b_FourLayerTracks_KVUd0err;   //!
  TBranch        *b_FourLayerTracks_KVUz0err;   //!
  TBranch        *b_FourLayerTracks_KVUthetaerr;   //!
  TBranch        *b_FourLayerTracks_KVUphierr;   //!
  TBranch        *b_FourLayerTracks_KVUqoverperr;   //!
  TBranch        *b_FourLayerTracks_KVUz0wrtPVerr;   //!
  TBranch        *b_FourLayerTracks_KVUz0wrtPVsig;   //!
  TBranch        *b_FourLayerTracks_KVUz0sinthetawrtPVsig;   //!
  TBranch        *b_FourLayerTracks_KVUd0sigTool;   //!
  TBranch        *b_FourLayerTracks_KVUd0sigToolBeamErr;   //!
  TBranch        *b_FourLayerTracks_KVUBeamErr;   //!
  TBranch        *b_FourLayerTracks_KVUchiSquared;   //!
  TBranch        *b_FourLayerTracks_KVUQuality;   //!
  TBranch        *b_FourLayerTracks_KVUchi2OvernDoF;   //!
  TBranch        *b_FourLayerTracks_numberOfContribPixelLayers;   //!
  TBranch        *b_FourLayerTracks_nBLayerHits;   //!
  TBranch        *b_FourLayerTracks_nPixHits;   //!
  TBranch        *b_FourLayerTracks_nSCTHits;   //!
  TBranch        *b_FourLayerTracks_nTRTHits;   //!
  TBranch        *b_FourLayerTracks_nSiHits;   //!
  TBranch        *b_FourLayerTracks_nBLayerSharedHits;   //!
  TBranch        *b_FourLayerTracks_nPixelSharedHits;   //!
  TBranch        *b_FourLayerTracks_nSCTSharedHits;   //!
  TBranch        *b_FourLayerTracks_nTRTSharedHits;   //!
  TBranch        *b_FourLayerTracks_nSiSharedHits;   //!
  TBranch        *b_FourLayerTracks_nPixelHoles;   //!
  TBranch        *b_FourLayerTracks_nSCTHoles;   //!
  TBranch        *b_FourLayerTracks_nSCTDoubleHoles;   //!
  TBranch        *b_FourLayerTracks_nTRTHoles;   //!
  TBranch        *b_FourLayerTracks_nSiHoles;   //!
  TBranch        *b_FourLayerTracks_nBLayerOutliers;   //!
  TBranch        *b_FourLayerTracks_nPixelOutliers;   //!
  TBranch        *b_FourLayerTracks_nSCTOutliers;   //!
  TBranch        *b_FourLayerTracks_nTRTOutliers;   //!
  TBranch        *b_FourLayerTracks_nSiOutliers;   //!
  TBranch        *b_FourLayerTracks_nBLayerHitsPlusOutlier;   //!
  TBranch        *b_FourLayerTracks_nPixHitsPlusOutlier;   //!
  TBranch        *b_FourLayerTracks_nSCTHitsPlusOutlier;   //!
  TBranch        *b_FourLayerTracks_nTRTHitsPlusOutlier;   //!
  TBranch        *b_FourLayerTracks_nSiHitsPlusOutlier;   //!
  TBranch        *b_FourLayerTracks_numberOfPixelSpoiltHits;   //!
  TBranch        *b_FourLayerTracks_numberOfSCTSpoiltHits;   //!
  TBranch        *b_FourLayerTracks_expectBLayerHit;   //!
  TBranch        *b_FourLayerTracks_expectInnermostPixelLayerHit;   //!
  TBranch        *b_FourLayerTracks_numberOfInnermostPixelLayerHits;   //!
  TBranch        *b_FourLayerTracks_numberOfGangedPixels;   //!
  TBranch        *b_FourLayerTracks_numberOfGangedFlaggedFakes;   //!
  TBranch        *b_FourLayerTracks_numberOfPixelDeadSensors;   //!
  TBranch        *b_FourLayerTracks_numberOfSCTDeadSensors;   //!
  TBranch        *b_FourLayerTracks_numberOfTRTDeadStraws;   //!
  TBranch        *b_FourLayerTracks_MuonSegment40_nPrecisionHits;   //!
  TBranch        *b_FourLayerTracks_MuonSegment40_nPhiLayers;   //!
  TBranch        *b_FourLayerTracks_MuonSegment40_nTrigEtaLayers;   //!
  TBranch        *b_FourLayerTracks_MuonSegment40_nAllHits;   //!
  TBranch        *b_FourLayerTracks_IsPassedSR;   //!
  TBranch        *b_FourLayerTracks_IsPassedFakeCR;   //!
  TBranch        *b_FourLayerTracks_IsPassedHadCR;   //!
  TBranch        *b_FourLayerTracks_IsPassedEleCRTag;   //!
  TBranch        *b_FourLayerTracks_IsPassedEleCRProbe;   //!
  TBranch        *b_FourLayerTracks_IsPassedMuCRTag;   //!
  TBranch        *b_FourLayerTracks_IsPassedMuCRProbe;   //!
  TBranch        *b_FourLayerTracks_IsPassedIsolated;   //!
  TBranch        *b_FourLayerTracks_IsPassedIsolatedLeading;   //!
  TBranch        *b_FourLayerTracks_IsPassedORJet;   //!
  TBranch        *b_FourLayerTracks_IsPassedORLep;   //!
  TBranch        *b_FourLayerTracks_IsPassedQuality;   //!
  TBranch        *b_FourLayerTracks_IsPassedBadHitsVeto;   //!
  TBranch        *b_FourLayerTracks_IsPassedEta;   //!
  TBranch        *b_FourLayerTracks_IsPassedImpactParameter;   //!
  TBranch        *b_FourLayerTracks_IsPassedTRTVeto;   //!
  TBranch        *b_FourLayerTracks_IsPassedSCTVeto;   //!
  TBranch        *b_FourLayerTracks_pixelBarrel0;   //!
  TBranch        *b_FourLayerTracks_pixelBarrel1;   //!
  TBranch        *b_FourLayerTracks_pixelBarrel2;   //!
  TBranch        *b_FourLayerTracks_pixelBarrel3;   //!
  TBranch        *b_FourLayerTracks_pixelEndCap0;   //!
  TBranch        *b_FourLayerTracks_pixelEndCap1;   //!
  TBranch        *b_FourLayerTracks_pixelEndCap2;   //!
  TBranch        *b_FourLayerTracks_sctBarrel0;   //!
  TBranch        *b_FourLayerTracks_sctBarrel1;   //!
  TBranch        *b_FourLayerTracks_sctBarrel2;   //!
  TBranch        *b_FourLayerTracks_sctBarrel3;   //!
  TBranch        *b_FourLayerTracks_sctEndCap0;   //!
  TBranch        *b_FourLayerTracks_sctEndCap1;   //!
  TBranch        *b_FourLayerTracks_sctEndCap2;   //!
  TBranch        *b_FourLayerTracks_sctEndCap3;   //!
  TBranch        *b_FourLayerTracks_sctEndCap4;   //!
  TBranch        *b_FourLayerTracks_sctEndCap5;   //!
  TBranch        *b_FourLayerTracks_sctEndCap6;   //!
  TBranch        *b_FourLayerTracks_sctEndCap7;   //!
  TBranch        *b_FourLayerTracks_sctEndCap8;   //!
  TBranch        *b_FourLayerTracks_trtBarrel;   //!
  TBranch        *b_FourLayerTracks_trtEndCap;   //!
  TBranch        *b_FourLayerTracks_DBM0;   //!
  TBranch        *b_FourLayerTracks_DBM1;   //!
  TBranch        *b_FourLayerTracks_DBM2;   //!
  TBranch        *b_FourLayerTracks_DecayRadius;   //!
  TBranch        *b_FourLayerTracks_DecayProperTime;   //!
  TBranch        *b_FourLayerTracks_deltaPt;   //!
  TBranch        *b_FourLayerTracks_deltaQoverP;   //!
  TBranch        *b_FourLayerTracks_deltaQoverPt;   //!
  TBranch        *b_FourLayerTracks_deltaEta;   //!
  TBranch        *b_FourLayerTracks_deltaTheta;   //!
  TBranch        *b_FourLayerTracks_deltaPhi;   //!
  TBranch        *b_FourLayerTracks_deltaR;   //!
  TBranch        *b_FourLayerTracks_deltaQoverPSig;   //!
  TBranch        *b_FourLayerTracks_deltaThetaSig;   //!
  TBranch        *b_FourLayerTracks_deltaPhiSig;   //!
  TBranch        *b_FourLayerTracks_TruthPt;   //!
  TBranch        *b_FourLayerTracks_TruthEta;   //!
  TBranch        *b_FourLayerTracks_TruthPhi;   //!
  TBranch        *b_FourLayerTracks_TruthCharge;   //!
  TBranch        *b_FourLayerTracks_TruthMass;   //!
  TBranch        *b_FourLayerTracks_TruthFromZ;   //!
  TBranch        *b_FourLayerTracks_index_Truth;   //!
  TBranch        *b_FourLayerTracks_TruthType;   //!
  TBranch        *b_FourLayerTracks_TruthDAODtruth;   //!
  TBranch        *b_FourLayerTracks_TruthORtruthJet;   //!
  TBranch        *b_FourLayerTracks_TruthOrigin;   //!
  TBranch        *b_FourLayerTracks_syst_name;   //!
  TBranch        *b_FourLayerTracks_syst_dRJet20;   //!
  TBranch        *b_FourLayerTracks_syst_dRJet50;   //!
  TBranch        *b_FourLayerTracks_syst_dRElectron;   //!
  TBranch        *b_FourLayerTracks_syst_dRMuon;   //!
  TBranch        *b_FourLayerTracks_syst_dRMSTrack;   //!
  TBranch        *b_DRAWJets_;   //!
  TBranch        *b_DRAWJets_fUniqueID;   //!
  TBranch        *b_DRAWJets_fBits;   //!
  TBranch        *b_DRAWJets_p4;   //!
  TBranch        *b_DRAWJets_IsSignal;   //!
  TBranch        *b_DRAWJets_BTagged;   //!
  TBranch        *b_DRAWJets_Quality;   //!
  TBranch        *b_DRAWJets_TruthLabelID;   //!
  TBranch        *b_RawJets_;   //!
  TBranch        *b_RawJets_fUniqueID;   //!
  TBranch        *b_RawJets_fBits;   //!
  TBranch        *b_RawJets_p4;   //!
  TBranch        *b_RawJets_IsSignal;   //!
  TBranch        *b_RawJets_BTagged;   //!
  TBranch        *b_RawJets_Quality;   //!
  TBranch        *b_RawJets_TruthLabelID;   //!
  TBranch        *b_GoodJets_;   //!
  TBranch        *b_GoodJets_fUniqueID;   //!
  TBranch        *b_GoodJets_fBits;   //!
  TBranch        *b_GoodJets_p4;   //!
  TBranch        *b_GoodJets_IsSignal;   //!
  TBranch        *b_GoodJets_BTagged;   //!
  TBranch        *b_GoodJets_Quality;   //!
  TBranch        *b_GoodJets_TruthLabelID;   //!
  TBranch        *b_TruthJets_;   //!
  TBranch        *b_TruthJets_fUniqueID;   //!
  TBranch        *b_TruthJets_fBits;   //!
  TBranch        *b_TruthJets_p4;   //!
  TBranch        *b_TruthJets_IsSignal;   //!
  TBranch        *b_TruthJets_BTagged;   //!
  TBranch        *b_TruthJets_Quality;   //!
  TBranch        *b_TruthJets_TruthLabelID;   //!
  TBranch        *b_GoodElectrons_;   //!
  TBranch        *b_GoodElectrons_fUniqueID;   //!
  TBranch        *b_GoodElectrons_fBits;   //!
  TBranch        *b_GoodElectrons_p4;   //!
  TBranch        *b_GoodElectrons_IsSignal;   //!
  TBranch        *b_GoodElectrons_Quality;   //!
  TBranch        *b_GoodElectrons_IsGoodIP;   //!
  TBranch        *b_GoodElectrons_Charge;   //!
  TBranch        *b_GoodElectrons_TriggerSF;   //!
  TBranch        *b_GoodElectrons_triggerMatching;   //!
  TBranch        *b_GoodElectrons_index_IDTrack;   //!
  TBranch        *b_GoodMuons_;   //!
  TBranch        *b_GoodMuons_fUniqueID;   //!
  TBranch        *b_GoodMuons_fBits;   //!
  TBranch        *b_GoodMuons_p4;   //!
  TBranch        *b_GoodMuons_IsSignal;   //!
  TBranch        *b_GoodMuons_Quality;   //!
  TBranch        *b_GoodMuons_IsGoodIP;   //!
  TBranch        *b_GoodMuons_Charge;   //!
  TBranch        *b_GoodMuons_TriggerSF;   //!
  TBranch        *b_GoodMuons_triggerMatching;   //!
  TBranch        *b_GoodMuons_index_IDTrack;   //!
  TBranch        *b_GoodTaus_;   //!
  TBranch        *b_GoodTaus_fUniqueID;   //!
  TBranch        *b_GoodTaus_fBits;   //!
  TBranch        *b_GoodTaus_p4;   //!
  TBranch        *b_GoodTaus_IsSignal;   //!
  TBranch        *b_GoodTaus_Quality;   //!
  TBranch        *b_GoodTaus_IsGoodIP;   //!
  TBranch        *b_GoodTaus_Charge;   //!
  TBranch        *b_GoodTaus_TriggerSF;   //!
  TBranch        *b_GoodTaus_triggerMatching;   //!
  TBranch        *b_GoodTaus_index_IDTrack;   //!
  TBranch        *b_Zee_ProbeCluster_;   //!
  TBranch        *b_Zee_ProbeCluster_fUniqueID;   //!
  TBranch        *b_Zee_ProbeCluster_fBits;   //!
  TBranch        *b_Zee_ProbeCluster_p4;   //!
  TBranch        *b_Zee_ProbeCluster_index_TagLepton;   //!
  TBranch        *b_Zee_ProbeCluster_index_ProbeIdTrack;   //!
  TBranch        *b_Zee_ProbeCluster_index_ProbeCaloCluster;   //!
  TBranch        *b_Zee_ProbeTrack_;   //!
  TBranch        *b_Zee_ProbeTrack_fUniqueID;   //!
  TBranch        *b_Zee_ProbeTrack_fBits;   //!
  TBranch        *b_Zee_ProbeTrack_p4;   //!
  TBranch        *b_Zee_ProbeTrack_index_TagLepton;   //!
  TBranch        *b_Zee_ProbeTrack_index_ProbeIdTrack;   //!
  TBranch        *b_Zee_ProbeTrack_index_ProbeCaloCluster;   //!
  TBranch        *b_Zmumu_ProbeCluster_;   //!
  TBranch        *b_Zmumu_ProbeCluster_fUniqueID;   //!
  TBranch        *b_Zmumu_ProbeCluster_fBits;   //!
  TBranch        *b_Zmumu_ProbeCluster_p4;   //!
  TBranch        *b_Zmumu_ProbeCluster_index_TagLepton;   //!
  TBranch        *b_Zmumu_ProbeCluster_index_ProbeIdTrack;   //!
  TBranch        *b_Zmumu_ProbeCluster_index_ProbeMSTrack;   //!
  TBranch        *b_Zmumu_ProbeTrack_;   //!
  TBranch        *b_Zmumu_ProbeTrack_fUniqueID;   //!
  TBranch        *b_Zmumu_ProbeTrack_fBits;   //!
  TBranch        *b_Zmumu_ProbeTrack_p4;   //!
  TBranch        *b_Zmumu_ProbeTrack_index_TagLepton;   //!
  TBranch        *b_Zmumu_ProbeTrack_index_ProbeIdTrack;   //!
  TBranch        *b_Zmumu_ProbeTrack_index_ProbeMSTrack;   //!
  TBranch        *b_Truths_;   //!
  TBranch        *b_Truths_fUniqueID;   //!
  TBranch        *b_Truths_fBits;   //!
  TBranch        *b_Truths_p4;   //!
  TBranch        *b_Truths_qoverp;   //!
  TBranch        *b_Truths_qoverpt;   //!
  TBranch        *b_Truths_ProperTime;   //!
  TBranch        *b_Truths_Mass;   //!
  TBranch        *b_Truths_Charge;   //!
  TBranch        *b_Truths_ProdRadius;   //!
  TBranch        *b_Truths_DecayRadius;   //!
  TBranch        *b_Truths_nChildren;   //!
  TBranch        *b_Truths_FromZ;   //!
  TBranch        *b_Truths_PdgId;   //!
  TBranch        *b_Truths_Type;   //!
  TBranch        *b_Truths_DAODtruth;   //!
  TBranch        *b_Truths_Origin;   //!
  TBranch        *b_Truths_ORtruthJet;   //!
  TBranch        *b_Truths_pt_vis;   //!
  TBranch        *b_Truths_eta_vis;   //!
  TBranch        *b_Truths_phi_vis;   //!
  TBranch        *b_Truths_m_vis;   //!
  TBranch        *b_Truths_numCharged;   //!
  TBranch        *b_Truths_numChargedPion;   //!
  TBranch        *b_Truths_HasHadronInChildren;   //!
  TBranch        *b_Truths_HasElectronInChildren;   //!
  TBranch        *b_Truths_HasMuonInChildren;   //!
  TBranch        *b_Truths_HasPhotonInChildren;   //!
  TBranch        *b_Truths_DecayParticle_dR;   //!
  TBranch        *b_Truths_DecayParticle_dPt;   //!
  TBranch        *b_MSTracks_;   //!
  TBranch        *b_MSTracks_fUniqueID;   //!
  TBranch        *b_MSTracks_fBits;   //!
  TBranch        *b_MSTracks_p4;   //!
  TBranch        *b_MSTracks_ReTracking;   //!
  TBranch        *b_MSTracks_PixelTracklet;   //!
  TBranch        *b_MSTracks_d0;   //!
  TBranch        *b_MSTracks_z0;   //!
  TBranch        *b_MSTracks_theta;   //!
  TBranch        *b_MSTracks_phi;   //!
  TBranch        *b_MSTracks_qoverp;   //!
  TBranch        *b_MSTracks_qoverpt;   //!
  TBranch        *b_MSTracks_z0wrtPV;   //!
  TBranch        *b_MSTracks_z0sinthetawrtPV;   //!
  TBranch        *b_MSTracks_d0err;   //!
  TBranch        *b_MSTracks_z0err;   //!
  TBranch        *b_MSTracks_thetaerr;   //!
  TBranch        *b_MSTracks_phierr;   //!
  TBranch        *b_MSTracks_qoverperr;   //!
  TBranch        *b_MSTracks_z0wrtPVerr;   //!
  TBranch        *b_MSTracks_z0wrtPVsig;   //!
  TBranch        *b_MSTracks_z0sinthetawrtPVsig;   //!
  TBranch        *b_MSTracks_d0sigTool;   //!
  TBranch        *b_MSTracks_d0sigToolBeamErr;   //!
  TBranch        *b_MSTracks_BeamErr;   //!
  TBranch        *b_MSTracks_charge;   //!
  TBranch        *b_MSTracks_pixeldEdx;   //!
  TBranch        *b_MSTracks_chiSquared;   //!
  TBranch        *b_MSTracks_Quality;   //!
  TBranch        *b_MSTracks_chi2OvernDoF;   //!
  TBranch        *b_MSTracks_nDoF;   //!
  TBranch        *b_MSTracks_truthMatchingProbability;   //!
  TBranch        *b_MSTracks_ptcone20_1gev;   //!
  TBranch        *b_MSTracks_ptcone30_1gev;   //!
  TBranch        *b_MSTracks_ptcone40_1gev;   //!
  TBranch        *b_MSTracks_ptcone20overPt_1gev;   //!
  TBranch        *b_MSTracks_ptcone30overPt_1gev;   //!
  TBranch        *b_MSTracks_ptcone40overPt_1gev;   //!
  TBranch        *b_MSTracks_ptvarcone20;   //!
  TBranch        *b_MSTracks_ptvarcone30;   //!
  TBranch        *b_MSTracks_ptvarcone40;   //!
  TBranch        *b_MSTracks_ptvarcone20overPt;   //!
  TBranch        *b_MSTracks_ptvarcone30overPt;   //!
  TBranch        *b_MSTracks_ptvarcone40overPt;   //!
  TBranch        *b_MSTracks_etcone20_topo;   //!
  TBranch        *b_MSTracks_etcone30_topo;   //!
  TBranch        *b_MSTracks_etcone40_topo;   //!
  TBranch        *b_MSTracks_etcone20overPt_topo;   //!
  TBranch        *b_MSTracks_etcone30overPt_topo;   //!
  TBranch        *b_MSTracks_etcone40overPt_topo;   //!
  TBranch        *b_MSTracks_etclus20_topo;   //!
  TBranch        *b_MSTracks_etclus30_topo;   //!
  TBranch        *b_MSTracks_etclus40_topo;   //!
  TBranch        *b_MSTracks_etclus20overPt_topo;   //!
  TBranch        *b_MSTracks_etclus30overPt_topo;   //!
  TBranch        *b_MSTracks_etclus40overPt_topo;   //!
  TBranch        *b_MSTracks_nearestClusterdR_calo;   //!
  TBranch        *b_MSTracks_nearestClusteret_calo;   //!
  TBranch        *b_MSTracks_nearestClusterdR_topo;   //!
  TBranch        *b_MSTracks_nearestClusteret_topo;   //!
  TBranch        *b_MSTracks_dRJet20;   //!
  TBranch        *b_MSTracks_dRJet50;   //!
  TBranch        *b_MSTracks_dRElectron;   //!
  TBranch        *b_MSTracks_dRMuon;   //!
  TBranch        *b_MSTracks_dRMSTrack;   //!
  TBranch        *b_MSTracks_dRTrack_5gev;   //!
  TBranch        *b_MSTracks_dRTrack_10gev;   //!
  TBranch        *b_MSTracks_KVUPt;   //!
  TBranch        *b_MSTracks_KVUEta;   //!
  TBranch        *b_MSTracks_KVUPhi;   //!
  TBranch        *b_MSTracks_KVUd0;   //!
  TBranch        *b_MSTracks_KVUz0;   //!
  TBranch        *b_MSTracks_KVUtheta;   //!
  TBranch        *b_MSTracks_KVUphi;   //!
  TBranch        *b_MSTracks_KVUqoverp;   //!
  TBranch        *b_MSTracks_KVUqoverpt;   //!
  TBranch        *b_MSTracks_KVUz0wrtPV;   //!
  TBranch        *b_MSTracks_KVUz0sinthetawrtPV;   //!
  TBranch        *b_MSTracks_KVUd0err;   //!
  TBranch        *b_MSTracks_KVUz0err;   //!
  TBranch        *b_MSTracks_KVUthetaerr;   //!
  TBranch        *b_MSTracks_KVUphierr;   //!
  TBranch        *b_MSTracks_KVUqoverperr;   //!
  TBranch        *b_MSTracks_KVUz0wrtPVerr;   //!
  TBranch        *b_MSTracks_KVUz0wrtPVsig;   //!
  TBranch        *b_MSTracks_KVUz0sinthetawrtPVsig;   //!
  TBranch        *b_MSTracks_KVUd0sigTool;   //!
  TBranch        *b_MSTracks_KVUd0sigToolBeamErr;   //!
  TBranch        *b_MSTracks_KVUBeamErr;   //!
  TBranch        *b_MSTracks_KVUchiSquared;   //!
  TBranch        *b_MSTracks_KVUQuality;   //!
  TBranch        *b_MSTracks_KVUchi2OvernDoF;   //!
  TBranch        *b_MSTracks_numberOfContribPixelLayers;   //!
  TBranch        *b_MSTracks_nBLayerHits;   //!
  TBranch        *b_MSTracks_nPixHits;   //!
  TBranch        *b_MSTracks_nSCTHits;   //!
  TBranch        *b_MSTracks_nTRTHits;   //!
  TBranch        *b_MSTracks_nSiHits;   //!
  TBranch        *b_MSTracks_nBLayerSharedHits;   //!
  TBranch        *b_MSTracks_nPixelSharedHits;   //!
  TBranch        *b_MSTracks_nSCTSharedHits;   //!
  TBranch        *b_MSTracks_nTRTSharedHits;   //!
  TBranch        *b_MSTracks_nSiSharedHits;   //!
  TBranch        *b_MSTracks_nPixelHoles;   //!
  TBranch        *b_MSTracks_nSCTHoles;   //!
  TBranch        *b_MSTracks_nSCTDoubleHoles;   //!
  TBranch        *b_MSTracks_nTRTHoles;   //!
  TBranch        *b_MSTracks_nSiHoles;   //!
  TBranch        *b_MSTracks_nBLayerOutliers;   //!
  TBranch        *b_MSTracks_nPixelOutliers;   //!
  TBranch        *b_MSTracks_nSCTOutliers;   //!
  TBranch        *b_MSTracks_nTRTOutliers;   //!
  TBranch        *b_MSTracks_nSiOutliers;   //!
  TBranch        *b_MSTracks_nBLayerHitsPlusOutlier;   //!
  TBranch        *b_MSTracks_nPixHitsPlusOutlier;   //!
  TBranch        *b_MSTracks_nSCTHitsPlusOutlier;   //!
  TBranch        *b_MSTracks_nTRTHitsPlusOutlier;   //!
  TBranch        *b_MSTracks_nSiHitsPlusOutlier;   //!
  TBranch        *b_MSTracks_numberOfPixelSpoiltHits;   //!
  TBranch        *b_MSTracks_numberOfSCTSpoiltHits;   //!
  TBranch        *b_MSTracks_expectBLayerHit;   //!
  TBranch        *b_MSTracks_expectInnermostPixelLayerHit;   //!
  TBranch        *b_MSTracks_numberOfInnermostPixelLayerHits;   //!
  TBranch        *b_MSTracks_numberOfGangedPixels;   //!
  TBranch        *b_MSTracks_numberOfGangedFlaggedFakes;   //!
  TBranch        *b_MSTracks_numberOfPixelDeadSensors;   //!
  TBranch        *b_MSTracks_numberOfSCTDeadSensors;   //!
  TBranch        *b_MSTracks_numberOfTRTDeadStraws;   //!
  TBranch        *b_MSTracks_MuonSegment40_nPrecisionHits;   //!
  TBranch        *b_MSTracks_MuonSegment40_nPhiLayers;   //!
  TBranch        *b_MSTracks_MuonSegment40_nTrigEtaLayers;   //!
  TBranch        *b_MSTracks_MuonSegment40_nAllHits;   //!
  TBranch        *b_MSTracks_IsPassedSR;   //!
  TBranch        *b_MSTracks_IsPassedFakeCR;   //!
  TBranch        *b_MSTracks_IsPassedHadCR;   //!
  TBranch        *b_MSTracks_IsPassedEleCRTag;   //!
  TBranch        *b_MSTracks_IsPassedEleCRProbe;   //!
  TBranch        *b_MSTracks_IsPassedMuCRTag;   //!
  TBranch        *b_MSTracks_IsPassedMuCRProbe;   //!
  TBranch        *b_MSTracks_IsPassedIsolated;   //!
  TBranch        *b_MSTracks_IsPassedIsolatedLeading;   //!
  TBranch        *b_MSTracks_IsPassedORJet;   //!
  TBranch        *b_MSTracks_IsPassedORLep;   //!
  TBranch        *b_MSTracks_IsPassedQuality;   //!
  TBranch        *b_MSTracks_IsPassedBadHitsVeto;   //!
  TBranch        *b_MSTracks_IsPassedEta;   //!
  TBranch        *b_MSTracks_IsPassedImpactParameter;   //!
  TBranch        *b_MSTracks_IsPassedTRTVeto;   //!
  TBranch        *b_MSTracks_IsPassedSCTVeto;   //!
  TBranch        *b_MSTracks_pixelBarrel0;   //!
  TBranch        *b_MSTracks_pixelBarrel1;   //!
  TBranch        *b_MSTracks_pixelBarrel2;   //!
  TBranch        *b_MSTracks_pixelBarrel3;   //!
  TBranch        *b_MSTracks_pixelEndCap0;   //!
  TBranch        *b_MSTracks_pixelEndCap1;   //!
  TBranch        *b_MSTracks_pixelEndCap2;   //!
  TBranch        *b_MSTracks_sctBarrel0;   //!
  TBranch        *b_MSTracks_sctBarrel1;   //!
  TBranch        *b_MSTracks_sctBarrel2;   //!
  TBranch        *b_MSTracks_sctBarrel3;   //!
  TBranch        *b_MSTracks_sctEndCap0;   //!
  TBranch        *b_MSTracks_sctEndCap1;   //!
  TBranch        *b_MSTracks_sctEndCap2;   //!
  TBranch        *b_MSTracks_sctEndCap3;   //!
  TBranch        *b_MSTracks_sctEndCap4;   //!
  TBranch        *b_MSTracks_sctEndCap5;   //!
  TBranch        *b_MSTracks_sctEndCap6;   //!
  TBranch        *b_MSTracks_sctEndCap7;   //!
  TBranch        *b_MSTracks_sctEndCap8;   //!
  TBranch        *b_MSTracks_trtBarrel;   //!
  TBranch        *b_MSTracks_trtEndCap;   //!
  TBranch        *b_MSTracks_DBM0;   //!
  TBranch        *b_MSTracks_DBM1;   //!
  TBranch        *b_MSTracks_DBM2;   //!
  TBranch        *b_MSTracks_DecayRadius;   //!
  TBranch        *b_MSTracks_DecayProperTime;   //!
  TBranch        *b_MSTracks_deltaPt;   //!
  TBranch        *b_MSTracks_deltaQoverP;   //!
  TBranch        *b_MSTracks_deltaQoverPt;   //!
  TBranch        *b_MSTracks_deltaEta;   //!
  TBranch        *b_MSTracks_deltaTheta;   //!
  TBranch        *b_MSTracks_deltaPhi;   //!
  TBranch        *b_MSTracks_deltaR;   //!
  TBranch        *b_MSTracks_deltaQoverPSig;   //!
  TBranch        *b_MSTracks_deltaThetaSig;   //!
  TBranch        *b_MSTracks_deltaPhiSig;   //!
  TBranch        *b_MSTracks_TruthPt;   //!
  TBranch        *b_MSTracks_TruthEta;   //!
  TBranch        *b_MSTracks_TruthPhi;   //!
  TBranch        *b_MSTracks_TruthCharge;   //!
  TBranch        *b_MSTracks_TruthMass;   //!
  TBranch        *b_MSTracks_TruthFromZ;   //!
  TBranch        *b_MSTracks_index_Truth;   //!
  TBranch        *b_MSTracks_TruthType;   //!
  TBranch        *b_MSTracks_TruthDAODtruth;   //!
  TBranch        *b_MSTracks_TruthORtruthJet;   //!
  TBranch        *b_MSTracks_TruthOrigin;   //!
  TBranch        *b_MSTracks_syst_name;   //!
  TBranch        *b_MSTracks_syst_dRJet20;   //!
  TBranch        *b_MSTracks_syst_dRJet50;   //!
  TBranch        *b_MSTracks_syst_dRElectron;   //!
  TBranch        *b_MSTracks_syst_dRMuon;   //!
  TBranch        *b_MSTracks_syst_dRMSTrack;   //!
  TBranch        *b_EgammaClusters_;   //!
  TBranch        *b_EgammaClusters_fUniqueID;   //!
  TBranch        *b_EgammaClusters_fBits;   //!
  TBranch        *b_EgammaClusters_p4;   //!
  TBranch        *b_EgammaClusters_ClusterSize;   //!
  TBranch        *b_MET_PhysObjBase_fUniqueID;   //!
  TBranch        *b_MET_PhysObjBase_fBits;   //!
  TBranch        *b_MET_PhysObjBase_p4;   //!
  TBranch        *b_MET_ForEleCR_PhysObjBase_fUniqueID;   //!
  TBranch        *b_MET_ForEleCR_PhysObjBase_fBits;   //!
  TBranch        *b_MET_ForEleCR_PhysObjBase_p4;   //!
  TBranch        *b_MET_ForMuCR_PhysObjBase_fUniqueID;   //!
  TBranch        *b_MET_ForMuCR_PhysObjBase_fBits;   //!
  TBranch        *b_MET_ForMuCR_PhysObjBase_p4;   //!
  TBranch        *b_MET_Truth_PhysObjBase_fUniqueID;   //!
  TBranch        *b_MET_Truth_PhysObjBase_fBits;   //!
  TBranch        *b_MET_Truth_PhysObjBase_p4;   //!
  TBranch        *b_MET_ForXeTrigger_PhysObjBase_fUniqueID;   //!
  TBranch        *b_MET_ForXeTrigger_PhysObjBase_fBits;   //!
  TBranch        *b_MET_ForXeTrigger_PhysObjBase_p4;   //!
  TBranch        *b_MET_ForDRAW_PhysObjBase_fUniqueID;   //!
  TBranch        *b_MET_ForDRAW_PhysObjBase_fBits;   //!
  TBranch        *b_MET_ForDRAW_PhysObjBase_p4;   //!
  TBranch        *b_GRL;   //!
  TBranch        *b_IsData;   //!
  TBranch        *b_BadJet_LooseBad;   //!
  TBranch        *b_IsPassedDRAWMuonVeto;   //!
  TBranch        *b_IsPassedDRAWElectronVeto;   //!
  TBranch        *b_IsPassedMETTrigger;   //!
  TBranch        *b_IsPassedElectronTrigger;   //!
  TBranch        *b_IsPassedMuonTrigger;   //!
  TBranch        *b_IsPassedJetMET;   //!
  TBranch        *b_IsPassedBadEventVeto;   //!
  TBranch        *b_IsPassedLeptonVeto;   //!
  TBranch        *b_IsPassedBadJet;   //!
  TBranch        *b_IsPassedNCBVeto;   //!
  TBranch        *b_IsPassedBadMuonVeto;   //!
  TBranch        *b_IsPassedBadMuonMETCleaning;   //!
  TBranch        *b_IsPassedCosmicMuonVeto;   //!
  TBranch        *b_IsPassedAnyMETTrigger;   //!
  TBranch        *b_IsPassedKinematics_EWK;   //!
  TBranch        *b_IsPassedKinematics_Strong;   //!
  TBranch        *b_METTriggerEfficiency_EW;   //!
  TBranch        *b_METTriggerEfficiency_EW_up;   //!
  TBranch        *b_METTriggerEfficiency_EW_down;   //!
  TBranch        *b_METTriggerEfficiency_strongPt0;   //!
  TBranch        *b_METTriggerEfficiency_strongPt0_up;   //!
  TBranch        *b_METTriggerEfficiency_strongPt0_down;   //!
  TBranch        *b_METTriggerEfficiency_strongPt2;   //!
  TBranch        *b_METTriggerEfficiency_strongPt2_up;   //!
  TBranch        *b_METTriggerEfficiency_strongPt2_down;   //!
  TBranch        *b_METPt;   //!
  TBranch        *b_JetPt;   //!
  TBranch        *b_actualInteractionsPerCrossing;   //!
  TBranch        *b_averageInteractionsPerCrossing;   //!
  TBranch        *b_CorrectedAverageInteractionsPerCrossing;   //!
  TBranch        *b_nTracksFromPrimaryVertex;   //!
  TBranch        *b_weightXsec;   //!
  TBranch        *b_weightMCweight;   //!
  TBranch        *b_weightSherpaNjets;   //!
  TBranch        *b_weightPileupReweighting;   //!
  TBranch        *b_weightElectronSF;   //!
  TBranch        *b_weightElectronTriggerSF;   //!
  TBranch        *b_weightTagElectronTriggerSF;   //!
  TBranch        *b_weightMuonSF;   //!
  TBranch        *b_weightMuonTriggerSF;   //!
  TBranch        *b_weightTagMuonTriggerSF;   //!
  TBranch        *b_weightTauSF;   //!
  TBranch        *b_treatAsYear;   //!
  TBranch        *b_prodType;   //!
  TBranch        *b_DetectorError;   //!
  TBranch        *b_randomRunNumber;   //!
  TBranch        *b_RunNumber;   //!
  TBranch        *b_lumiBlock;   //!
  TBranch        *b_nPrimaryVertex;   //!
  TBranch        *b_EventNumber;   //!
  TBranch        *b_mcEventWeights;   //!

  MyAnalysis(TTree *tree=0);
  virtual ~MyAnalysis();
  virtual Int_t    Cut(Long64_t entry);
  virtual Int_t    GetEntry(Long64_t entry);
  virtual Long64_t LoadTree(Long64_t entry);
  virtual void     Init(TTree *tree);
  virtual void     Loop(std::string, std::string);
  virtual Bool_t   Notify();
  virtual void     Show(Long64_t entry = -1);
  virtual Bool_t   track_manager();
  virtual Bool_t   lepton_matching(myTrack *track);
  virtual Bool_t   tag_probe_tester(myTrack *track);
  virtual Bool_t   hadron_cand(myTrack *track);
  virtual Bool_t   event_manager();
  virtual Bool_t   physics_object_manager();
  virtual void     truth_manager();
  virtual void     truthjet_manager();
  virtual void     get_weight(std::string fin);
  virtual void     file_manager(std::string output);
  virtual void     set_branch();
  virtual void     clear_vector();
  virtual void     delete_object();
  virtual Bool_t   set_bin_label(TH1D* hist, std::vector<std::string> label);
  virtual void     print_electron_profile(myElectron* electron);
  virtual void     print_track_profile(myTrack* track);
};

#endif

#ifdef MyAnalysis_cxx


#endif // #ifdef MyAnalysis_cxx
