#include "../MyAnalysis/MyAnalysis.h"
#include "../MyAnalysis/PhysicsObjectProxyBase.h"

Bool_t MyAnalysis::event_manager(){
  m_is_data = IsData;
  m_xsec_eff = weightXsec;
  m_runNumber = RunNumber;
  m_lumiBlock = lumiBlock;
  m_eventNumber = EventNumber;
  m_randomRunNumber = randomRunNumber;
  m_mc_eventweight = weightMCweight;
  m_prw_weight = weightPileupReweighting;
  m_average_interaction_per_crossing = averageInteractionsPerCrossing;
  m_actual_interaction_per_crossing = actualInteractionsPerCrossing;
  m_corrected_average_interaction_per_crossing = CorrectedAverageInteractionsPerCrossing;
  m_pass_electron = LU_SingleElectrontrigger;
  m_pass_muon = LU_SingleMuontrigger;
  m_pass_missingET = LU_METtrigger_SUSYTools;
  if(nTracksFromPrimaryVertex > 1) m_has_pv = 1;
  if(IsPassedBadMuonVeto) m_has_bad_muon = 0;
  else m_has_bad_muon = 1;
  if(IsPassedCosmicMuonVeto) m_has_cosmic_muon = 0;
  else m_has_cosmic_muon = 1;
  if(!IsPassedBadJet && !IsPassedNCBVeto) m_has_bad_jet = 2;
  if(IsPassedBadJet && !IsPassedNCBVeto) m_has_bad_jet = 1;
  if(IsPassedBadJet && IsPassedNCBVeto) m_has_bad_jet = 0;

  if(!DetectorError && nTracksFromPrimaryVertex > 1 && GRL) m_pass_data_quality = 1;
  else m_pass_data_quality = 0;
  if(IsPassedBadMuonVeto && IsPassedCosmicMuonVeto && IsPassedBadJet) m_pass_event_cleaning = 1;
  else m_pass_event_cleaning = 0;
  if(IsPassedBadMuonMETCleaning) m_is_bad_muon_missingET = 0;
  else m_is_bad_muon_missingET = 1;
  m_nMuons = m_goodMuons->size();
  m_nElectrons = m_goodElectrons->size();
  m_nTaus = m_goodTaus->size();
  m_nJets = m_goodJets->size();
  m_nTruths = m_truthParticles->size();

  //return true;
  //if(m_pass_data_quality && m_pass_event_cleaning && (m_pass_electron || m_pass_muon || m_pass_missingET)) return true;
  //if(GRL==true && IsPassedBadEventVeto && DetectorError==0 && (m_pass_electron || m_pass_muon || m_pass_missingET)) return true;
  if(GRL==true && m_pass_event_cleaning && DetectorError==0 && (m_pass_electron || m_pass_muon || m_pass_missingET)) return true;
  else return false;
}

void MyAnalysis::get_weight(std::string fin) {
  TFile* file = TFile::Open(fin.c_str());
  m_sum_of_events = ((TH1D*)file->Get("nEventsProcessedBCK"))->GetBinContent(1);
  m_sum_of_weights = ((TH1D*)file->Get("sumOfWeightsBCK"))->GetBinContent(1);
  m_sum_of_weights_squared = ((TH1D*)file->Get("sumOfWeightsSquaredBCK"))->GetBinContent(1);
  m_h_sum_of_events->SetBinContent(1, m_sum_of_events);
  m_h_sum_of_events->SetBinContent(2, m_sum_of_weights);
  m_h_sum_of_events->SetBinContent(3, m_sum_of_weights_squared);
  file->Close();
}
