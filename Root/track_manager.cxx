#include "../MyAnalysis/MyAnalysis.h"
#include "../MyAnalysis/PhysicsObjectProxyBase.h"
#include "TLorentzVector.h"

Bool_t MyAnalysis::track_manager(){
  std::vector<float> parameterCovMatrix {15, -999.};
  unsigned nPixelTracklets(0), nStdTracks(0), nTracks(0);
  int ii_processed_tracks(-1);
  for(auto &track : *m_Tracks) {
    ++ii_processed_tracks;
    if(!track->pixelBarrel0 || !track->pixelBarrel1 || !track->pixelBarrel2) continue;
    if(track->numberOfGangedFlaggedFakes) continue;
    if(track->numberOfPixelSpoiltHits) continue;
    if(track->nPixelOutliers) continue;
    if(track->dRJet50 < 0.4) continue;
    if(track->p4.Pt()*0.001 < 10.) continue;
    // if(track->ptcone40overPt_1gev > 0.04) continue;
    // if(track->dRMSTrack > 0.4 || track->dRMSTrack < 0.2) {} else continue;
    // if(track->dRMuon > 0.4 || track->dRMuon < 0.2) {} else continue;
    // if(track->dRElectron > 0.4 || track->dRElectron < 0.2) {} else continue;
    if(track->PixelTracklet){
      if((std::fabs(track->p4.Eta()) > 0.1 && std::fabs(track->p4.Eta()) < 1.9) || 
	 (std::fabs(track->KVUEta) > 0.1 && std::fabs(track->KVUEta) < 1.9)) {} else continue;
      //if(track->nSCTHits > 0) continue;
      ++nPixelTracklets;
      //std::cout<<"pixel tracklet"<<std::endl;
    } else {
      if(std::fabs(track->p4.Eta()) > 0.1 && std::fabs(track->p4.Eta()) < 1.9) {} else continue;

      if(!track->pixelBarrel3) continue;
      //if(track->nSCTHits < 6) continue;
      //if(!lepton_matching(track) && !tag_probe_tester(track) && !hadron_cand(track)) continue;
      //if(!tag_probe_tester(track) && !lepton_matching(track)) continue;
      //std::cout<<"std track"<<std::endl;

      ++nStdTracks;
    }

    // if(m_kinematics_region == "highmet" &&
    //    track->p4.Pt()/1000. > 10. &&
    //    track->numberOfContribPixelLayers >= 4 &&
    //    track->nSCTHits >= 6 &&
    //    track->etclus40_topo / track->ptcone40_1gev > 0.5 &&
    //    track->etcone20_topo*0.001 > 3. &&
    //    fabs(track->p4.Eta()) > 0.1 && fabs(track->p4.Eta() < 1.9) &&
    //    fabs(track->z0sinthetawrtPV) < 0.5 && 
    //    fabs(track->d0sigTool) < 1.5)
    //   std::cout<<"run number = "<<RunNumber<<", event number = "<<EventNumber<<", detector error = "<<DetectorError<<", ntracksPV = "<<nTracksFromPrimaryVertex<<", pass bad muon vetp = "<<IsPassedBadMuonVeto<<", pass cosmic muon veto = "<<IsPassedCosmicMuonVeto<<", pass bad jet vato = "<<IsPassedBadJet<<", pass data quality = "<<m_pass_data_quality<<", pass event cleaning = "<<m_pass_event_cleaning<<", missing et = "<<m_MET->p4.Pt()/1000.<<", jet pt = "<<m_goodJets->at(0)->p4.Pt()/1000.<<", pt = "<<track->p4.Pt()/1000.<<", eta = "<<track->p4.Eta()<<", phi = "<<track->p4.Phi()<<", ptcone = "<<track->ptcone40overPt_1gev<<", etclus = "<<track->etclus20_topo/1000.<<", et40/pt40 = "<<track->etclus40_topo / track->ptcone40_1gev<<", d0sig = "<<track->d0sigTool<<", z0ST = "<<track->z0sinthetawrtPV<<", dRjet50 = "<<track->dRJet50<<", dRmuon = "<<track->dRMuon<<", dRelectron = "<<track->dRElectron<<", dRMStrack = "<<track->dRMSTrack<<std::endl;

    // if(m_kinematics_region == "highmet" &&
    //    track->p4.Pt()/1000. > 20. &&
    //    track->numberOfContribPixelLayers >= 4 &&
    //    track->nSCTHits == 0 &&
    //    track->ptcone40overPt_1gev < 0.04 &&
    //    track->etclus20_topo/1000. < 5. &&
    //    track->Quality > 0.1 &&
    //    fabs(track->p4.Eta()) > 0.1 && fabs(track->p4.Eta() < 1.9) &&
    //    fabs(track->z0sinthetawrtPV) < 0.5 && 
    //    fabs(track->d0sigTool) < 1.5)
    //   std::cout<<"run number = "<<RunNumber<<", event number = "<<EventNumber<<"detector error = "<<DetectorError<<", ntracksPV = "<<nTracksFromPrimaryVertex<<", pass bad muon vetp = "<<IsPassedBadMuonVeto<<", pass cosmic muon veto = "<<IsPassedCosmicMuonVeto<<", pass bad jet vato = "<<IsPassedBadJet<<", pass data quality = "<<m_pass_data_quality<<", pass event cleaning = "<<m_pass_event_cleaning<<", missing et = "<<m_MET->p4.Pt()/1000.<<", jet pt = "<<m_goodJets->at(0)->p4.Pt()/1000.<<", pt = "<<track->p4.Pt()/1000.<<", eta = "<<track->p4.Eta()<<", phi = "<<track->p4.Phi()<<", ptcone = "<<track->ptcone40overPt_1gev<<", etclus = "<<track->etclus20_topo/1000.<<", d0sig = "<<track->d0sigTool<<", z0ST = "<<track->z0sinthetawrtPV<<", dRjet50 = "<<track->dRJet50<<", dRmuon = "<<track->dRMuon<<", dRelectron = "<<track->dRElectron<<", dRMStrack = "<<track->dRMSTrack<<std::endl;

    m_track_isTracklet->push_back(track->PixelTracklet);
    m_track_phi->push_back(track->p4.Phi());
    m_track_eta->push_back(track->p4.Eta());
    m_track_pt->push_back(track->p4.Pt()*0.001);
    m_track_e->push_back(track->p4.E()*0.001);
    m_track_charge->push_back(track->charge);
    if(track->PixelTracklet) {
      m_track_KVUPhi->push_back(track->KVUPhi);
      m_track_KVUEta->push_back(track->KVUEta);
      m_track_KVUPt->push_back(track->KVUPt*0.001);
      if(track->KVUqoverp > 0) m_track_KVUCharge->push_back(1);
      else m_track_KVUCharge->push_back(-1);
      m_track_KVUChi2Prob->push_back(track->KVUQuality);
    } else {
      m_track_KVUPhi->push_back(-999.);
      m_track_KVUEta->push_back(-999.);
      m_track_KVUPt->push_back(-999.);
      m_track_KVUCharge->push_back(-999);
      m_track_KVUChi2Prob->push_back(-999.);
    }
    m_track_pixelBarrel_0->push_back(track->pixelBarrel0);
    m_track_pixelBarrel_1->push_back(track->pixelBarrel1);
    m_track_pixelBarrel_2->push_back(track->pixelBarrel2);
    m_track_pixelBarrel_3->push_back(track->pixelBarrel3);
    m_track_sctBarrel_0->push_back(track->sctBarrel0);
    m_track_sctBarrel_1->push_back(track->sctBarrel1);
    m_track_sctBarrel_2->push_back(track->sctBarrel2);
    m_track_sctBarrel_3->push_back(track->sctBarrel3);
    m_track_sctEndcap_0->push_back(track->sctEndCap0);
    m_track_sctEndcap_1->push_back(track->sctEndCap1);
    m_track_sctEndcap_2->push_back(track->sctEndCap2);
    m_track_sctEndcap_3->push_back(track->sctEndCap3);
    m_track_sctEndcap_4->push_back(track->sctEndCap4);
    m_track_sctEndcap_5->push_back(track->sctEndCap5);
    m_track_sctEndcap_6->push_back(track->sctEndCap6);
    m_track_sctEndcap_7->push_back(track->sctEndCap7);
    m_track_sctEndcap_8->push_back(track->sctEndCap8);
    unsigned nPixECHits = 0;
    if(track->pixelEndCap0) ++nPixECHits;
    if(track->pixelEndCap1) ++nPixECHits;
    if(track->pixelEndCap2) ++nPixECHits;
    m_track_nPixelEndcap->push_back(nPixECHits);
    m_track_nPixHits->push_back(track->nPixHits);
    m_track_nPixHoles->push_back(track->nPixelHoles);
    m_track_nPixSharedHits->push_back(track->nPixelSharedHits);
    m_track_nPixOutliers->push_back(track->nPixelOutliers);
    m_track_nPixDeadSensors->push_back(track->numberOfPixelDeadSensors);
    m_track_nContribPixelLayers->push_back(track->numberOfContribPixelLayers);
    m_track_nGangedFlaggedFakes->push_back(track->numberOfGangedFlaggedFakes);
    m_track_nPixelSpoiltHits->push_back(track->numberOfPixelSpoiltHits);
    m_track_nSCTHits->push_back(track->nSCTHits);
    m_track_nSCTHoles->push_back(track->nSCTHoles);
    m_track_nSCTSharedHits->push_back(track->nSCTSharedHits);
    m_track_nSCTOutliers->push_back(track->nSCTOutliers);
    m_track_nSCTDeadSensors->push_back(track->numberOfSCTDeadSensors);
    m_track_nTRTHits->push_back(track->nTRTHits);
    m_track_nTRTHoles->push_back(track->nTRTHoles);
    m_track_nTRTSharedHits->push_back(track->nTRTSharedHits);
    m_track_nTRTDeadSensors->push_back(track->numberOfTRTDeadStraws);
    m_track_nTRTOutliers->push_back(track->nTRTOutliers);
    m_track_dEdx->push_back(track->pixeldEdx);
    m_track_ptCone20OverPt->push_back(track->ptcone20overPt_1gev);
    m_track_ptCone30OverPt->push_back(track->ptcone30overPt_1gev);
    m_track_ptCone40OverPt->push_back(track->ptcone40overPt_1gev);
    if(track->PixelTracklet) {
      m_track_ptCone20OverKVUPt->push_back(track->ptcone20overPt_1gev*track->p4.Pt()/track->KVUPt);
      m_track_ptCone30OverKVUPt->push_back(track->ptcone30overPt_1gev*track->p4.Pt()/track->KVUPt);
      m_track_ptCone40OverKVUPt->push_back(track->ptcone40overPt_1gev*track->p4.Pt()/track->KVUPt);
    } else {
      m_track_ptCone20OverKVUPt->push_back(-999.);
      m_track_ptCone30OverKVUPt->push_back(-999.);
      m_track_ptCone40OverKVUPt->push_back(-999.);
    }
    m_track_eTTopoClus20->push_back(track->etclus20_topo*0.001);
    m_track_eTTopoClus30->push_back(track->etclus30_topo*0.001);
    m_track_eTTopoClus40->push_back(track->etclus40_topo*0.001);
    m_track_eTTopoCone20->push_back(track->etcone20_topo*0.001);
    m_track_eTTopoCone30->push_back(track->etcone30_topo*0.001);
    m_track_eTTopoCone40->push_back(track->etcone40_topo*0.001);
    m_track_dRJet20->push_back(track->dRJet20);
    m_track_dRJet50->push_back(track->dRJet50);
    m_track_dRMSTrack->push_back(track->dRMSTrack);
    m_track_dRMuon->push_back(track->dRMuon);
    m_track_dRElectron->push_back(track->dRElectron);
    m_track_d0->push_back(track->d0);
    m_track_d0Sig->push_back(track->d0sigTool);
    m_track_z0->push_back(track->z0);
    m_track_z0PV->push_back(track->z0wrtPV);
    m_track_z0PVSinTheta->push_back(track->z0sinthetawrtPV);
    m_track_chi2Prob->push_back(track->Quality);
    m_track_electron_key->push_back(-999);
    m_track_muon_key->push_back(-999);

    for(unsigned ii_electron = 0; ii_electron < m_electron_index->size(); ++ii_electron) {
      for(unsigned ii_track_index = 0; ii_track_index < m_electron_index->at(ii_electron).size(); ++ii_track_index) {
	if(ii_processed_tracks == m_electron_index->at(ii_electron).at(ii_track_index)) {
	  m_track_electron_key->back() = m_electron_key->at(ii_electron);
	}
      }
    }
    for(unsigned ii_muon = 0; ii_muon < m_muon_index->size(); ++ii_muon) {
      for(unsigned ii_track_index = 0; ii_track_index < m_muon_index->at(ii_muon).size(); ++ii_track_index) {
	if(ii_processed_tracks == m_muon_index->at(ii_muon).at(ii_track_index)) {
	  m_track_muon_key->back() = m_muon_key->at(ii_muon);
	}
      }
    }

    // m_track_parameterCovMatrix->push_back(parameterCovMatrix);
    if(!IsData) {
      m_track_truth_phi->push_back(track->TruthPhi);
      m_track_truth_eta->push_back(track->TruthEta);
      m_track_truth_pt->push_back(track->TruthPt/1000.);
      m_track_truth_e->push_back(-999.);
      m_track_truth_charge->push_back(track->TruthCharge);
      m_track_truth_pdgid->push_back(-999);
      m_track_truth_barcode->push_back(-999);
      m_track_truth_nChildren->push_back(-999);
      m_track_truth_properTime->push_back(track->DecayProperTime);
      m_track_truth_decayr->push_back(track->DecayRadius);
      m_track_truth_matchingProb->push_back(-999.);
      m_track_truth_parent_phi->push_back(-999.);
      m_track_truth_parent_eta->push_back(-999.);
      m_track_truth_parent_pt->push_back(-999.);
      m_track_truth_parent_e->push_back(-999.);
      m_track_truth_parent_charge->push_back(-999);
      m_track_truth_parent_pdgid->push_back(-999);
      m_track_truth_parent_barcode->push_back(-999);
      m_track_truth_parent_nChildren->push_back(-999);
      m_track_truth_parent_decayr->push_back(-999.);
    } else {
      m_track_truth_phi->push_back(-999.);
      m_track_truth_eta->push_back(-999.);
      m_track_truth_pt->push_back(-999.);
      m_track_truth_e->push_back(-999.);
      m_track_truth_charge->push_back(-999);
      m_track_truth_pdgid->push_back(-999);
      m_track_truth_barcode->push_back(-999);
      m_track_truth_nChildren->push_back(-999);
      m_track_truth_properTime->push_back(-999.);
      m_track_truth_decayr->push_back(-999.);
      m_track_truth_matchingProb->push_back(-999.);
      m_track_truth_parent_phi->push_back(-999.);
      m_track_truth_parent_eta->push_back(-999.);
      m_track_truth_parent_pt->push_back(-999.);
      m_track_truth_parent_e->push_back(-999.);
      m_track_truth_parent_charge->push_back(-999);
      m_track_truth_parent_pdgid->push_back(-999);
      m_track_truth_parent_barcode->push_back(-999);
      m_track_truth_parent_nChildren->push_back(-999);
      m_track_truth_parent_decayr->push_back(-999.);
    }

    ++nTracks;
  }

  if(!nPixelTracklets && !nStdTracks) return false;
  m_nTracks = nTracks;
  return true;
}

bool MyAnalysis::lepton_matching(myTrack *track) {
  // float min_muon_dR = 999.;
  // bool is_muon_matched = false;
  // for(auto &muon : *m_goodMuons) {
  //   float dPhi = TVector2::Phi_mpi_pi(muon->p4.Phi() - track->p4.Phi());
  //   float dEta = muon->p4.Eta() - track->p4.Eta();
  //   float dR = std::sqrt(dPhi*dPhi + dEta*dEta);
  //   if(dR < min_muon_dR) min_muon_dR = dR;
  // }
  // if(min_muon_dR < 0.2) is_muon_matched = true;

  float min_electron_dR = 999.;
  bool is_electron_matched = false;
  for(auto &electron : *m_goodElectrons) {
    float dPhi = TVector2::Phi_mpi_pi(electron->p4.Phi() - track->p4.Phi());
    float dEta = electron->p4.Eta() - track->p4.Eta();
    float dR = std::sqrt(dPhi*dPhi + dEta*dEta);
    if(dR < min_electron_dR) min_electron_dR = dR;
  }
  if(min_electron_dR < 0.2) is_electron_matched = true;
  
  //if( is_muon_matched || is_electron_matched ) return true;
  if( is_electron_matched ) return true;
  else return false;
}

bool MyAnalysis::tag_probe_tester(myTrack *track) {
  TLorentzVector track_vec;
  const double e_mass = 0.51099895;
  bool is_zevent(false);
  if(!m_goodElectrons->size() && !m_goodMuons->size()) return false;
  for(auto electron : *m_goodElectrons) {
    TLorentzVector electron_vec = electron->p4;
    track_vec.SetPtEtaPhiM(track->p4.Pt(), track->p4.Eta(), track->p4.Phi(), e_mass);
    if(std::fabs((track_vec+electron_vec).M()*0.001 - zmass) < 20.)
      is_zevent = true;

    //if(electron->p4.DeltaR(track->p4) > 0.001 && electron->p4.Pt()*0.001 > 30. && is_zevent) {
    if(electron->p4.Pt()*0.001 > 30. && is_zevent) {
      print_electron_profile(electron);
      print_track_profile(track);
    }


    if(!m_goodMuons->size() &&
       LU_SingleElectrontrigger &&
       electron->p4.Pt()*0.001 > 30. &&
       electron->IsSignal &&
       //electron->triggerMatching.at("LU_SingleElectrontrigger") &&
       track->dRMuon > 0.4 &&
       track->dRMSTrack > 0.4 &&
       !track->nSiHoles &&
       std::fabs(track->z0sinthetawrtPV) < 0.5 && 
       std::fabs(track->d0sigTool) < 1.5 &&
       track->Quality > 0.1 &&
       track->etclus20_topo*0.001 > 10. &&
       track->charge * electron->Charge == -1 &&
       std::fabs((track_vec+electron_vec).M()*0.001 - zmass) < 10.) {
      m_h_zmass_os_greater10->Fill((track_vec+electron_vec).M()*0.001);
      std::cout<<"Paas: "<<(track_vec+electron_vec).M()*0.001<<std::endl;

    }
  }
  // TLorentzVector muon;
  // if(m_goodMuons->size()) {
  //   electron.SetPtEtaPhiE(m_goodMuons->at(0)->p4.Pt(), m_goodMuons->at(0)->p4.Eta(), m_goodMuons->at(0)->p4.Phi(), m_goodMuons->at(0)->p4.E());
  //   if(std::fabs((track->p4+muon).M()*0.001 - zmass) < 20.) is_zevent = true;
  // }
  return is_zevent;
}

bool MyAnalysis::hadron_cand(myTrack *track) {
  if(track->nTRTHits >= 15 &&
     track->ptcone40overPt_1gev < 0.04 &&
     track->etcone20_topo * 0.001 > 3. &&
     track->etclus40_topo / track->ptcone40_1gev > 0.5) return true;
  else return false;
}

void MyAnalysis::print_electron_profile(myElectron *electron) {
  std::cout<<"nElectrons = "<<m_goodElectrons->size()<<", electron: pT = "<<electron->p4.Pt()*0.001<<", eta = "<<electron->p4.Eta()<<", phi = "<<electron->p4.Phi()<<", e = "<<electron->p4.E()*0.001<<", charge = "<<electron->Charge<<", is signal = "<<electron->IsSignal<<", trig = "<<electron->triggerMatching.at("LU_SingleElectrontrigger")<<std::endl;
}

void MyAnalysis::print_track_profile(myTrack *track) {
  std::cout<<"track: pT = "<<track->p4.Pt()*0.001<<", eta = "<<track->p4.Eta()<<", phi = "<<track->p4.Phi()<<", e = "<<track->p4.E()*0.001<<", charge = "<<track->charge<<", eT = "<<track->etclus20_topo*0.001<<", dRMuon = "<<track->dRMuon<<", dRMS = "<<track->dRMSTrack<<", d0sig = "<<std::fabs(track->d0sigTool)<<", z0ST = "<<std::fabs(track->z0sinthetawrtPV)<<", quality = "<<track->Quality<<std::endl;
}
