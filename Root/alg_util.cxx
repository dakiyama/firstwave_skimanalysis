#include "../MyAnalysis/MyAnalysis.h"
#include "../MyAnalysis/PhysicsObjectProxyBase.h"

MyAnalysis::MyAnalysis(TTree *tree) : 
  fChain(0),
  zmass(91.1876)
{
  // if parameter tree is not specified (or zero), connect the file
  // used to generate this class and read the Tree.
  if (tree == 0) {

#ifdef SINGLE_TREE
    // The following code should be used if you want this class to access
    // a single tree instead of a chain
    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("Memory Directory");
    if (!f || !f->IsOpen()) {
      f = new TFile("Memory Directory");
    }
    f->GetObject("tree",tree);

#else // SINGLE_TREE

      // The following code should be used if you want this class to access a chain
      // of trees.
    TChain * chain = new TChain("tree","");
    chain->Add("/gpfs/fs7001/toshiaki/Common_v2_3_4/mc16_13TeV.448394.MGPy8EG_A14N23LO_mAMSB_C1N1_5000_243800_LL0p2_MET60.deriv.Common_v2_3_4.e7157_s3353_r10724_p3877/out.448394_mc16e._000001.common_ntuple.root/tree");
    //chain->Add("/gpfs/fs7001/dakiyama/DTStudy/DTAnalysis/second_wave/first_wave_ntuple_maker/second_wave/myntuple_21_2_138.root/tree");
    tree = chain;
#endif // SINGLE_TREE

  }
  Init(tree);
}


MyAnalysis::~MyAnalysis()
{
  if (!fChain) return;
  delete fChain->GetCurrentFile();
  delete m_MET;
  delete m_MET_Electron;
  delete m_MET_Muon;
  delete m_MET_XeTrigger;
  delete m_mcEventWeights;
  m_MET_DRAW.reset();
  m_drawJets.reset();

  delete m_goodJets;
  delete m_truthJets;
  delete m_goodMuons;
  delete m_goodTaus;
  delete m_msTracks;
  delete m_goodElectrons;
  delete m_egammaClusters;
  delete m_truthParticles;
  delete m_Tracks;
  delete m_threeLayerTracks;
  delete m_fourLayerTracks;


  m_MET                 = nullptr;
  m_MET_Electron        = nullptr;
  m_MET_Muon            = nullptr;
  m_MET_XeTrigger       = nullptr;
  m_MET_DRAW            = nullptr;
  m_mcEventWeights      = nullptr;
  m_drawJets            = nullptr;
  m_goodJets            = nullptr;
  m_truthJets           = nullptr;
  m_goodMuons           = nullptr;
  m_goodTaus            = nullptr;
  m_msTracks            = nullptr;
  m_goodElectrons       = nullptr;
  m_egammaClusters      = nullptr;
  m_truthParticles      = nullptr;
  m_threeLayerTracks    = nullptr;
  m_fourLayerTracks     = nullptr;
  m_Tracks              = nullptr;

  delete m_tca_goodJets;
  delete m_tca_truthJets;
  delete m_tca_goodMuons;
  delete m_tca_goodTaus;
  delete m_tca_msTracks;
  delete m_tca_goodElectrons;
  delete m_tca_egammaClusters;
  delete m_tca_truthParticles;
  delete m_tca_Tracks;
  delete m_tca_threeLayerTracks;
  delete m_tca_fourLayerTracks;

  m_tca_goodJets = nullptr;
  m_tca_truthJets = nullptr;
  m_tca_goodMuons = nullptr;
  m_tca_goodTaus = nullptr;
  m_tca_msTracks = nullptr;
  m_tca_goodElectrons = nullptr;
  m_tca_egammaClusters = nullptr;
  m_tca_truthParticles = nullptr;
  m_tca_Tracks = nullptr;
  m_tca_threeLayerTracks = nullptr;
  m_tca_fourLayerTracks = nullptr;
}

Int_t MyAnalysis::GetEntry(Long64_t entry)
{
  // Read contents of entry.
  if (!fChain) return 0;
  //return fChain->GetEntry(entry);
  
  //m_drawJets->clear();
  m_goodJets->clear();
  m_truthJets->clear();
  m_goodMuons->clear();
  m_goodTaus->clear();
  m_msTracks->clear();
  m_goodElectrons->clear();
  m_egammaClusters->clear();
  m_truthParticles->clear();
  m_threeLayerTracks->clear();
  m_fourLayerTracks->clear();
  m_Tracks->clear();

  //std::cout<<m_tca_goodJets->GetEntries()<<std::endl;
  for(int i = 0; i < ( m_tca_goodJets->GetEntries());         ++i ) m_goodJets->push_back        ( (myJet*) m_tca_goodJets->ConstructedAt(i) );
  for(int i = 0; i < ( m_tca_truthJets->GetEntries());        ++i ) m_truthJets->push_back       ( (myJet*) m_tca_truthJets->ConstructedAt(i) );
  for(int i = 0; i < ( m_tca_goodMuons->GetEntries());        ++i ) m_goodMuons->push_back       ( (myMuon*) m_tca_goodMuons->ConstructedAt(i) );
  for(int i = 0; i < ( m_tca_goodTaus->GetEntries());         ++i ) m_goodTaus->push_back        ( (myTau*) m_tca_goodTaus->ConstructedAt(i) );
  for(int i = 0; i < ( m_tca_msTracks->GetEntries());         ++i ) m_msTracks->push_back        ( (myTrack*) m_tca_msTracks->ConstructedAt(i) );
  for(int i = 0; i < ( m_tca_goodElectrons->GetEntries());    ++i ) m_goodElectrons->push_back   ( (myElectron*) m_tca_goodElectrons->ConstructedAt(i) );
  for(int i = 0; i < ( m_tca_egammaClusters->GetEntries());   ++i ) m_egammaClusters->push_back  ( (myCaloCluster*) m_tca_egammaClusters->ConstructedAt(i) );
  for(int i = 0; i < ( m_tca_truthParticles->GetEntries());   ++i ) m_truthParticles->push_back  ( (myTruth*) m_tca_truthParticles->ConstructedAt(i) );
  for(int i = 0; i < ( m_tca_Tracks->GetEntries());           ++i ) m_Tracks->push_back          ( (myTrack*) m_tca_Tracks->ConstructedAt(i) );
  for(int i = 0; i < ( m_tca_threeLayerTracks->GetEntries()); ++i ) m_threeLayerTracks->push_back( (myTrack*) m_tca_threeLayerTracks->ConstructedAt(i) );
  for(int i = 0; i < ( m_tca_fourLayerTracks->GetEntries());  ++i ) m_fourLayerTracks->push_back ( (myTrack*) m_tca_fourLayerTracks->ConstructedAt(i) );

  return 0;
}

Long64_t MyAnalysis::LoadTree(Long64_t entry)
{
  // Set the environment to read one entry
  if (!fChain) return -5;
  Long64_t centry = fChain->LoadTree(entry);
  if (centry < 0) return centry;
  if (fChain->GetTreeNumber() != fCurrent) {
    fCurrent = fChain->GetTreeNumber();
    Notify();
  }
  return centry;
}

void MyAnalysis::Init(TTree *tree)
{
  // Set object pointer                                                                                                                                                                                                                      

  m_MET                            = new myMET();
  m_MET_Electron                   = new myMET();
  m_MET_Muon                       = new myMET();
  m_MET_XeTrigger                  = new myMET();
  m_mcEventWeights                 = new std::vector<float>;
  //m_MET_DRAW                       = std::make_unique<myMET>();
  //m_drawJets                       = std::make_unique<std::vector<myJet*>>();
  m_goodJets                       = new std::vector<myJet*>();
  m_truthJets                      = new std::vector<myJet*>();
  m_goodMuons                      = new std::vector<myMuon*>();
  m_goodTaus                       = new std::vector<myTau*>();
  m_msTracks                       = new std::vector<myTrack*>();
  m_goodElectrons                  = new std::vector<myElectron*>();
  m_egammaClusters                 = new std::vector<myCaloCluster*>();
  m_truthParticles                 = new std::vector<myTruth*>();
  m_Tracks                         = new std::vector<myTrack*>();
  m_threeLayerTracks               = new std::vector<myTrack*>();
  m_fourLayerTracks                = new std::vector<myTrack*>();

  //m_tca_drawJets                   = std::unique_ptr< TClonesArray >( new TClonesArray("myJet") );
  m_tca_goodJets                   = new TClonesArray("myJet") ;
  m_tca_truthJets                  = new TClonesArray("myJet") ;
  m_tca_goodMuons                  = new TClonesArray("myMuon");
  m_tca_goodTaus                   = new TClonesArray("myTau");
  m_tca_msTracks                   = new TClonesArray("myTrack");
  m_tca_goodElectrons              = new TClonesArray("myElectron");
  m_tca_egammaClusters             = new TClonesArray("myCaloCluster");
  m_tca_truthParticles             = new TClonesArray("myTruth");
  m_tca_Tracks                     = new TClonesArray("myTrack");
  m_tca_threeLayerTracks           = new TClonesArray("myTrack");
  m_tca_fourLayerTracks            = new TClonesArray("myTrack");

  //m_drawJets->clear();
  m_goodJets->clear();
  m_truthJets->clear();
  m_goodMuons->clear();
  m_goodTaus->clear();
  m_msTracks->clear();
  m_goodElectrons->clear();
  m_egammaClusters->clear();
  m_truthParticles->clear();
  m_Tracks->clear();
  m_threeLayerTracks->clear();
  m_fourLayerTracks->clear();
  m_mcEventWeights->clear();

  //mcEventWeights=0;

  if (!tree) return;
  fChain = tree;
  fCurrent = -1;
  fChain->SetMakeClass(1);
  fChain->SetMakeClass(kFALSE);

  fChain->SetBranchAddress("MET.",                                    &m_MET );
  fChain->SetBranchAddress("MET_ForEleCR.",                           &m_MET_Electron );
  fChain->SetBranchAddress("MET_ForMuCR.",                            &m_MET_Muon );
  fChain->SetBranchAddress("MET_ForXeTrigger.",                       &m_MET_XeTrigger );
  fChain->SetBranchAddress("mcEventWeights",                          &m_mcEventWeights );
  fChain->SetBranchAddress("GoodJets",                                &m_tca_goodJets);
  fChain->SetBranchAddress("TruthJets",                               &m_tca_truthJets);
  fChain->SetBranchAddress("GoodMuons",                               &m_tca_goodMuons);
  fChain->SetBranchAddress("GoodTaus",                                &m_tca_goodTaus);
  fChain->SetBranchAddress("GoodElectrons",                           &m_tca_goodElectrons);
  fChain->SetBranchAddress("EgammaClusters",                          &m_tca_egammaClusters);
  fChain->SetBranchAddress("Truths",                                  &m_tca_truthParticles);
  fChain->SetBranchAddress("MSTracks",                                &m_tca_msTracks);
  fChain->SetBranchAddress("Tracks",                                  &m_tca_Tracks);
  fChain->SetBranchAddress("ThreeLayerTracks",                        &m_tca_threeLayerTracks);
  fChain->SetBranchAddress("FourLayerTracks",                         &m_tca_fourLayerTracks);

  fChain->SetBranchAddress("GRL",                                     &GRL);
  fChain->SetBranchAddress("IsPassedBadEventVeto",                    &IsPassedBadEventVeto);
  fChain->SetBranchAddress("DetectorError",                           &DetectorError);
  fChain->SetBranchAddress("IsPassedMETTrigger",                      &IsPassedMETTrigger);
  fChain->SetBranchAddress("LU_METtrigger_SUSYTools",                 &LU_METtrigger_SUSYTools);
  fChain->SetBranchAddress("LU_SingleElectrontrigger",                &LU_SingleElectrontrigger);
  fChain->SetBranchAddress("LU_SingleMuontrigger",                    &LU_SingleMuontrigger);
  fChain->SetBranchAddress("IsPassedBadJet",                          &IsPassedBadJet);
  fChain->SetBranchAddress("IsPassedNCBVeto",                         &IsPassedNCBVeto);
  fChain->SetBranchAddress("IsPassedBadMuonVeto",                     &IsPassedBadMuonVeto);
  fChain->SetBranchAddress("IsPassedCosmicMuonVeto",                  &IsPassedCosmicMuonVeto);
  fChain->SetBranchAddress("IsPassedLeptonVeto",                      &IsPassedLeptonVeto);
  fChain->SetBranchAddress("IsPassedBadMuonMETCleaning",              &IsPassedBadMuonMETCleaning);
  fChain->SetBranchAddress("IsData",                                  &IsData);
  fChain->SetBranchAddress("actualInteractionsPerCrossing",           &actualInteractionsPerCrossing);
  fChain->SetBranchAddress("nTracksFromPrimaryVertex",                &nTracksFromPrimaryVertex);
  fChain->SetBranchAddress("weightXsec",                              &weightXsec);
  fChain->SetBranchAddress("weightMCweight",                          &weightMCweight);
  fChain->SetBranchAddress("weightPileupReweighting",                 &weightPileupReweighting);
  fChain->SetBranchAddress("treatAsYear",                             &treatAsYear);
  fChain->SetBranchAddress("RunNumber",                               &RunNumber);
  fChain->SetBranchAddress("EventNumber",                             &EventNumber);
}

Bool_t MyAnalysis::Notify()
{
  // The Notify() function is called when a new file is opened. This
  // can be either for a new TTree in a TChain or when when a new TTree
  // is started when using PROOF. It is normally not necessary to make changes
  // to the generated code, but the routine can be extended by the
  // user if needed. The return value is currently not used.

  return kTRUE;
}

void MyAnalysis::Show(Long64_t entry)
{
  // Print contents of entry.
  // If entry is not specified, print current entry
  if (!fChain) return;
  fChain->Show(entry);
}
Int_t MyAnalysis::Cut(Long64_t entry)
{
  // This function may be called from Loop.
  // returns  1 if entry is accepted.
  // returns -1 otherwise.
  return 1;
}
