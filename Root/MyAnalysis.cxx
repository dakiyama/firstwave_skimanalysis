#define MyAnalysis_cxx
#include "../MyAnalysis/MyAnalysis.h"
#include "../MyAnalysis/PhysicsObjectProxyBase.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>

void MyAnalysis::Loop(std::string path, std::string output)
{
  if (fChain == 0) return;

  Long64_t nentries = fChain->GetEntriesFast();

  set_branch();
  get_weight(path+"/"+output);
  file_manager(output);

  bool got_xsec_eff(false);
  Long64_t nbytes = 0, nb = 0;
  for (Long64_t jentry=0; jentry<nentries;jentry++) {
    Long64_t ientry = LoadTree(jentry);
    if (ientry < 0) break;
    nb = fChain->GetEntry(jentry);   nbytes += nb;
    GetEntry(jentry);
    if(jentry % 1000 == 0) {
      std::cout<<"Loop() -----> "<<jentry<<std::endl;
    }

    clear_vector();
    if(!event_manager()) continue;
    if(!got_xsec_eff) {
      m_h_xsec_eff->SetBinContent(1, m_xsec_eff);
      got_xsec_eff = true;
    }
    if(!physics_object_manager()) continue;
    if(!track_manager()) continue;
    if(!IsData) {
      truth_manager();
      truthjet_manager();
    }

    m_tree->Fill();
  } // -> void Loop()

  m_tree->Write();
  TDirectoryFile* dir = new TDirectoryFile("EventInfo", "EventInfo");
  dir->cd();
  m_h_sum_of_events->Write();
  m_h_xsec_eff->Write();
  m_h_zmass_os_greater10->Write();
  delete_object();
}
