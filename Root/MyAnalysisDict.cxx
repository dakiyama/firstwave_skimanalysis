// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME MyAnalysisDict
#define R__NO_DEPRECATION

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Header files passed as explicit arguments
#include "../MyAnalysis/PhysicsObjectProxyBase.h"

// Header files passed via #pragma extra_include

// The generated code does not explicitly qualify STL entities
namespace std {} using namespace std;

namespace ROOT {
   static void *new_PhysObjBase(void *p = 0);
   static void *newArray_PhysObjBase(Long_t size, void *p);
   static void delete_PhysObjBase(void *p);
   static void deleteArray_PhysObjBase(void *p);
   static void destruct_PhysObjBase(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::PhysObjBase*)
   {
      ::PhysObjBase *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::PhysObjBase >(0);
      static ::ROOT::TGenericClassInfo 
         instance("PhysObjBase", ::PhysObjBase::Class_Version(), "../MyAnalysis/PhysicsObjectProxyBase.h", 17,
                  typeid(::PhysObjBase), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::PhysObjBase::Dictionary, isa_proxy, 4,
                  sizeof(::PhysObjBase) );
      instance.SetNew(&new_PhysObjBase);
      instance.SetNewArray(&newArray_PhysObjBase);
      instance.SetDelete(&delete_PhysObjBase);
      instance.SetDeleteArray(&deleteArray_PhysObjBase);
      instance.SetDestructor(&destruct_PhysObjBase);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::PhysObjBase*)
   {
      return GenerateInitInstanceLocal((::PhysObjBase*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::PhysObjBase*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_myJet(void *p = 0);
   static void *newArray_myJet(Long_t size, void *p);
   static void delete_myJet(void *p);
   static void deleteArray_myJet(void *p);
   static void destruct_myJet(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::myJet*)
   {
      ::myJet *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::myJet >(0);
      static ::ROOT::TGenericClassInfo 
         instance("myJet", ::myJet::Class_Version(), "../MyAnalysis/PhysicsObjectProxyBase.h", 45,
                  typeid(::myJet), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::myJet::Dictionary, isa_proxy, 4,
                  sizeof(::myJet) );
      instance.SetNew(&new_myJet);
      instance.SetNewArray(&newArray_myJet);
      instance.SetDelete(&delete_myJet);
      instance.SetDeleteArray(&deleteArray_myJet);
      instance.SetDestructor(&destruct_myJet);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::myJet*)
   {
      return GenerateInitInstanceLocal((::myJet*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::myJet*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_myElectron(void *p = 0);
   static void *newArray_myElectron(Long_t size, void *p);
   static void delete_myElectron(void *p);
   static void deleteArray_myElectron(void *p);
   static void destruct_myElectron(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::myElectron*)
   {
      ::myElectron *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::myElectron >(0);
      static ::ROOT::TGenericClassInfo 
         instance("myElectron", ::myElectron::Class_Version(), "../MyAnalysis/PhysicsObjectProxyBase.h", 62,
                  typeid(::myElectron), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::myElectron::Dictionary, isa_proxy, 4,
                  sizeof(::myElectron) );
      instance.SetNew(&new_myElectron);
      instance.SetNewArray(&newArray_myElectron);
      instance.SetDelete(&delete_myElectron);
      instance.SetDeleteArray(&deleteArray_myElectron);
      instance.SetDestructor(&destruct_myElectron);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::myElectron*)
   {
      return GenerateInitInstanceLocal((::myElectron*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::myElectron*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_myMuon(void *p = 0);
   static void *newArray_myMuon(Long_t size, void *p);
   static void delete_myMuon(void *p);
   static void deleteArray_myMuon(void *p);
   static void destruct_myMuon(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::myMuon*)
   {
      ::myMuon *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::myMuon >(0);
      static ::ROOT::TGenericClassInfo 
         instance("myMuon", ::myMuon::Class_Version(), "../MyAnalysis/PhysicsObjectProxyBase.h", 83,
                  typeid(::myMuon), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::myMuon::Dictionary, isa_proxy, 4,
                  sizeof(::myMuon) );
      instance.SetNew(&new_myMuon);
      instance.SetNewArray(&newArray_myMuon);
      instance.SetDelete(&delete_myMuon);
      instance.SetDeleteArray(&deleteArray_myMuon);
      instance.SetDestructor(&destruct_myMuon);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::myMuon*)
   {
      return GenerateInitInstanceLocal((::myMuon*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::myMuon*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_myTau(void *p = 0);
   static void *newArray_myTau(Long_t size, void *p);
   static void delete_myTau(void *p);
   static void deleteArray_myTau(void *p);
   static void destruct_myTau(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::myTau*)
   {
      ::myTau *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::myTau >(0);
      static ::ROOT::TGenericClassInfo 
         instance("myTau", ::myTau::Class_Version(), "../MyAnalysis/PhysicsObjectProxyBase.h", 104,
                  typeid(::myTau), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::myTau::Dictionary, isa_proxy, 4,
                  sizeof(::myTau) );
      instance.SetNew(&new_myTau);
      instance.SetNewArray(&newArray_myTau);
      instance.SetDelete(&delete_myTau);
      instance.SetDeleteArray(&deleteArray_myTau);
      instance.SetDestructor(&destruct_myTau);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::myTau*)
   {
      return GenerateInitInstanceLocal((::myTau*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::myTau*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_myCaloCluster(void *p = 0);
   static void *newArray_myCaloCluster(Long_t size, void *p);
   static void delete_myCaloCluster(void *p);
   static void deleteArray_myCaloCluster(void *p);
   static void destruct_myCaloCluster(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::myCaloCluster*)
   {
      ::myCaloCluster *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::myCaloCluster >(0);
      static ::ROOT::TGenericClassInfo 
         instance("myCaloCluster", ::myCaloCluster::Class_Version(), "../MyAnalysis/PhysicsObjectProxyBase.h", 125,
                  typeid(::myCaloCluster), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::myCaloCluster::Dictionary, isa_proxy, 4,
                  sizeof(::myCaloCluster) );
      instance.SetNew(&new_myCaloCluster);
      instance.SetNewArray(&newArray_myCaloCluster);
      instance.SetDelete(&delete_myCaloCluster);
      instance.SetDeleteArray(&deleteArray_myCaloCluster);
      instance.SetDestructor(&destruct_myCaloCluster);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::myCaloCluster*)
   {
      return GenerateInitInstanceLocal((::myCaloCluster*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::myCaloCluster*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_myMET(void *p = 0);
   static void *newArray_myMET(Long_t size, void *p);
   static void delete_myMET(void *p);
   static void deleteArray_myMET(void *p);
   static void destruct_myMET(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::myMET*)
   {
      ::myMET *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::myMET >(0);
      static ::ROOT::TGenericClassInfo 
         instance("myMET", ::myMET::Class_Version(), "../MyAnalysis/PhysicsObjectProxyBase.h", 137,
                  typeid(::myMET), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::myMET::Dictionary, isa_proxy, 4,
                  sizeof(::myMET) );
      instance.SetNew(&new_myMET);
      instance.SetNewArray(&newArray_myMET);
      instance.SetDelete(&delete_myMET);
      instance.SetDeleteArray(&deleteArray_myMET);
      instance.SetDestructor(&destruct_myMET);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::myMET*)
   {
      return GenerateInitInstanceLocal((::myMET*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::myMET*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_myTruth(void *p = 0);
   static void *newArray_myTruth(Long_t size, void *p);
   static void delete_myTruth(void *p);
   static void deleteArray_myTruth(void *p);
   static void destruct_myTruth(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::myTruth*)
   {
      ::myTruth *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::myTruth >(0);
      static ::ROOT::TGenericClassInfo 
         instance("myTruth", ::myTruth::Class_Version(), "../MyAnalysis/PhysicsObjectProxyBase.h", 150,
                  typeid(::myTruth), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::myTruth::Dictionary, isa_proxy, 4,
                  sizeof(::myTruth) );
      instance.SetNew(&new_myTruth);
      instance.SetNewArray(&newArray_myTruth);
      instance.SetDelete(&delete_myTruth);
      instance.SetDeleteArray(&deleteArray_myTruth);
      instance.SetDestructor(&destruct_myTruth);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::myTruth*)
   {
      return GenerateInitInstanceLocal((::myTruth*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::myTruth*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_myTrack(void *p = 0);
   static void *newArray_myTrack(Long_t size, void *p);
   static void delete_myTrack(void *p);
   static void deleteArray_myTrack(void *p);
   static void destruct_myTrack(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::myTrack*)
   {
      ::myTrack *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::myTrack >(0);
      static ::ROOT::TGenericClassInfo 
         instance("myTrack", ::myTrack::Class_Version(), "../MyAnalysis/PhysicsObjectProxyBase.h", 194,
                  typeid(::myTrack), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::myTrack::Dictionary, isa_proxy, 4,
                  sizeof(::myTrack) );
      instance.SetNew(&new_myTrack);
      instance.SetNewArray(&newArray_myTrack);
      instance.SetDelete(&delete_myTrack);
      instance.SetDeleteArray(&deleteArray_myTrack);
      instance.SetDestructor(&destruct_myTrack);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::myTrack*)
   {
      return GenerateInitInstanceLocal((::myTrack*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::myTrack*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_myZee(void *p = 0);
   static void *newArray_myZee(Long_t size, void *p);
   static void delete_myZee(void *p);
   static void deleteArray_myZee(void *p);
   static void destruct_myZee(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::myZee*)
   {
      ::myZee *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::myZee >(0);
      static ::ROOT::TGenericClassInfo 
         instance("myZee", ::myZee::Class_Version(), "../MyAnalysis/PhysicsObjectProxyBase.h", 447,
                  typeid(::myZee), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::myZee::Dictionary, isa_proxy, 4,
                  sizeof(::myZee) );
      instance.SetNew(&new_myZee);
      instance.SetNewArray(&newArray_myZee);
      instance.SetDelete(&delete_myZee);
      instance.SetDeleteArray(&deleteArray_myZee);
      instance.SetDestructor(&destruct_myZee);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::myZee*)
   {
      return GenerateInitInstanceLocal((::myZee*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::myZee*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_myZmumu(void *p = 0);
   static void *newArray_myZmumu(Long_t size, void *p);
   static void delete_myZmumu(void *p);
   static void deleteArray_myZmumu(void *p);
   static void destruct_myZmumu(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::myZmumu*)
   {
      ::myZmumu *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::myZmumu >(0);
      static ::ROOT::TGenericClassInfo 
         instance("myZmumu", ::myZmumu::Class_Version(), "../MyAnalysis/PhysicsObjectProxyBase.h", 465,
                  typeid(::myZmumu), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::myZmumu::Dictionary, isa_proxy, 4,
                  sizeof(::myZmumu) );
      instance.SetNew(&new_myZmumu);
      instance.SetNewArray(&newArray_myZmumu);
      instance.SetDelete(&delete_myZmumu);
      instance.SetDeleteArray(&deleteArray_myZmumu);
      instance.SetDestructor(&destruct_myZmumu);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::myZmumu*)
   {
      return GenerateInitInstanceLocal((::myZmumu*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::myZmumu*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr PhysObjBase::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *PhysObjBase::Class_Name()
{
   return "PhysObjBase";
}

//______________________________________________________________________________
const char *PhysObjBase::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::PhysObjBase*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int PhysObjBase::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::PhysObjBase*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *PhysObjBase::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::PhysObjBase*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *PhysObjBase::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::PhysObjBase*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr myJet::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *myJet::Class_Name()
{
   return "myJet";
}

//______________________________________________________________________________
const char *myJet::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::myJet*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int myJet::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::myJet*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *myJet::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::myJet*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *myJet::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::myJet*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr myElectron::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *myElectron::Class_Name()
{
   return "myElectron";
}

//______________________________________________________________________________
const char *myElectron::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::myElectron*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int myElectron::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::myElectron*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *myElectron::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::myElectron*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *myElectron::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::myElectron*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr myMuon::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *myMuon::Class_Name()
{
   return "myMuon";
}

//______________________________________________________________________________
const char *myMuon::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::myMuon*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int myMuon::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::myMuon*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *myMuon::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::myMuon*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *myMuon::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::myMuon*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr myTau::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *myTau::Class_Name()
{
   return "myTau";
}

//______________________________________________________________________________
const char *myTau::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::myTau*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int myTau::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::myTau*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *myTau::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::myTau*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *myTau::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::myTau*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr myCaloCluster::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *myCaloCluster::Class_Name()
{
   return "myCaloCluster";
}

//______________________________________________________________________________
const char *myCaloCluster::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::myCaloCluster*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int myCaloCluster::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::myCaloCluster*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *myCaloCluster::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::myCaloCluster*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *myCaloCluster::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::myCaloCluster*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr myMET::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *myMET::Class_Name()
{
   return "myMET";
}

//______________________________________________________________________________
const char *myMET::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::myMET*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int myMET::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::myMET*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *myMET::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::myMET*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *myMET::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::myMET*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr myTruth::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *myTruth::Class_Name()
{
   return "myTruth";
}

//______________________________________________________________________________
const char *myTruth::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::myTruth*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int myTruth::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::myTruth*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *myTruth::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::myTruth*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *myTruth::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::myTruth*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr myTrack::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *myTrack::Class_Name()
{
   return "myTrack";
}

//______________________________________________________________________________
const char *myTrack::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::myTrack*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int myTrack::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::myTrack*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *myTrack::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::myTrack*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *myTrack::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::myTrack*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr myZee::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *myZee::Class_Name()
{
   return "myZee";
}

//______________________________________________________________________________
const char *myZee::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::myZee*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int myZee::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::myZee*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *myZee::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::myZee*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *myZee::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::myZee*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr myZmumu::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *myZmumu::Class_Name()
{
   return "myZmumu";
}

//______________________________________________________________________________
const char *myZmumu::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::myZmumu*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int myZmumu::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::myZmumu*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *myZmumu::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::myZmumu*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *myZmumu::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::myZmumu*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void PhysObjBase::Streamer(TBuffer &R__b)
{
   // Stream an object of class PhysObjBase.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(PhysObjBase::Class(),this);
   } else {
      R__b.WriteClassBuffer(PhysObjBase::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_PhysObjBase(void *p) {
      return  p ? new(p) ::PhysObjBase : new ::PhysObjBase;
   }
   static void *newArray_PhysObjBase(Long_t nElements, void *p) {
      return p ? new(p) ::PhysObjBase[nElements] : new ::PhysObjBase[nElements];
   }
   // Wrapper around operator delete
   static void delete_PhysObjBase(void *p) {
      delete ((::PhysObjBase*)p);
   }
   static void deleteArray_PhysObjBase(void *p) {
      delete [] ((::PhysObjBase*)p);
   }
   static void destruct_PhysObjBase(void *p) {
      typedef ::PhysObjBase current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::PhysObjBase

//______________________________________________________________________________
void myJet::Streamer(TBuffer &R__b)
{
   // Stream an object of class myJet.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(myJet::Class(),this);
   } else {
      R__b.WriteClassBuffer(myJet::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_myJet(void *p) {
      return  p ? new(p) ::myJet : new ::myJet;
   }
   static void *newArray_myJet(Long_t nElements, void *p) {
      return p ? new(p) ::myJet[nElements] : new ::myJet[nElements];
   }
   // Wrapper around operator delete
   static void delete_myJet(void *p) {
      delete ((::myJet*)p);
   }
   static void deleteArray_myJet(void *p) {
      delete [] ((::myJet*)p);
   }
   static void destruct_myJet(void *p) {
      typedef ::myJet current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::myJet

//______________________________________________________________________________
void myElectron::Streamer(TBuffer &R__b)
{
   // Stream an object of class myElectron.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(myElectron::Class(),this);
   } else {
      R__b.WriteClassBuffer(myElectron::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_myElectron(void *p) {
      return  p ? new(p) ::myElectron : new ::myElectron;
   }
   static void *newArray_myElectron(Long_t nElements, void *p) {
      return p ? new(p) ::myElectron[nElements] : new ::myElectron[nElements];
   }
   // Wrapper around operator delete
   static void delete_myElectron(void *p) {
      delete ((::myElectron*)p);
   }
   static void deleteArray_myElectron(void *p) {
      delete [] ((::myElectron*)p);
   }
   static void destruct_myElectron(void *p) {
      typedef ::myElectron current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::myElectron

//______________________________________________________________________________
void myMuon::Streamer(TBuffer &R__b)
{
   // Stream an object of class myMuon.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(myMuon::Class(),this);
   } else {
      R__b.WriteClassBuffer(myMuon::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_myMuon(void *p) {
      return  p ? new(p) ::myMuon : new ::myMuon;
   }
   static void *newArray_myMuon(Long_t nElements, void *p) {
      return p ? new(p) ::myMuon[nElements] : new ::myMuon[nElements];
   }
   // Wrapper around operator delete
   static void delete_myMuon(void *p) {
      delete ((::myMuon*)p);
   }
   static void deleteArray_myMuon(void *p) {
      delete [] ((::myMuon*)p);
   }
   static void destruct_myMuon(void *p) {
      typedef ::myMuon current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::myMuon

//______________________________________________________________________________
void myTau::Streamer(TBuffer &R__b)
{
   // Stream an object of class myTau.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(myTau::Class(),this);
   } else {
      R__b.WriteClassBuffer(myTau::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_myTau(void *p) {
      return  p ? new(p) ::myTau : new ::myTau;
   }
   static void *newArray_myTau(Long_t nElements, void *p) {
      return p ? new(p) ::myTau[nElements] : new ::myTau[nElements];
   }
   // Wrapper around operator delete
   static void delete_myTau(void *p) {
      delete ((::myTau*)p);
   }
   static void deleteArray_myTau(void *p) {
      delete [] ((::myTau*)p);
   }
   static void destruct_myTau(void *p) {
      typedef ::myTau current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::myTau

//______________________________________________________________________________
void myCaloCluster::Streamer(TBuffer &R__b)
{
   // Stream an object of class myCaloCluster.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(myCaloCluster::Class(),this);
   } else {
      R__b.WriteClassBuffer(myCaloCluster::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_myCaloCluster(void *p) {
      return  p ? new(p) ::myCaloCluster : new ::myCaloCluster;
   }
   static void *newArray_myCaloCluster(Long_t nElements, void *p) {
      return p ? new(p) ::myCaloCluster[nElements] : new ::myCaloCluster[nElements];
   }
   // Wrapper around operator delete
   static void delete_myCaloCluster(void *p) {
      delete ((::myCaloCluster*)p);
   }
   static void deleteArray_myCaloCluster(void *p) {
      delete [] ((::myCaloCluster*)p);
   }
   static void destruct_myCaloCluster(void *p) {
      typedef ::myCaloCluster current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::myCaloCluster

//______________________________________________________________________________
void myMET::Streamer(TBuffer &R__b)
{
   // Stream an object of class myMET.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(myMET::Class(),this);
   } else {
      R__b.WriteClassBuffer(myMET::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_myMET(void *p) {
      return  p ? new(p) ::myMET : new ::myMET;
   }
   static void *newArray_myMET(Long_t nElements, void *p) {
      return p ? new(p) ::myMET[nElements] : new ::myMET[nElements];
   }
   // Wrapper around operator delete
   static void delete_myMET(void *p) {
      delete ((::myMET*)p);
   }
   static void deleteArray_myMET(void *p) {
      delete [] ((::myMET*)p);
   }
   static void destruct_myMET(void *p) {
      typedef ::myMET current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::myMET

//______________________________________________________________________________
void myTruth::Streamer(TBuffer &R__b)
{
   // Stream an object of class myTruth.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(myTruth::Class(),this);
   } else {
      R__b.WriteClassBuffer(myTruth::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_myTruth(void *p) {
      return  p ? new(p) ::myTruth : new ::myTruth;
   }
   static void *newArray_myTruth(Long_t nElements, void *p) {
      return p ? new(p) ::myTruth[nElements] : new ::myTruth[nElements];
   }
   // Wrapper around operator delete
   static void delete_myTruth(void *p) {
      delete ((::myTruth*)p);
   }
   static void deleteArray_myTruth(void *p) {
      delete [] ((::myTruth*)p);
   }
   static void destruct_myTruth(void *p) {
      typedef ::myTruth current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::myTruth

//______________________________________________________________________________
void myTrack::Streamer(TBuffer &R__b)
{
   // Stream an object of class myTrack.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(myTrack::Class(),this);
   } else {
      R__b.WriteClassBuffer(myTrack::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_myTrack(void *p) {
      return  p ? new(p) ::myTrack : new ::myTrack;
   }
   static void *newArray_myTrack(Long_t nElements, void *p) {
      return p ? new(p) ::myTrack[nElements] : new ::myTrack[nElements];
   }
   // Wrapper around operator delete
   static void delete_myTrack(void *p) {
      delete ((::myTrack*)p);
   }
   static void deleteArray_myTrack(void *p) {
      delete [] ((::myTrack*)p);
   }
   static void destruct_myTrack(void *p) {
      typedef ::myTrack current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::myTrack

//______________________________________________________________________________
void myZee::Streamer(TBuffer &R__b)
{
   // Stream an object of class myZee.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(myZee::Class(),this);
   } else {
      R__b.WriteClassBuffer(myZee::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_myZee(void *p) {
      return  p ? new(p) ::myZee : new ::myZee;
   }
   static void *newArray_myZee(Long_t nElements, void *p) {
      return p ? new(p) ::myZee[nElements] : new ::myZee[nElements];
   }
   // Wrapper around operator delete
   static void delete_myZee(void *p) {
      delete ((::myZee*)p);
   }
   static void deleteArray_myZee(void *p) {
      delete [] ((::myZee*)p);
   }
   static void destruct_myZee(void *p) {
      typedef ::myZee current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::myZee

//______________________________________________________________________________
void myZmumu::Streamer(TBuffer &R__b)
{
   // Stream an object of class myZmumu.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(myZmumu::Class(),this);
   } else {
      R__b.WriteClassBuffer(myZmumu::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_myZmumu(void *p) {
      return  p ? new(p) ::myZmumu : new ::myZmumu;
   }
   static void *newArray_myZmumu(Long_t nElements, void *p) {
      return p ? new(p) ::myZmumu[nElements] : new ::myZmumu[nElements];
   }
   // Wrapper around operator delete
   static void delete_myZmumu(void *p) {
      delete ((::myZmumu*)p);
   }
   static void deleteArray_myZmumu(void *p) {
      delete [] ((::myZmumu*)p);
   }
   static void destruct_myZmumu(void *p) {
      typedef ::myZmumu current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::myZmumu

namespace ROOT {
   static TClass *vectorlEvoidmUgR_Dictionary();
   static void vectorlEvoidmUgR_TClassManip(TClass*);
   static void *new_vectorlEvoidmUgR(void *p = 0);
   static void *newArray_vectorlEvoidmUgR(Long_t size, void *p);
   static void delete_vectorlEvoidmUgR(void *p);
   static void deleteArray_vectorlEvoidmUgR(void *p);
   static void destruct_vectorlEvoidmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<void*>*)
   {
      vector<void*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<void*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<void*>", -2, "vector", 210,
                  typeid(vector<void*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEvoidmUgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<void*>) );
      instance.SetNew(&new_vectorlEvoidmUgR);
      instance.SetNewArray(&newArray_vectorlEvoidmUgR);
      instance.SetDelete(&delete_vectorlEvoidmUgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvoidmUgR);
      instance.SetDestructor(&destruct_vectorlEvoidmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<void*> >()));

      ::ROOT::AddClassAlternate("vector<void*>","std::vector<void*, std::allocator<void*> >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<void*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvoidmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<void*>*)0x0)->GetClass();
      vectorlEvoidmUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvoidmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvoidmUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<void*> : new vector<void*>;
   }
   static void *newArray_vectorlEvoidmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<void*>[nElements] : new vector<void*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvoidmUgR(void *p) {
      delete ((vector<void*>*)p);
   }
   static void deleteArray_vectorlEvoidmUgR(void *p) {
      delete [] ((vector<void*>*)p);
   }
   static void destruct_vectorlEvoidmUgR(void *p) {
      typedef vector<void*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<void*>

namespace ROOT {
   static TClass *vectorlEvectorlEvoidmUgRsPgR_Dictionary();
   static void vectorlEvectorlEvoidmUgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEvoidmUgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEvoidmUgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEvoidmUgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEvoidmUgRsPgR(void *p);
   static void destruct_vectorlEvectorlEvoidmUgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<void*> >*)
   {
      vector<vector<void*> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<void*> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<void*> >", -2, "vector", 210,
                  typeid(vector<vector<void*> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEvoidmUgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<void*> >) );
      instance.SetNew(&new_vectorlEvectorlEvoidmUgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEvoidmUgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEvoidmUgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEvoidmUgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEvoidmUgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<void*> > >()));

      ::ROOT::AddClassAlternate("vector<vector<void*> >","std::vector<std::vector<void*, std::allocator<void*> >, std::allocator<std::vector<void*, std::allocator<void*> > > >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<vector<void*> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEvoidmUgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<void*> >*)0x0)->GetClass();
      vectorlEvectorlEvoidmUgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEvoidmUgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEvoidmUgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<void*> > : new vector<vector<void*> >;
   }
   static void *newArray_vectorlEvectorlEvoidmUgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<void*> >[nElements] : new vector<vector<void*> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEvoidmUgRsPgR(void *p) {
      delete ((vector<vector<void*> >*)p);
   }
   static void deleteArray_vectorlEvectorlEvoidmUgRsPgR(void *p) {
      delete [] ((vector<vector<void*> >*)p);
   }
   static void destruct_vectorlEvectorlEvoidmUgRsPgR(void *p) {
      typedef vector<vector<void*> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<void*> >

namespace ROOT {
   static TClass *vectorlEvectorlEunsignedsPshortgRsPgR_Dictionary();
   static void vectorlEvectorlEunsignedsPshortgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEunsignedsPshortgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEunsignedsPshortgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEunsignedsPshortgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEunsignedsPshortgRsPgR(void *p);
   static void destruct_vectorlEvectorlEunsignedsPshortgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<unsigned short> >*)
   {
      vector<vector<unsigned short> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<unsigned short> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<unsigned short> >", -2, "vector", 210,
                  typeid(vector<vector<unsigned short> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEunsignedsPshortgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<unsigned short> >) );
      instance.SetNew(&new_vectorlEvectorlEunsignedsPshortgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEunsignedsPshortgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEunsignedsPshortgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEunsignedsPshortgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEunsignedsPshortgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<unsigned short> > >()));

      ::ROOT::AddClassAlternate("vector<vector<unsigned short> >","std::vector<std::vector<unsigned short, std::allocator<unsigned short> >, std::allocator<std::vector<unsigned short, std::allocator<unsigned short> > > >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<vector<unsigned short> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEunsignedsPshortgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<unsigned short> >*)0x0)->GetClass();
      vectorlEvectorlEunsignedsPshortgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEunsignedsPshortgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEunsignedsPshortgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<unsigned short> > : new vector<vector<unsigned short> >;
   }
   static void *newArray_vectorlEvectorlEunsignedsPshortgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<unsigned short> >[nElements] : new vector<vector<unsigned short> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEunsignedsPshortgRsPgR(void *p) {
      delete ((vector<vector<unsigned short> >*)p);
   }
   static void deleteArray_vectorlEvectorlEunsignedsPshortgRsPgR(void *p) {
      delete [] ((vector<vector<unsigned short> >*)p);
   }
   static void destruct_vectorlEvectorlEunsignedsPshortgRsPgR(void *p) {
      typedef vector<vector<unsigned short> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<unsigned short> >

namespace ROOT {
   static TClass *vectorlEvectorlEunsignedsPlonggRsPgR_Dictionary();
   static void vectorlEvectorlEunsignedsPlonggRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEunsignedsPlonggRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEunsignedsPlonggRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEunsignedsPlonggRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEunsignedsPlonggRsPgR(void *p);
   static void destruct_vectorlEvectorlEunsignedsPlonggRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<unsigned long> >*)
   {
      vector<vector<unsigned long> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<unsigned long> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<unsigned long> >", -2, "vector", 210,
                  typeid(vector<vector<unsigned long> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEunsignedsPlonggRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<unsigned long> >) );
      instance.SetNew(&new_vectorlEvectorlEunsignedsPlonggRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEunsignedsPlonggRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEunsignedsPlonggRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEunsignedsPlonggRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEunsignedsPlonggRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<unsigned long> > >()));

      ::ROOT::AddClassAlternate("vector<vector<unsigned long> >","std::vector<std::vector<unsigned long, std::allocator<unsigned long> >, std::allocator<std::vector<unsigned long, std::allocator<unsigned long> > > >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<vector<unsigned long> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEunsignedsPlonggRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<unsigned long> >*)0x0)->GetClass();
      vectorlEvectorlEunsignedsPlonggRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEunsignedsPlonggRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEunsignedsPlonggRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<unsigned long> > : new vector<vector<unsigned long> >;
   }
   static void *newArray_vectorlEvectorlEunsignedsPlonggRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<unsigned long> >[nElements] : new vector<vector<unsigned long> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEunsignedsPlonggRsPgR(void *p) {
      delete ((vector<vector<unsigned long> >*)p);
   }
   static void deleteArray_vectorlEvectorlEunsignedsPlonggRsPgR(void *p) {
      delete [] ((vector<vector<unsigned long> >*)p);
   }
   static void destruct_vectorlEvectorlEunsignedsPlonggRsPgR(void *p) {
      typedef vector<vector<unsigned long> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<unsigned long> >

namespace ROOT {
   static TClass *vectorlEvectorlEunsignedsPintgRsPgR_Dictionary();
   static void vectorlEvectorlEunsignedsPintgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEunsignedsPintgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEunsignedsPintgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEunsignedsPintgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEunsignedsPintgRsPgR(void *p);
   static void destruct_vectorlEvectorlEunsignedsPintgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<unsigned int> >*)
   {
      vector<vector<unsigned int> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<unsigned int> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<unsigned int> >", -2, "vector", 210,
                  typeid(vector<vector<unsigned int> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEunsignedsPintgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<unsigned int> >) );
      instance.SetNew(&new_vectorlEvectorlEunsignedsPintgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEunsignedsPintgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEunsignedsPintgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEunsignedsPintgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEunsignedsPintgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<unsigned int> > >()));

      ::ROOT::AddClassAlternate("vector<vector<unsigned int> >","std::vector<std::vector<unsigned int, std::allocator<unsigned int> >, std::allocator<std::vector<unsigned int, std::allocator<unsigned int> > > >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<vector<unsigned int> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEunsignedsPintgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<unsigned int> >*)0x0)->GetClass();
      vectorlEvectorlEunsignedsPintgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEunsignedsPintgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEunsignedsPintgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<unsigned int> > : new vector<vector<unsigned int> >;
   }
   static void *newArray_vectorlEvectorlEunsignedsPintgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<unsigned int> >[nElements] : new vector<vector<unsigned int> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEunsignedsPintgRsPgR(void *p) {
      delete ((vector<vector<unsigned int> >*)p);
   }
   static void deleteArray_vectorlEvectorlEunsignedsPintgRsPgR(void *p) {
      delete [] ((vector<vector<unsigned int> >*)p);
   }
   static void destruct_vectorlEvectorlEunsignedsPintgRsPgR(void *p) {
      typedef vector<vector<unsigned int> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<unsigned int> >

namespace ROOT {
   static TClass *vectorlEvectorlEunsignedsPchargRsPgR_Dictionary();
   static void vectorlEvectorlEunsignedsPchargRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEunsignedsPchargRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEunsignedsPchargRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEunsignedsPchargRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEunsignedsPchargRsPgR(void *p);
   static void destruct_vectorlEvectorlEunsignedsPchargRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<unsigned char> >*)
   {
      vector<vector<unsigned char> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<unsigned char> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<unsigned char> >", -2, "vector", 210,
                  typeid(vector<vector<unsigned char> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEunsignedsPchargRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<unsigned char> >) );
      instance.SetNew(&new_vectorlEvectorlEunsignedsPchargRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEunsignedsPchargRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEunsignedsPchargRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEunsignedsPchargRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEunsignedsPchargRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<unsigned char> > >()));

      ::ROOT::AddClassAlternate("vector<vector<unsigned char> >","std::vector<std::vector<unsigned char, std::allocator<unsigned char> >, std::allocator<std::vector<unsigned char, std::allocator<unsigned char> > > >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<vector<unsigned char> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEunsignedsPchargRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<unsigned char> >*)0x0)->GetClass();
      vectorlEvectorlEunsignedsPchargRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEunsignedsPchargRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEunsignedsPchargRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<unsigned char> > : new vector<vector<unsigned char> >;
   }
   static void *newArray_vectorlEvectorlEunsignedsPchargRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<unsigned char> >[nElements] : new vector<vector<unsigned char> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEunsignedsPchargRsPgR(void *p) {
      delete ((vector<vector<unsigned char> >*)p);
   }
   static void deleteArray_vectorlEvectorlEunsignedsPchargRsPgR(void *p) {
      delete [] ((vector<vector<unsigned char> >*)p);
   }
   static void destruct_vectorlEvectorlEunsignedsPchargRsPgR(void *p) {
      typedef vector<vector<unsigned char> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<unsigned char> >

namespace ROOT {
   static TClass *vectorlEvectorlEshortgRsPgR_Dictionary();
   static void vectorlEvectorlEshortgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEshortgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEshortgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEshortgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEshortgRsPgR(void *p);
   static void destruct_vectorlEvectorlEshortgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<short> >*)
   {
      vector<vector<short> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<short> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<short> >", -2, "vector", 210,
                  typeid(vector<vector<short> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEshortgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<short> >) );
      instance.SetNew(&new_vectorlEvectorlEshortgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEshortgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEshortgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEshortgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEshortgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<short> > >()));

      ::ROOT::AddClassAlternate("vector<vector<short> >","std::vector<std::vector<short, std::allocator<short> >, std::allocator<std::vector<short, std::allocator<short> > > >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<vector<short> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEshortgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<short> >*)0x0)->GetClass();
      vectorlEvectorlEshortgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEshortgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEshortgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<short> > : new vector<vector<short> >;
   }
   static void *newArray_vectorlEvectorlEshortgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<short> >[nElements] : new vector<vector<short> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEshortgRsPgR(void *p) {
      delete ((vector<vector<short> >*)p);
   }
   static void deleteArray_vectorlEvectorlEshortgRsPgR(void *p) {
      delete [] ((vector<vector<short> >*)p);
   }
   static void destruct_vectorlEvectorlEshortgRsPgR(void *p) {
      typedef vector<vector<short> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<short> >

namespace ROOT {
   static TClass *vectorlEvectorlElonggRsPgR_Dictionary();
   static void vectorlEvectorlElonggRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlElonggRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlElonggRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlElonggRsPgR(void *p);
   static void deleteArray_vectorlEvectorlElonggRsPgR(void *p);
   static void destruct_vectorlEvectorlElonggRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<long> >*)
   {
      vector<vector<long> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<long> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<long> >", -2, "vector", 210,
                  typeid(vector<vector<long> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEvectorlElonggRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<long> >) );
      instance.SetNew(&new_vectorlEvectorlElonggRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlElonggRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlElonggRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlElonggRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlElonggRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<long> > >()));

      ::ROOT::AddClassAlternate("vector<vector<long> >","std::vector<std::vector<long, std::allocator<long> >, std::allocator<std::vector<long, std::allocator<long> > > >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<vector<long> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlElonggRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<long> >*)0x0)->GetClass();
      vectorlEvectorlElonggRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlElonggRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlElonggRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<long> > : new vector<vector<long> >;
   }
   static void *newArray_vectorlEvectorlElonggRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<long> >[nElements] : new vector<vector<long> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlElonggRsPgR(void *p) {
      delete ((vector<vector<long> >*)p);
   }
   static void deleteArray_vectorlEvectorlElonggRsPgR(void *p) {
      delete [] ((vector<vector<long> >*)p);
   }
   static void destruct_vectorlEvectorlElonggRsPgR(void *p) {
      typedef vector<vector<long> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<long> >

namespace ROOT {
   static TClass *vectorlEvectorlEintgRsPgR_Dictionary();
   static void vectorlEvectorlEintgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEintgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEintgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEintgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEintgRsPgR(void *p);
   static void destruct_vectorlEvectorlEintgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<int> >*)
   {
      vector<vector<int> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<int> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<int> >", -2, "vector", 210,
                  typeid(vector<vector<int> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEintgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<int> >) );
      instance.SetNew(&new_vectorlEvectorlEintgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEintgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEintgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEintgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEintgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<int> > >()));

      ::ROOT::AddClassAlternate("vector<vector<int> >","std::vector<std::vector<int, std::allocator<int> >, std::allocator<std::vector<int, std::allocator<int> > > >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<vector<int> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEintgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<int> >*)0x0)->GetClass();
      vectorlEvectorlEintgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEintgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEintgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<int> > : new vector<vector<int> >;
   }
   static void *newArray_vectorlEvectorlEintgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<int> >[nElements] : new vector<vector<int> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEintgRsPgR(void *p) {
      delete ((vector<vector<int> >*)p);
   }
   static void deleteArray_vectorlEvectorlEintgRsPgR(void *p) {
      delete [] ((vector<vector<int> >*)p);
   }
   static void destruct_vectorlEvectorlEintgRsPgR(void *p) {
      typedef vector<vector<int> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<int> >

namespace ROOT {
   static TClass *vectorlEvectorlEfloatgRsPgR_Dictionary();
   static void vectorlEvectorlEfloatgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEfloatgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEfloatgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEfloatgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEfloatgRsPgR(void *p);
   static void destruct_vectorlEvectorlEfloatgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<float> >*)
   {
      vector<vector<float> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<float> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<float> >", -2, "vector", 210,
                  typeid(vector<vector<float> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEfloatgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<float> >) );
      instance.SetNew(&new_vectorlEvectorlEfloatgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEfloatgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEfloatgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEfloatgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEfloatgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<float> > >()));

      ::ROOT::AddClassAlternate("vector<vector<float> >","std::vector<std::vector<float, std::allocator<float> >, std::allocator<std::vector<float, std::allocator<float> > > >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<vector<float> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEfloatgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<float> >*)0x0)->GetClass();
      vectorlEvectorlEfloatgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEfloatgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEfloatgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<float> > : new vector<vector<float> >;
   }
   static void *newArray_vectorlEvectorlEfloatgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<float> >[nElements] : new vector<vector<float> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEfloatgRsPgR(void *p) {
      delete ((vector<vector<float> >*)p);
   }
   static void deleteArray_vectorlEvectorlEfloatgRsPgR(void *p) {
      delete [] ((vector<vector<float> >*)p);
   }
   static void destruct_vectorlEvectorlEfloatgRsPgR(void *p) {
      typedef vector<vector<float> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<float> >

namespace ROOT {
   static TClass *vectorlEvectorlEdoublegRsPgR_Dictionary();
   static void vectorlEvectorlEdoublegRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEdoublegRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEdoublegRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEdoublegRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEdoublegRsPgR(void *p);
   static void destruct_vectorlEvectorlEdoublegRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<double> >*)
   {
      vector<vector<double> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<double> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<double> >", -2, "vector", 210,
                  typeid(vector<vector<double> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEdoublegRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<double> >) );
      instance.SetNew(&new_vectorlEvectorlEdoublegRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEdoublegRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEdoublegRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEdoublegRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEdoublegRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<double> > >()));

      ::ROOT::AddClassAlternate("vector<vector<double> >","std::vector<std::vector<double, std::allocator<double> >, std::allocator<std::vector<double, std::allocator<double> > > >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<vector<double> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEdoublegRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<double> >*)0x0)->GetClass();
      vectorlEvectorlEdoublegRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEdoublegRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEdoublegRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<double> > : new vector<vector<double> >;
   }
   static void *newArray_vectorlEvectorlEdoublegRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<double> >[nElements] : new vector<vector<double> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEdoublegRsPgR(void *p) {
      delete ((vector<vector<double> >*)p);
   }
   static void deleteArray_vectorlEvectorlEdoublegRsPgR(void *p) {
      delete [] ((vector<vector<double> >*)p);
   }
   static void destruct_vectorlEvectorlEdoublegRsPgR(void *p) {
      typedef vector<vector<double> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<double> >

namespace ROOT {
   static TClass *vectorlEvectorlEconstsPcharmUgRsPgR_Dictionary();
   static void vectorlEvectorlEconstsPcharmUgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEconstsPcharmUgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEconstsPcharmUgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEconstsPcharmUgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEconstsPcharmUgRsPgR(void *p);
   static void destruct_vectorlEvectorlEconstsPcharmUgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<const char*> >*)
   {
      vector<vector<const char*> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<const char*> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<const char*> >", -2, "vector", 210,
                  typeid(vector<vector<const char*> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEconstsPcharmUgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<const char*> >) );
      instance.SetNew(&new_vectorlEvectorlEconstsPcharmUgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEconstsPcharmUgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEconstsPcharmUgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEconstsPcharmUgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEconstsPcharmUgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<const char*> > >()));

      ::ROOT::AddClassAlternate("vector<vector<const char*> >","std::vector<std::vector<char const*, std::allocator<char const*> >, std::allocator<std::vector<char const*, std::allocator<char const*> > > >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<vector<const char*> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEconstsPcharmUgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<const char*> >*)0x0)->GetClass();
      vectorlEvectorlEconstsPcharmUgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEconstsPcharmUgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEconstsPcharmUgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<const char*> > : new vector<vector<const char*> >;
   }
   static void *newArray_vectorlEvectorlEconstsPcharmUgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<const char*> >[nElements] : new vector<vector<const char*> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEconstsPcharmUgRsPgR(void *p) {
      delete ((vector<vector<const char*> >*)p);
   }
   static void deleteArray_vectorlEvectorlEconstsPcharmUgRsPgR(void *p) {
      delete [] ((vector<vector<const char*> >*)p);
   }
   static void destruct_vectorlEvectorlEconstsPcharmUgRsPgR(void *p) {
      typedef vector<vector<const char*> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<const char*> >

namespace ROOT {
   static TClass *vectorlEvectorlEchargRsPgR_Dictionary();
   static void vectorlEvectorlEchargRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEchargRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEchargRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEchargRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEchargRsPgR(void *p);
   static void destruct_vectorlEvectorlEchargRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<char> >*)
   {
      vector<vector<char> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<char> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<char> >", -2, "vector", 210,
                  typeid(vector<vector<char> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEchargRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<char> >) );
      instance.SetNew(&new_vectorlEvectorlEchargRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEchargRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEchargRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEchargRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEchargRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<char> > >()));

      ::ROOT::AddClassAlternate("vector<vector<char> >","std::vector<std::vector<char, std::allocator<char> >, std::allocator<std::vector<char, std::allocator<char> > > >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<vector<char> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEchargRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<char> >*)0x0)->GetClass();
      vectorlEvectorlEchargRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEchargRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEchargRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<char> > : new vector<vector<char> >;
   }
   static void *newArray_vectorlEvectorlEchargRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<char> >[nElements] : new vector<vector<char> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEchargRsPgR(void *p) {
      delete ((vector<vector<char> >*)p);
   }
   static void deleteArray_vectorlEvectorlEchargRsPgR(void *p) {
      delete [] ((vector<vector<char> >*)p);
   }
   static void destruct_vectorlEvectorlEchargRsPgR(void *p) {
      typedef vector<vector<char> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<char> >

namespace ROOT {
   static TClass *vectorlEvectorlEcharmUgRsPgR_Dictionary();
   static void vectorlEvectorlEcharmUgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEcharmUgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEcharmUgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEcharmUgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEcharmUgRsPgR(void *p);
   static void destruct_vectorlEvectorlEcharmUgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<char*> >*)
   {
      vector<vector<char*> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<char*> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<char*> >", -2, "vector", 210,
                  typeid(vector<vector<char*> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEcharmUgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<char*> >) );
      instance.SetNew(&new_vectorlEvectorlEcharmUgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEcharmUgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEcharmUgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEcharmUgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEcharmUgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<char*> > >()));

      ::ROOT::AddClassAlternate("vector<vector<char*> >","std::vector<std::vector<char*, std::allocator<char*> >, std::allocator<std::vector<char*, std::allocator<char*> > > >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<vector<char*> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEcharmUgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<char*> >*)0x0)->GetClass();
      vectorlEvectorlEcharmUgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEcharmUgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEcharmUgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<char*> > : new vector<vector<char*> >;
   }
   static void *newArray_vectorlEvectorlEcharmUgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<char*> >[nElements] : new vector<vector<char*> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEcharmUgRsPgR(void *p) {
      delete ((vector<vector<char*> >*)p);
   }
   static void deleteArray_vectorlEvectorlEcharmUgRsPgR(void *p) {
      delete [] ((vector<vector<char*> >*)p);
   }
   static void destruct_vectorlEvectorlEcharmUgRsPgR(void *p) {
      typedef vector<vector<char*> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<char*> >

namespace ROOT {
   static TClass *vectorlEvectorlEboolgRsPgR_Dictionary();
   static void vectorlEvectorlEboolgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEboolgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEboolgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEboolgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEboolgRsPgR(void *p);
   static void destruct_vectorlEvectorlEboolgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<bool> >*)
   {
      vector<vector<bool> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<bool> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<bool> >", -2, "vector", 210,
                  typeid(vector<vector<bool> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEboolgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<bool> >) );
      instance.SetNew(&new_vectorlEvectorlEboolgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEboolgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEboolgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEboolgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEboolgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<bool> > >()));

      ::ROOT::AddClassAlternate("vector<vector<bool> >","std::vector<std::vector<bool, std::allocator<bool> >, std::allocator<std::vector<bool, std::allocator<bool> > > >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<vector<bool> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEboolgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<bool> >*)0x0)->GetClass();
      vectorlEvectorlEboolgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEboolgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEboolgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<bool> > : new vector<vector<bool> >;
   }
   static void *newArray_vectorlEvectorlEboolgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<bool> >[nElements] : new vector<vector<bool> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEboolgRsPgR(void *p) {
      delete ((vector<vector<bool> >*)p);
   }
   static void deleteArray_vectorlEvectorlEboolgRsPgR(void *p) {
      delete [] ((vector<vector<bool> >*)p);
   }
   static void destruct_vectorlEvectorlEboolgRsPgR(void *p) {
      typedef vector<vector<bool> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<bool> >

namespace ROOT {
   static TClass *vectorlEvectorlEULong64_tgRsPgR_Dictionary();
   static void vectorlEvectorlEULong64_tgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEULong64_tgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEULong64_tgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEULong64_tgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEULong64_tgRsPgR(void *p);
   static void destruct_vectorlEvectorlEULong64_tgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ULong64_t> >*)
   {
      vector<vector<ULong64_t> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ULong64_t> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ULong64_t> >", -2, "vector", 210,
                  typeid(vector<vector<ULong64_t> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEULong64_tgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ULong64_t> >) );
      instance.SetNew(&new_vectorlEvectorlEULong64_tgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEULong64_tgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEULong64_tgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEULong64_tgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEULong64_tgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ULong64_t> > >()));

      ::ROOT::AddClassAlternate("vector<vector<ULong64_t> >","std::vector<std::vector<unsigned long long, std::allocator<unsigned long long> >, std::allocator<std::vector<unsigned long long, std::allocator<unsigned long long> > > >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<vector<ULong64_t> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEULong64_tgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ULong64_t> >*)0x0)->GetClass();
      vectorlEvectorlEULong64_tgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEULong64_tgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEULong64_tgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<ULong64_t> > : new vector<vector<ULong64_t> >;
   }
   static void *newArray_vectorlEvectorlEULong64_tgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<ULong64_t> >[nElements] : new vector<vector<ULong64_t> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEULong64_tgRsPgR(void *p) {
      delete ((vector<vector<ULong64_t> >*)p);
   }
   static void deleteArray_vectorlEvectorlEULong64_tgRsPgR(void *p) {
      delete [] ((vector<vector<ULong64_t> >*)p);
   }
   static void destruct_vectorlEvectorlEULong64_tgRsPgR(void *p) {
      typedef vector<vector<ULong64_t> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ULong64_t> >

namespace ROOT {
   static TClass *vectorlEvectorlELong64_tgRsPgR_Dictionary();
   static void vectorlEvectorlELong64_tgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlELong64_tgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlELong64_tgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlELong64_tgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlELong64_tgRsPgR(void *p);
   static void destruct_vectorlEvectorlELong64_tgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<Long64_t> >*)
   {
      vector<vector<Long64_t> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<Long64_t> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<Long64_t> >", -2, "vector", 210,
                  typeid(vector<vector<Long64_t> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEvectorlELong64_tgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<Long64_t> >) );
      instance.SetNew(&new_vectorlEvectorlELong64_tgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlELong64_tgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlELong64_tgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlELong64_tgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlELong64_tgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<Long64_t> > >()));

      ::ROOT::AddClassAlternate("vector<vector<Long64_t> >","std::vector<std::vector<long long, std::allocator<long long> >, std::allocator<std::vector<long long, std::allocator<long long> > > >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<vector<Long64_t> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlELong64_tgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<Long64_t> >*)0x0)->GetClass();
      vectorlEvectorlELong64_tgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlELong64_tgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlELong64_tgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<Long64_t> > : new vector<vector<Long64_t> >;
   }
   static void *newArray_vectorlEvectorlELong64_tgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<Long64_t> >[nElements] : new vector<vector<Long64_t> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlELong64_tgRsPgR(void *p) {
      delete ((vector<vector<Long64_t> >*)p);
   }
   static void deleteArray_vectorlEvectorlELong64_tgRsPgR(void *p) {
      delete [] ((vector<vector<Long64_t> >*)p);
   }
   static void destruct_vectorlEvectorlELong64_tgRsPgR(void *p) {
      typedef vector<vector<Long64_t> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<Long64_t> >

namespace ROOT {
   static TClass *vectorlEunsignedsPshortgR_Dictionary();
   static void vectorlEunsignedsPshortgR_TClassManip(TClass*);
   static void *new_vectorlEunsignedsPshortgR(void *p = 0);
   static void *newArray_vectorlEunsignedsPshortgR(Long_t size, void *p);
   static void delete_vectorlEunsignedsPshortgR(void *p);
   static void deleteArray_vectorlEunsignedsPshortgR(void *p);
   static void destruct_vectorlEunsignedsPshortgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<unsigned short>*)
   {
      vector<unsigned short> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<unsigned short>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<unsigned short>", -2, "vector", 210,
                  typeid(vector<unsigned short>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEunsignedsPshortgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<unsigned short>) );
      instance.SetNew(&new_vectorlEunsignedsPshortgR);
      instance.SetNewArray(&newArray_vectorlEunsignedsPshortgR);
      instance.SetDelete(&delete_vectorlEunsignedsPshortgR);
      instance.SetDeleteArray(&deleteArray_vectorlEunsignedsPshortgR);
      instance.SetDestructor(&destruct_vectorlEunsignedsPshortgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<unsigned short> >()));

      ::ROOT::AddClassAlternate("vector<unsigned short>","std::vector<unsigned short, std::allocator<unsigned short> >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<unsigned short>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEunsignedsPshortgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<unsigned short>*)0x0)->GetClass();
      vectorlEunsignedsPshortgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEunsignedsPshortgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEunsignedsPshortgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<unsigned short> : new vector<unsigned short>;
   }
   static void *newArray_vectorlEunsignedsPshortgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<unsigned short>[nElements] : new vector<unsigned short>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEunsignedsPshortgR(void *p) {
      delete ((vector<unsigned short>*)p);
   }
   static void deleteArray_vectorlEunsignedsPshortgR(void *p) {
      delete [] ((vector<unsigned short>*)p);
   }
   static void destruct_vectorlEunsignedsPshortgR(void *p) {
      typedef vector<unsigned short> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<unsigned short>

namespace ROOT {
   static TClass *vectorlEunsignedsPlonggR_Dictionary();
   static void vectorlEunsignedsPlonggR_TClassManip(TClass*);
   static void *new_vectorlEunsignedsPlonggR(void *p = 0);
   static void *newArray_vectorlEunsignedsPlonggR(Long_t size, void *p);
   static void delete_vectorlEunsignedsPlonggR(void *p);
   static void deleteArray_vectorlEunsignedsPlonggR(void *p);
   static void destruct_vectorlEunsignedsPlonggR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<unsigned long>*)
   {
      vector<unsigned long> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<unsigned long>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<unsigned long>", -2, "vector", 210,
                  typeid(vector<unsigned long>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEunsignedsPlonggR_Dictionary, isa_proxy, 4,
                  sizeof(vector<unsigned long>) );
      instance.SetNew(&new_vectorlEunsignedsPlonggR);
      instance.SetNewArray(&newArray_vectorlEunsignedsPlonggR);
      instance.SetDelete(&delete_vectorlEunsignedsPlonggR);
      instance.SetDeleteArray(&deleteArray_vectorlEunsignedsPlonggR);
      instance.SetDestructor(&destruct_vectorlEunsignedsPlonggR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<unsigned long> >()));

      ::ROOT::AddClassAlternate("vector<unsigned long>","std::vector<unsigned long, std::allocator<unsigned long> >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<unsigned long>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEunsignedsPlonggR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<unsigned long>*)0x0)->GetClass();
      vectorlEunsignedsPlonggR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEunsignedsPlonggR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEunsignedsPlonggR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<unsigned long> : new vector<unsigned long>;
   }
   static void *newArray_vectorlEunsignedsPlonggR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<unsigned long>[nElements] : new vector<unsigned long>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEunsignedsPlonggR(void *p) {
      delete ((vector<unsigned long>*)p);
   }
   static void deleteArray_vectorlEunsignedsPlonggR(void *p) {
      delete [] ((vector<unsigned long>*)p);
   }
   static void destruct_vectorlEunsignedsPlonggR(void *p) {
      typedef vector<unsigned long> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<unsigned long>

namespace ROOT {
   static TClass *vectorlEunsignedsPintgR_Dictionary();
   static void vectorlEunsignedsPintgR_TClassManip(TClass*);
   static void *new_vectorlEunsignedsPintgR(void *p = 0);
   static void *newArray_vectorlEunsignedsPintgR(Long_t size, void *p);
   static void delete_vectorlEunsignedsPintgR(void *p);
   static void deleteArray_vectorlEunsignedsPintgR(void *p);
   static void destruct_vectorlEunsignedsPintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<unsigned int>*)
   {
      vector<unsigned int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<unsigned int>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<unsigned int>", -2, "vector", 210,
                  typeid(vector<unsigned int>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEunsignedsPintgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<unsigned int>) );
      instance.SetNew(&new_vectorlEunsignedsPintgR);
      instance.SetNewArray(&newArray_vectorlEunsignedsPintgR);
      instance.SetDelete(&delete_vectorlEunsignedsPintgR);
      instance.SetDeleteArray(&deleteArray_vectorlEunsignedsPintgR);
      instance.SetDestructor(&destruct_vectorlEunsignedsPintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<unsigned int> >()));

      ::ROOT::AddClassAlternate("vector<unsigned int>","std::vector<unsigned int, std::allocator<unsigned int> >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<unsigned int>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEunsignedsPintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<unsigned int>*)0x0)->GetClass();
      vectorlEunsignedsPintgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEunsignedsPintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEunsignedsPintgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<unsigned int> : new vector<unsigned int>;
   }
   static void *newArray_vectorlEunsignedsPintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<unsigned int>[nElements] : new vector<unsigned int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEunsignedsPintgR(void *p) {
      delete ((vector<unsigned int>*)p);
   }
   static void deleteArray_vectorlEunsignedsPintgR(void *p) {
      delete [] ((vector<unsigned int>*)p);
   }
   static void destruct_vectorlEunsignedsPintgR(void *p) {
      typedef vector<unsigned int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<unsigned int>

namespace ROOT {
   static TClass *vectorlEunsignedsPchargR_Dictionary();
   static void vectorlEunsignedsPchargR_TClassManip(TClass*);
   static void *new_vectorlEunsignedsPchargR(void *p = 0);
   static void *newArray_vectorlEunsignedsPchargR(Long_t size, void *p);
   static void delete_vectorlEunsignedsPchargR(void *p);
   static void deleteArray_vectorlEunsignedsPchargR(void *p);
   static void destruct_vectorlEunsignedsPchargR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<unsigned char>*)
   {
      vector<unsigned char> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<unsigned char>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<unsigned char>", -2, "vector", 210,
                  typeid(vector<unsigned char>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEunsignedsPchargR_Dictionary, isa_proxy, 4,
                  sizeof(vector<unsigned char>) );
      instance.SetNew(&new_vectorlEunsignedsPchargR);
      instance.SetNewArray(&newArray_vectorlEunsignedsPchargR);
      instance.SetDelete(&delete_vectorlEunsignedsPchargR);
      instance.SetDeleteArray(&deleteArray_vectorlEunsignedsPchargR);
      instance.SetDestructor(&destruct_vectorlEunsignedsPchargR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<unsigned char> >()));

      ::ROOT::AddClassAlternate("vector<unsigned char>","std::vector<unsigned char, std::allocator<unsigned char> >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<unsigned char>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEunsignedsPchargR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<unsigned char>*)0x0)->GetClass();
      vectorlEunsignedsPchargR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEunsignedsPchargR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEunsignedsPchargR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<unsigned char> : new vector<unsigned char>;
   }
   static void *newArray_vectorlEunsignedsPchargR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<unsigned char>[nElements] : new vector<unsigned char>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEunsignedsPchargR(void *p) {
      delete ((vector<unsigned char>*)p);
   }
   static void deleteArray_vectorlEunsignedsPchargR(void *p) {
      delete [] ((vector<unsigned char>*)p);
   }
   static void destruct_vectorlEunsignedsPchargR(void *p) {
      typedef vector<unsigned char> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<unsigned char>

namespace ROOT {
   static TClass *vectorlEstringgR_Dictionary();
   static void vectorlEstringgR_TClassManip(TClass*);
   static void *new_vectorlEstringgR(void *p = 0);
   static void *newArray_vectorlEstringgR(Long_t size, void *p);
   static void delete_vectorlEstringgR(void *p);
   static void deleteArray_vectorlEstringgR(void *p);
   static void destruct_vectorlEstringgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<string>*)
   {
      vector<string> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<string>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<string>", -2, "vector", 210,
                  typeid(vector<string>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEstringgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<string>) );
      instance.SetNew(&new_vectorlEstringgR);
      instance.SetNewArray(&newArray_vectorlEstringgR);
      instance.SetDelete(&delete_vectorlEstringgR);
      instance.SetDeleteArray(&deleteArray_vectorlEstringgR);
      instance.SetDestructor(&destruct_vectorlEstringgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<string> >()));

      ::ROOT::AddClassAlternate("vector<string>","std::vector<std::string, std::allocator<std::string> >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<string>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEstringgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<string>*)0x0)->GetClass();
      vectorlEstringgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEstringgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEstringgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<string> : new vector<string>;
   }
   static void *newArray_vectorlEstringgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<string>[nElements] : new vector<string>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEstringgR(void *p) {
      delete ((vector<string>*)p);
   }
   static void deleteArray_vectorlEstringgR(void *p) {
      delete [] ((vector<string>*)p);
   }
   static void destruct_vectorlEstringgR(void *p) {
      typedef vector<string> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<string>

namespace ROOT {
   static TClass *vectorlEshortgR_Dictionary();
   static void vectorlEshortgR_TClassManip(TClass*);
   static void *new_vectorlEshortgR(void *p = 0);
   static void *newArray_vectorlEshortgR(Long_t size, void *p);
   static void delete_vectorlEshortgR(void *p);
   static void deleteArray_vectorlEshortgR(void *p);
   static void destruct_vectorlEshortgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<short>*)
   {
      vector<short> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<short>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<short>", -2, "vector", 210,
                  typeid(vector<short>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEshortgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<short>) );
      instance.SetNew(&new_vectorlEshortgR);
      instance.SetNewArray(&newArray_vectorlEshortgR);
      instance.SetDelete(&delete_vectorlEshortgR);
      instance.SetDeleteArray(&deleteArray_vectorlEshortgR);
      instance.SetDestructor(&destruct_vectorlEshortgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<short> >()));

      ::ROOT::AddClassAlternate("vector<short>","std::vector<short, std::allocator<short> >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<short>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEshortgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<short>*)0x0)->GetClass();
      vectorlEshortgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEshortgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEshortgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<short> : new vector<short>;
   }
   static void *newArray_vectorlEshortgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<short>[nElements] : new vector<short>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEshortgR(void *p) {
      delete ((vector<short>*)p);
   }
   static void deleteArray_vectorlEshortgR(void *p) {
      delete [] ((vector<short>*)p);
   }
   static void destruct_vectorlEshortgR(void *p) {
      typedef vector<short> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<short>

namespace ROOT {
   static TClass *vectorlEmyTruthgR_Dictionary();
   static void vectorlEmyTruthgR_TClassManip(TClass*);
   static void *new_vectorlEmyTruthgR(void *p = 0);
   static void *newArray_vectorlEmyTruthgR(Long_t size, void *p);
   static void delete_vectorlEmyTruthgR(void *p);
   static void deleteArray_vectorlEmyTruthgR(void *p);
   static void destruct_vectorlEmyTruthgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<myTruth>*)
   {
      vector<myTruth> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<myTruth>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<myTruth>", -2, "vector", 210,
                  typeid(vector<myTruth>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEmyTruthgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<myTruth>) );
      instance.SetNew(&new_vectorlEmyTruthgR);
      instance.SetNewArray(&newArray_vectorlEmyTruthgR);
      instance.SetDelete(&delete_vectorlEmyTruthgR);
      instance.SetDeleteArray(&deleteArray_vectorlEmyTruthgR);
      instance.SetDestructor(&destruct_vectorlEmyTruthgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<myTruth> >()));

      ::ROOT::AddClassAlternate("vector<myTruth>","std::vector<myTruth, std::allocator<myTruth> >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<myTruth>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEmyTruthgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<myTruth>*)0x0)->GetClass();
      vectorlEmyTruthgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEmyTruthgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEmyTruthgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<myTruth> : new vector<myTruth>;
   }
   static void *newArray_vectorlEmyTruthgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<myTruth>[nElements] : new vector<myTruth>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEmyTruthgR(void *p) {
      delete ((vector<myTruth>*)p);
   }
   static void deleteArray_vectorlEmyTruthgR(void *p) {
      delete [] ((vector<myTruth>*)p);
   }
   static void destruct_vectorlEmyTruthgR(void *p) {
      typedef vector<myTruth> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<myTruth>

namespace ROOT {
   static TClass *vectorlElonggR_Dictionary();
   static void vectorlElonggR_TClassManip(TClass*);
   static void *new_vectorlElonggR(void *p = 0);
   static void *newArray_vectorlElonggR(Long_t size, void *p);
   static void delete_vectorlElonggR(void *p);
   static void deleteArray_vectorlElonggR(void *p);
   static void destruct_vectorlElonggR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<long>*)
   {
      vector<long> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<long>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<long>", -2, "vector", 210,
                  typeid(vector<long>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlElonggR_Dictionary, isa_proxy, 4,
                  sizeof(vector<long>) );
      instance.SetNew(&new_vectorlElonggR);
      instance.SetNewArray(&newArray_vectorlElonggR);
      instance.SetDelete(&delete_vectorlElonggR);
      instance.SetDeleteArray(&deleteArray_vectorlElonggR);
      instance.SetDestructor(&destruct_vectorlElonggR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<long> >()));

      ::ROOT::AddClassAlternate("vector<long>","std::vector<long, std::allocator<long> >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<long>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlElonggR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<long>*)0x0)->GetClass();
      vectorlElonggR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlElonggR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlElonggR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<long> : new vector<long>;
   }
   static void *newArray_vectorlElonggR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<long>[nElements] : new vector<long>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlElonggR(void *p) {
      delete ((vector<long>*)p);
   }
   static void deleteArray_vectorlElonggR(void *p) {
      delete [] ((vector<long>*)p);
   }
   static void destruct_vectorlElonggR(void *p) {
      typedef vector<long> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<long>

namespace ROOT {
   static TClass *vectorlEintgR_Dictionary();
   static void vectorlEintgR_TClassManip(TClass*);
   static void *new_vectorlEintgR(void *p = 0);
   static void *newArray_vectorlEintgR(Long_t size, void *p);
   static void delete_vectorlEintgR(void *p);
   static void deleteArray_vectorlEintgR(void *p);
   static void destruct_vectorlEintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<int>*)
   {
      vector<int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<int>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<int>", -2, "vector", 210,
                  typeid(vector<int>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEintgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<int>) );
      instance.SetNew(&new_vectorlEintgR);
      instance.SetNewArray(&newArray_vectorlEintgR);
      instance.SetDelete(&delete_vectorlEintgR);
      instance.SetDeleteArray(&deleteArray_vectorlEintgR);
      instance.SetDestructor(&destruct_vectorlEintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<int> >()));

      ::ROOT::AddClassAlternate("vector<int>","std::vector<int, std::allocator<int> >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<int>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<int>*)0x0)->GetClass();
      vectorlEintgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEintgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<int> : new vector<int>;
   }
   static void *newArray_vectorlEintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<int>[nElements] : new vector<int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEintgR(void *p) {
      delete ((vector<int>*)p);
   }
   static void deleteArray_vectorlEintgR(void *p) {
      delete [] ((vector<int>*)p);
   }
   static void destruct_vectorlEintgR(void *p) {
      typedef vector<int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<int>

namespace ROOT {
   static TClass *vectorlEfloatgR_Dictionary();
   static void vectorlEfloatgR_TClassManip(TClass*);
   static void *new_vectorlEfloatgR(void *p = 0);
   static void *newArray_vectorlEfloatgR(Long_t size, void *p);
   static void delete_vectorlEfloatgR(void *p);
   static void deleteArray_vectorlEfloatgR(void *p);
   static void destruct_vectorlEfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<float>*)
   {
      vector<float> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<float>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<float>", -2, "vector", 210,
                  typeid(vector<float>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEfloatgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<float>) );
      instance.SetNew(&new_vectorlEfloatgR);
      instance.SetNewArray(&newArray_vectorlEfloatgR);
      instance.SetDelete(&delete_vectorlEfloatgR);
      instance.SetDeleteArray(&deleteArray_vectorlEfloatgR);
      instance.SetDestructor(&destruct_vectorlEfloatgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<float> >()));

      ::ROOT::AddClassAlternate("vector<float>","std::vector<float, std::allocator<float> >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<float>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<float>*)0x0)->GetClass();
      vectorlEfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEfloatgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<float> : new vector<float>;
   }
   static void *newArray_vectorlEfloatgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<float>[nElements] : new vector<float>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEfloatgR(void *p) {
      delete ((vector<float>*)p);
   }
   static void deleteArray_vectorlEfloatgR(void *p) {
      delete [] ((vector<float>*)p);
   }
   static void destruct_vectorlEfloatgR(void *p) {
      typedef vector<float> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<float>

namespace ROOT {
   static TClass *vectorlEdoublegR_Dictionary();
   static void vectorlEdoublegR_TClassManip(TClass*);
   static void *new_vectorlEdoublegR(void *p = 0);
   static void *newArray_vectorlEdoublegR(Long_t size, void *p);
   static void delete_vectorlEdoublegR(void *p);
   static void deleteArray_vectorlEdoublegR(void *p);
   static void destruct_vectorlEdoublegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<double>*)
   {
      vector<double> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<double>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<double>", -2, "vector", 210,
                  typeid(vector<double>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEdoublegR_Dictionary, isa_proxy, 4,
                  sizeof(vector<double>) );
      instance.SetNew(&new_vectorlEdoublegR);
      instance.SetNewArray(&newArray_vectorlEdoublegR);
      instance.SetDelete(&delete_vectorlEdoublegR);
      instance.SetDeleteArray(&deleteArray_vectorlEdoublegR);
      instance.SetDestructor(&destruct_vectorlEdoublegR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<double> >()));

      ::ROOT::AddClassAlternate("vector<double>","std::vector<double, std::allocator<double> >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<double>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEdoublegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<double>*)0x0)->GetClass();
      vectorlEdoublegR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEdoublegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEdoublegR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<double> : new vector<double>;
   }
   static void *newArray_vectorlEdoublegR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<double>[nElements] : new vector<double>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEdoublegR(void *p) {
      delete ((vector<double>*)p);
   }
   static void deleteArray_vectorlEdoublegR(void *p) {
      delete [] ((vector<double>*)p);
   }
   static void destruct_vectorlEdoublegR(void *p) {
      typedef vector<double> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<double>

namespace ROOT {
   static TClass *vectorlEconstsPcharmUgR_Dictionary();
   static void vectorlEconstsPcharmUgR_TClassManip(TClass*);
   static void *new_vectorlEconstsPcharmUgR(void *p = 0);
   static void *newArray_vectorlEconstsPcharmUgR(Long_t size, void *p);
   static void delete_vectorlEconstsPcharmUgR(void *p);
   static void deleteArray_vectorlEconstsPcharmUgR(void *p);
   static void destruct_vectorlEconstsPcharmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<const char*>*)
   {
      vector<const char*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<const char*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<const char*>", -2, "vector", 210,
                  typeid(vector<const char*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEconstsPcharmUgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<const char*>) );
      instance.SetNew(&new_vectorlEconstsPcharmUgR);
      instance.SetNewArray(&newArray_vectorlEconstsPcharmUgR);
      instance.SetDelete(&delete_vectorlEconstsPcharmUgR);
      instance.SetDeleteArray(&deleteArray_vectorlEconstsPcharmUgR);
      instance.SetDestructor(&destruct_vectorlEconstsPcharmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<const char*> >()));

      ::ROOT::AddClassAlternate("vector<const char*>","std::vector<char const*, std::allocator<char const*> >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<const char*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEconstsPcharmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<const char*>*)0x0)->GetClass();
      vectorlEconstsPcharmUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEconstsPcharmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEconstsPcharmUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<const char*> : new vector<const char*>;
   }
   static void *newArray_vectorlEconstsPcharmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<const char*>[nElements] : new vector<const char*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEconstsPcharmUgR(void *p) {
      delete ((vector<const char*>*)p);
   }
   static void deleteArray_vectorlEconstsPcharmUgR(void *p) {
      delete [] ((vector<const char*>*)p);
   }
   static void destruct_vectorlEconstsPcharmUgR(void *p) {
      typedef vector<const char*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<const char*>

namespace ROOT {
   static TClass *vectorlEchargR_Dictionary();
   static void vectorlEchargR_TClassManip(TClass*);
   static void *new_vectorlEchargR(void *p = 0);
   static void *newArray_vectorlEchargR(Long_t size, void *p);
   static void delete_vectorlEchargR(void *p);
   static void deleteArray_vectorlEchargR(void *p);
   static void destruct_vectorlEchargR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<char>*)
   {
      vector<char> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<char>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<char>", -2, "vector", 210,
                  typeid(vector<char>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEchargR_Dictionary, isa_proxy, 4,
                  sizeof(vector<char>) );
      instance.SetNew(&new_vectorlEchargR);
      instance.SetNewArray(&newArray_vectorlEchargR);
      instance.SetDelete(&delete_vectorlEchargR);
      instance.SetDeleteArray(&deleteArray_vectorlEchargR);
      instance.SetDestructor(&destruct_vectorlEchargR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<char> >()));

      ::ROOT::AddClassAlternate("vector<char>","std::vector<char, std::allocator<char> >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<char>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEchargR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<char>*)0x0)->GetClass();
      vectorlEchargR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEchargR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEchargR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<char> : new vector<char>;
   }
   static void *newArray_vectorlEchargR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<char>[nElements] : new vector<char>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEchargR(void *p) {
      delete ((vector<char>*)p);
   }
   static void deleteArray_vectorlEchargR(void *p) {
      delete [] ((vector<char>*)p);
   }
   static void destruct_vectorlEchargR(void *p) {
      typedef vector<char> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<char>

namespace ROOT {
   static TClass *vectorlEcharmUgR_Dictionary();
   static void vectorlEcharmUgR_TClassManip(TClass*);
   static void *new_vectorlEcharmUgR(void *p = 0);
   static void *newArray_vectorlEcharmUgR(Long_t size, void *p);
   static void delete_vectorlEcharmUgR(void *p);
   static void deleteArray_vectorlEcharmUgR(void *p);
   static void destruct_vectorlEcharmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<char*>*)
   {
      vector<char*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<char*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<char*>", -2, "vector", 210,
                  typeid(vector<char*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEcharmUgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<char*>) );
      instance.SetNew(&new_vectorlEcharmUgR);
      instance.SetNewArray(&newArray_vectorlEcharmUgR);
      instance.SetDelete(&delete_vectorlEcharmUgR);
      instance.SetDeleteArray(&deleteArray_vectorlEcharmUgR);
      instance.SetDestructor(&destruct_vectorlEcharmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<char*> >()));

      ::ROOT::AddClassAlternate("vector<char*>","std::vector<char*, std::allocator<char*> >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<char*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEcharmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<char*>*)0x0)->GetClass();
      vectorlEcharmUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEcharmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEcharmUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<char*> : new vector<char*>;
   }
   static void *newArray_vectorlEcharmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<char*>[nElements] : new vector<char*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEcharmUgR(void *p) {
      delete ((vector<char*>*)p);
   }
   static void deleteArray_vectorlEcharmUgR(void *p) {
      delete [] ((vector<char*>*)p);
   }
   static void destruct_vectorlEcharmUgR(void *p) {
      typedef vector<char*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<char*>

namespace ROOT {
   static TClass *vectorlEboolgR_Dictionary();
   static void vectorlEboolgR_TClassManip(TClass*);
   static void *new_vectorlEboolgR(void *p = 0);
   static void *newArray_vectorlEboolgR(Long_t size, void *p);
   static void delete_vectorlEboolgR(void *p);
   static void deleteArray_vectorlEboolgR(void *p);
   static void destruct_vectorlEboolgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<bool>*)
   {
      vector<bool> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<bool>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<bool>", -2, "vector", 518,
                  typeid(vector<bool>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEboolgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<bool>) );
      instance.SetNew(&new_vectorlEboolgR);
      instance.SetNewArray(&newArray_vectorlEboolgR);
      instance.SetDelete(&delete_vectorlEboolgR);
      instance.SetDeleteArray(&deleteArray_vectorlEboolgR);
      instance.SetDestructor(&destruct_vectorlEboolgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<bool> >()));

      ::ROOT::AddClassAlternate("vector<bool>","std::vector<bool, std::allocator<bool> >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<bool>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEboolgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<bool>*)0x0)->GetClass();
      vectorlEboolgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEboolgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEboolgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<bool> : new vector<bool>;
   }
   static void *newArray_vectorlEboolgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<bool>[nElements] : new vector<bool>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEboolgR(void *p) {
      delete ((vector<bool>*)p);
   }
   static void deleteArray_vectorlEboolgR(void *p) {
      delete [] ((vector<bool>*)p);
   }
   static void destruct_vectorlEboolgR(void *p) {
      typedef vector<bool> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<bool>

namespace ROOT {
   static TClass *vectorlEULong64_tgR_Dictionary();
   static void vectorlEULong64_tgR_TClassManip(TClass*);
   static void *new_vectorlEULong64_tgR(void *p = 0);
   static void *newArray_vectorlEULong64_tgR(Long_t size, void *p);
   static void delete_vectorlEULong64_tgR(void *p);
   static void deleteArray_vectorlEULong64_tgR(void *p);
   static void destruct_vectorlEULong64_tgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ULong64_t>*)
   {
      vector<ULong64_t> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ULong64_t>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ULong64_t>", -2, "vector", 210,
                  typeid(vector<ULong64_t>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEULong64_tgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ULong64_t>) );
      instance.SetNew(&new_vectorlEULong64_tgR);
      instance.SetNewArray(&newArray_vectorlEULong64_tgR);
      instance.SetDelete(&delete_vectorlEULong64_tgR);
      instance.SetDeleteArray(&deleteArray_vectorlEULong64_tgR);
      instance.SetDestructor(&destruct_vectorlEULong64_tgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ULong64_t> >()));

      ::ROOT::AddClassAlternate("vector<ULong64_t>","std::vector<unsigned long long, std::allocator<unsigned long long> >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<ULong64_t>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEULong64_tgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ULong64_t>*)0x0)->GetClass();
      vectorlEULong64_tgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEULong64_tgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEULong64_tgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<ULong64_t> : new vector<ULong64_t>;
   }
   static void *newArray_vectorlEULong64_tgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<ULong64_t>[nElements] : new vector<ULong64_t>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEULong64_tgR(void *p) {
      delete ((vector<ULong64_t>*)p);
   }
   static void deleteArray_vectorlEULong64_tgR(void *p) {
      delete [] ((vector<ULong64_t>*)p);
   }
   static void destruct_vectorlEULong64_tgR(void *p) {
      typedef vector<ULong64_t> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ULong64_t>

namespace ROOT {
   static TClass *vectorlELong64_tgR_Dictionary();
   static void vectorlELong64_tgR_TClassManip(TClass*);
   static void *new_vectorlELong64_tgR(void *p = 0);
   static void *newArray_vectorlELong64_tgR(Long_t size, void *p);
   static void delete_vectorlELong64_tgR(void *p);
   static void deleteArray_vectorlELong64_tgR(void *p);
   static void destruct_vectorlELong64_tgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<Long64_t>*)
   {
      vector<Long64_t> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<Long64_t>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<Long64_t>", -2, "vector", 210,
                  typeid(vector<Long64_t>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlELong64_tgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<Long64_t>) );
      instance.SetNew(&new_vectorlELong64_tgR);
      instance.SetNewArray(&newArray_vectorlELong64_tgR);
      instance.SetDelete(&delete_vectorlELong64_tgR);
      instance.SetDeleteArray(&deleteArray_vectorlELong64_tgR);
      instance.SetDestructor(&destruct_vectorlELong64_tgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<Long64_t> >()));

      ::ROOT::AddClassAlternate("vector<Long64_t>","std::vector<long long, std::allocator<long long> >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<Long64_t>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlELong64_tgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<Long64_t>*)0x0)->GetClass();
      vectorlELong64_tgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlELong64_tgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlELong64_tgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Long64_t> : new vector<Long64_t>;
   }
   static void *newArray_vectorlELong64_tgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Long64_t>[nElements] : new vector<Long64_t>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlELong64_tgR(void *p) {
      delete ((vector<Long64_t>*)p);
   }
   static void deleteArray_vectorlELong64_tgR(void *p) {
      delete [] ((vector<Long64_t>*)p);
   }
   static void destruct_vectorlELong64_tgR(void *p) {
      typedef vector<Long64_t> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<Long64_t>

namespace ROOT {
   static TClass *maplEstringcOboolgR_Dictionary();
   static void maplEstringcOboolgR_TClassManip(TClass*);
   static void *new_maplEstringcOboolgR(void *p = 0);
   static void *newArray_maplEstringcOboolgR(Long_t size, void *p);
   static void delete_maplEstringcOboolgR(void *p);
   static void deleteArray_maplEstringcOboolgR(void *p);
   static void destruct_maplEstringcOboolgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,bool>*)
   {
      map<string,bool> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,bool>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,bool>", -2, "map", 96,
                  typeid(map<string,bool>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOboolgR_Dictionary, isa_proxy, 4,
                  sizeof(map<string,bool>) );
      instance.SetNew(&new_maplEstringcOboolgR);
      instance.SetNewArray(&newArray_maplEstringcOboolgR);
      instance.SetDelete(&delete_maplEstringcOboolgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOboolgR);
      instance.SetDestructor(&destruct_maplEstringcOboolgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,bool> >()));

      ::ROOT::AddClassAlternate("map<string,bool>","std::map<std::string, bool, std::less<std::string>, std::allocator<std::pair<std::string const, bool> > >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,bool>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOboolgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,bool>*)0x0)->GetClass();
      maplEstringcOboolgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOboolgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOboolgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,bool> : new map<string,bool>;
   }
   static void *newArray_maplEstringcOboolgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,bool>[nElements] : new map<string,bool>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOboolgR(void *p) {
      delete ((map<string,bool>*)p);
   }
   static void deleteArray_maplEstringcOboolgR(void *p) {
      delete [] ((map<string,bool>*)p);
   }
   static void destruct_maplEstringcOboolgR(void *p) {
      typedef map<string,bool> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,bool>

namespace ROOT {
   static TClass *maplEstringcOTVector2gR_Dictionary();
   static void maplEstringcOTVector2gR_TClassManip(TClass*);
   static void *new_maplEstringcOTVector2gR(void *p = 0);
   static void *newArray_maplEstringcOTVector2gR(Long_t size, void *p);
   static void delete_maplEstringcOTVector2gR(void *p);
   static void deleteArray_maplEstringcOTVector2gR(void *p);
   static void destruct_maplEstringcOTVector2gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,TVector2>*)
   {
      map<string,TVector2> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,TVector2>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,TVector2>", -2, "map", 96,
                  typeid(map<string,TVector2>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOTVector2gR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,TVector2>) );
      instance.SetNew(&new_maplEstringcOTVector2gR);
      instance.SetNewArray(&newArray_maplEstringcOTVector2gR);
      instance.SetDelete(&delete_maplEstringcOTVector2gR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOTVector2gR);
      instance.SetDestructor(&destruct_maplEstringcOTVector2gR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,TVector2> >()));

      ::ROOT::AddClassAlternate("map<string,TVector2>","std::map<std::string, TVector2, std::less<std::string>, std::allocator<std::pair<std::string const, TVector2> > >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,TVector2>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOTVector2gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,TVector2>*)0x0)->GetClass();
      maplEstringcOTVector2gR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOTVector2gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOTVector2gR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,TVector2> : new map<string,TVector2>;
   }
   static void *newArray_maplEstringcOTVector2gR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,TVector2>[nElements] : new map<string,TVector2>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOTVector2gR(void *p) {
      delete ((map<string,TVector2>*)p);
   }
   static void deleteArray_maplEstringcOTVector2gR(void *p) {
      delete [] ((map<string,TVector2>*)p);
   }
   static void destruct_maplEstringcOTVector2gR(void *p) {
      typedef map<string,TVector2> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,TVector2>

namespace {
  void TriggerDictionaryInitialization_MyAnalysisDict_Impl() {
    static const char* headers[] = {
"../MyAnalysis/PhysicsObjectProxyBase.h",
0
    };
    static const char* includePaths[] = {
"/usr/include/root",
"/gpfs/fs8001/dakiyama/DTStudy/DTAnalysis/second_wave/FirstWave_SkimAnalysis/firstwave_skimanalysis/Root/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "MyAnalysisDict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_AutoLoading_Map;
namespace std{template <typename _Tp> class __attribute__((annotate("$clingAutoload$bits/allocator.h")))  __attribute__((annotate("$clingAutoload$string")))  allocator;
}
namespace std{template <class _CharT> struct __attribute__((annotate("$clingAutoload$bits/char_traits.h")))  __attribute__((annotate("$clingAutoload$string")))  char_traits;
}
class __attribute__((annotate("$clingAutoload$../MyAnalysis/PhysicsObjectProxyBase.h")))  PhysObjBase;
class __attribute__((annotate("$clingAutoload$../MyAnalysis/PhysicsObjectProxyBase.h")))  myJet;
class __attribute__((annotate("$clingAutoload$../MyAnalysis/PhysicsObjectProxyBase.h")))  myElectron;
class __attribute__((annotate("$clingAutoload$../MyAnalysis/PhysicsObjectProxyBase.h")))  myMuon;
class __attribute__((annotate("$clingAutoload$../MyAnalysis/PhysicsObjectProxyBase.h")))  myTau;
class __attribute__((annotate("$clingAutoload$../MyAnalysis/PhysicsObjectProxyBase.h")))  myCaloCluster;
class __attribute__((annotate("$clingAutoload$../MyAnalysis/PhysicsObjectProxyBase.h")))  myMET;
class __attribute__((annotate("$clingAutoload$../MyAnalysis/PhysicsObjectProxyBase.h")))  myTruth;
class __attribute__((annotate("$clingAutoload$../MyAnalysis/PhysicsObjectProxyBase.h")))  myTrack;
class __attribute__((annotate("$clingAutoload$../MyAnalysis/PhysicsObjectProxyBase.h")))  myZee;
class __attribute__((annotate("$clingAutoload$../MyAnalysis/PhysicsObjectProxyBase.h")))  myZmumu;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "MyAnalysisDict dictionary payload"


#define _BACKWARD_BACKWARD_WARNING_H
// Inline headers
#include "../MyAnalysis/PhysicsObjectProxyBase.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[] = {
"PhysObjBase", payloadCode, "@",
"myCaloCluster", payloadCode, "@",
"myElectron", payloadCode, "@",
"myJet", payloadCode, "@",
"myMET", payloadCode, "@",
"myMuon", payloadCode, "@",
"myTau", payloadCode, "@",
"myTrack", payloadCode, "@",
"myTruth", payloadCode, "@",
"myZee", payloadCode, "@",
"myZmumu", payloadCode, "@",
nullptr
};
    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("MyAnalysisDict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_MyAnalysisDict_Impl, {}, classesHeaders, /*hasCxxModule*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_MyAnalysisDict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_MyAnalysisDict() {
  TriggerDictionaryInitialization_MyAnalysisDict_Impl();
}
