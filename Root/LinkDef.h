#ifndef __VEC__
#define __VEC__
 
#include <vector>
#include <map>
  
#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ class vector<vector<int> >+;
#pragma link C++ class vector<vector<bool> >+;
#pragma link C++ class vector<vector<char> >+;
#pragma link C++ class vector<vector<short> >+;
#pragma link C++ class vector<vector<long> >+;
#pragma link C++ class vector<vector<unsigned char> >+;
#pragma link C++ class vector<vector<unsigned short> >+;
#pragma link C++ class vector<vector<unsigned int> >+;
#pragma link C++ class vector<vector<unsigned long> >+;
#pragma link C++ class vector<vector<float> >+;
#pragma link C++ class vector<vector<double> >+;
#pragma link C++ class vector<vector<char*> >+;
#pragma link C++ class vector<vector<const char*> >+;
#pragma link C++ class vector<vector<Long64_t> >+;
#pragma link C++ class vector<vector<ULong64_t> >+;
#pragma link C++ class vector<vector<void*> >+;
#pragma link C++ class map<string,bool>+;
#pragma link C++ class PhysObjBase+;
#pragma link C++ class myTrack+;
#pragma link C++ class myJet+;
#pragma link C++ class myElectron+;
#pragma link C++ class myMuon+;
#pragma link C++ class myTau+;
#pragma link C++ class myZee+;
#pragma link C++ class myZmumu+;
#pragma link C++ class myMET+;
#pragma link C++ class myTruth+;
#pragma link C++ class myCaloCluster+;
#endif
 
#endif
