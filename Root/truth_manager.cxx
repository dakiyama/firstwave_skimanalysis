#include "../MyAnalysis/MyAnalysis.h"
#include "../MyAnalysis/PhysicsObjectProxyBase.h"

void MyAnalysis::truth_manager() {
  for(auto &truth : *m_truthParticles){
    m_truth_phi->push_back(truth->p4.Phi());
    m_truth_eta->push_back(truth->p4.Eta());
    m_truth_pt->push_back(truth->p4.Pt()/1000.);
    m_truth_e->push_back(truth->p4.E()/1000.);
    m_truth_vis_phi->push_back(truth->p4.Phi());
    m_truth_vis_eta->push_back(truth->p4.Eta());
    m_truth_vis_pt->push_back(truth->p4.Pt()/1000.);
    m_truth_vis_e->push_back(truth->p4.E()/1000.);
    m_truth_charge->push_back(truth->Charge);
    m_truth_pdgid->push_back(truth->PdgId);
    m_truth_barcode->push_back(-999);
    m_truth_nChildren->push_back(truth->nChildren);
    m_truth_nTauProngs->push_back(-999);
    m_truth_nPi0->push_back(-999);
    m_truth_isTauHad->push_back(-999);
    m_truth_parent_barcode->push_back(-999);
    m_truth_parent_pdgid->push_back(-999);
    m_truth_properTime->push_back(truth->ProperTime);
    m_truth_decayr->push_back(truth->DecayRadius);
  }
}

void MyAnalysis::truthjet_manager() {
  for(auto &truthjet : *m_truthJets){
    m_truthjet_phi->push_back(truthjet->p4.Phi());
    m_truthjet_eta->push_back(truthjet->p4.Eta());
    m_truthjet_pt->push_back(truthjet->p4.Pt()/1000.);
    m_truthjet_e->push_back(truthjet->p4.E()/1000.);
  }
}
