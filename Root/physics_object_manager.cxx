#include "../MyAnalysis/MyAnalysis.h"
#include "../MyAnalysis/PhysicsObjectProxyBase.h"

Bool_t MyAnalysis::physics_object_manager(){
  //if(m_goodMuons->size() == 0 && m_goodElectrons->size() == 0 && m_MET->p4.Pt()*0.001 < 100.) return false;
  //if(m_goodMuons->size() > 0 && m_goodElectrons->size() > 0) return false;

  // muon
  unsigned ii_muon(0);
  for(auto &muon : *m_goodMuons) {
    m_muon_phi->push_back(muon->p4.Phi());
    m_muon_eta->push_back(muon->p4.Eta());
    m_muon_pt->push_back(muon->p4.Pt()*0.001);
    m_muon_e->push_back(muon->p4.E()*0.001);
    m_muon_charge->push_back(muon->Charge);
    m_muon_ptVarCone20OverPt->push_back(-999.);
    m_muon_ptVarCone30OverPt->push_back(-999.);
    m_muon_ptVarCone40OverPt->push_back(-999.);
    m_muon_eTTopoCone20->push_back(-999.);
    m_muon_eTTopoCone30->push_back(-999.);
    m_muon_eTTopoCone40->push_back(-999.);
    m_muon_quality->push_back(muon->Quality);
    m_muon_isSignal->push_back(muon->IsSignal ? 1 : 0);
    m_muon_isTrigMatch->push_back(muon->triggerMatching.at("LU_SingleMuontrigger"));
    m_muon_key->push_back(ii_muon+100);
    m_muon_index->push_back(muon->index_IDTrack);
    ++ii_muon;
  }

  // electron
  unsigned ii_electron(0);
  for(auto &electron : *m_goodElectrons) {
    m_electron_phi->push_back(electron->p4.Phi());
    m_electron_eta->push_back(electron->p4.Eta());
    m_electron_pt->push_back(electron->p4.Pt()*0.001);
    m_electron_e->push_back(electron->p4.E()*0.001);
    m_electron_charge->push_back(electron->Charge);
    m_electron_ptVarCone20OverPt->push_back(-999.);
    m_electron_eTTopoCone20->push_back(-999.);
    m_electron_quality->push_back(electron->IsSignal ? 2 : 0);
    m_electron_isSignal->push_back(electron->IsSignal ? 1 : 0);
    m_electron_isTrigMatch->push_back(electron->triggerMatching.at("LU_SingleElectrontrigger"));
    m_electron_key->push_back(ii_electron+100);
    m_electron_index->push_back(electron->index_IDTrack);
    ++ii_electron;
  }

  // tau
  for(auto &tau : *m_goodTaus) {
    m_tau_phi->push_back(tau->p4.Phi());
    m_tau_eta->push_back(tau->p4.Eta());
    m_tau_pt->push_back(tau->p4.Pt()*0.001);
    m_tau_e->push_back(tau->p4.E()*0.001);
    m_tau_charge->push_back(tau->Charge);
    m_tau_nprongs->push_back(-999);
    m_tau_BDTJetScore->push_back(-999.);
    m_tau_isMedium->push_back(tau->Quality);
  }

  // jet
  bool is_passed_jet100(false);
  for(auto &jet : *m_goodJets) {
    if(jet->p4.Pt()*0.001 > 100.) is_passed_jet100 = true;
    m_jet_phi->push_back(jet->p4.Phi());
    m_jet_eta->push_back(jet->p4.Eta());
    m_jet_pt->push_back(jet->p4.Pt()*0.001);
    m_jet_e->push_back(jet->p4.E()*0.001);
    m_jet_isBJet->push_back(jet->BTagged);
  }

  // MSTrack
  unsigned nMSTracks(0);
  for(auto &mstrack : *m_msTracks) {
    if(mstrack->p4.Pt()*0.001 > 10. && std::fabs(mstrack->p4.Eta()) < 2.5) {
      m_msTrack_phi->push_back(mstrack->p4.Phi());
      m_msTrack_eta->push_back(mstrack->p4.Eta());
      m_msTrack_pt->push_back(mstrack->p4.Pt()*0.001);
      m_msTrack_e->push_back(mstrack->p4.E()*0.001);
      m_msTrack_charge->push_back(mstrack->charge);
      ++nMSTracks;
    }
  }
  m_nMSTracks = nMSTracks;

  // calo cluster
  unsigned nCaloClusters(0);
  for(auto &caloCluster : *m_egammaClusters) {
    if(caloCluster->p4.Pt()*0.001 > 10. && std::fabs(caloCluster->p4.Eta()) < 2.5) {
      m_caloCluster_phi->push_back(caloCluster->p4.Phi());
      m_caloCluster_eta->push_back(caloCluster->p4.Eta());
      m_caloCluster_pt->push_back(caloCluster->p4.Pt()*0.001);
      m_caloCluster_e->push_back(caloCluster->p4.E()*0.001);
      m_caloCluster_clusterSize->push_back(caloCluster->ClusterSize);
      ++nCaloClusters;
    }
  }
  m_nCaloClusters = nCaloClusters;

  // met
  m_missingET_TST_pt = m_MET->p4.Pt()*0.001;
  m_missingET_TST_phi = m_MET->p4.Phi();
  m_TST_pt = -999.;
  m_TST_phi = -999.;
  m_missingET_CST_pt = m_MET->p4.Pt()*0.001;
  m_missingET_CST_phi = m_MET->p4.Phi();
  m_CST_pt = -999.;
  m_CST_phi = -999.;
  m_missingET_TST_leleVeto_pt = m_MET_Electron->p4.Pt()*0.001;
  m_missingET_TST_leleVeto_phi = m_MET_Electron->p4.Phi();
  m_missingET_TST_lmuVeto_pt = m_MET_Muon->p4.Pt()*0.001;
  m_missingET_TST_lmuVeto_phi = m_MET_Muon->p4.Phi();

  m_kinematics_region = "none";
  if(m_missingET_TST_pt > 200. && is_passed_jet100) m_kinematics_region = "highmet";

  // if(m_goodMuons->size() == 2) return true;
  // else if(m_goodElectrons->size() == 2) return true;
  // else return false;
  return true;
}
